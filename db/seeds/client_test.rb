# frozen_string_literal: true
# These seeds are currently only required for the UI tests
load "#{File.dirname(__FILE__)}/common_service_types.rb"
load "#{File.dirname(__FILE__)}/containers.rb"
