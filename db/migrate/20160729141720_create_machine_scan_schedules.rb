class CreateMachineScanSchedules < ActiveRecord::Migration[4.2]
  def change
    create_table :machine_scan_schedules do |t|
      t.references :machine, index: true
      t.integer :period, default: 0, null: false
      t.string :at, null: false

      t.timestamps null: false
    end
    add_foreign_key :machine_scan_schedules, :machines, on_delete: :cascade
  end
end
