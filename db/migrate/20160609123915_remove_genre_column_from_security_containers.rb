class RemoveGenreColumnFromSecurityContainers < ActiveRecord::Migration[4.2]
  def up
    change_table :security_containers do |t|
      t.remove :genre
    end
  end

  def down
    change_table :security_containers do |t|
      t.integer :genre, default: 0, null: false
    end
  end
end
