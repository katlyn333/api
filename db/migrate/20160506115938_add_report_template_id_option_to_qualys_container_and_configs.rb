class AddReportTemplateIdOptionToQualysContainerAndConfigs < ActiveRecord::Migration[4.2]
  def up
    qualys = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys:0.0.1')
    return unless qualys
    ActiveRecord::Base.transaction do
      dconfig = qualys.default_config.dup
      dconfig['report_template_id'] = '1880200'
      qualys.default_config = dconfig
      qualys.prog_args += ' -t %{report_template_id}'
      qualys.save!

      SecurityContainerConfig.where(security_container: qualys).each do |sc_config|
        config = sc_config.values.dup
        config['report_template_id'] = dconfig['report_template_id']
        sc_config.values = config
        sc_config.save!
      end
    end
  end

  def down
    qualys = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys:0.0.1')
    return unless qualys
    ActiveRecord::Base.transaction do
      config = qualys.default_config.dup
      config.delete 'report_template_id'
      qualys.default_config = config
      qualys.prog_args = qualys.prog_args.gsub(/-t %{report_template_id}/, '')
      qualys.save!

      SecurityContainerConfig.where(security_container: qualys).each do |sc_config|
        config = sc_config.values.dup
        config.delete 'report_template_id'
        sc_config.values = config
        sc_config.save!
      end
    end
  end
end
