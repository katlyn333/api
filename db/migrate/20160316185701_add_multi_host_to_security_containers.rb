class AddMultiHostToSecurityContainers < ActiveRecord::Migration[4.2]
  def change
    add_column :security_containers, :multi_host, :boolean, default: false, null: false
  end
end
