class DropAgentCommands < ActiveRecord::Migration[5.0]
  def up
    drop_table :agent_commands
  end

  def down
    create_table :agent_commands do |t|
      t.references :commandable, polymorphic: true, index: true
      t.text :error_details
      t.integer :state, default: 0, null: false
      t.json :statement, null: false

      t.timestamps null: false
    end
  end
end
