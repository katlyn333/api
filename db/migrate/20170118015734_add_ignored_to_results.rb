class AddIgnoredToResults < ActiveRecord::Migration[5.0]
  def change
    add_column :results, :ignored, :boolean, default: false, null: false
  end
end
