class RemovePolymorphicCommandColumnFromBlackBoxAssessments < ActiveRecord::Migration[4.2]
  class BlackBoxAssessment < ActiveRecord::Base
    belongs_to :command, polymorphic: true
    belongs_to :docker_command
  end

  def up
    change_table :black_box_assessments do |t|
      t.references :docker_command, index: true
    end
    add_foreign_key :black_box_assessments, :docker_commands, on_delete: :cascade
    BlackBoxAssessment.reset_column_information

    BlackBoxAssessment.all.each do |assessment|
      assessment.docker_command_id = assessment.command_id
      assessment.save!
    end

    change_table :black_box_assessments do |t|
      t.remove :command_type
      t.remove :command_id
    end
  end

  def down
    change_table :black_box_assessments do |t|
      t.references :command, polymorphic: true, index: true
    end
    BlackBoxAssessment.reset_column_information

    BlackBoxAssessment.all.each do |assessment|
      assessment.command = assessment.docker_command
      assessment.save!
    end

    change_table :black_box_assessments do |t|
      t.remove :docker_command_id
    end
  end
end
