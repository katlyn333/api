class AddUniquenessConstraintIndexToSecurityContainerConfigs < ActiveRecord::Migration[4.2]
  def change
    add_index(:security_container_configs,
              [:security_container_id, :configurable_type, :configurable_id],
              name: 'index_security_container_configs_unique_container_configurable', unique: true
             )
  end
end
