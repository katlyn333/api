class AddTestTypesColumnToSecurityContainers < ActiveRecord::Migration[4.2]
  def change
    add_column :security_containers, :test_types, :string, array: true, default: [], null: false
  end
end
