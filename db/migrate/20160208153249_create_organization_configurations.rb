class CreateOrganizationConfigurations < ActiveRecord::Migration[4.2]
  def change
    create_table :organization_configurations do |t|
      t.references :organization, null: false
      t.boolean :auto_approve_docker_relays, default: false, null: false

      t.timestamps null: false
    end
    add_index :organization_configurations, :organization_id, unique: true
    add_foreign_key :organization_configurations, :organizations, on_delete: :cascade
  end
end
