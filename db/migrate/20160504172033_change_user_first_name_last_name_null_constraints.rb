class ChangeUserFirstNameLastNameNullConstraints < ActiveRecord::Migration[4.2]
  def up
    change_column_null(:users, :firstname, true)
    change_column_null(:users, :lastname, true)
  end

  def down
    User.where(firstname: nil).each do |user|
      user.firstname = 'FirstName'
      user.save!
    end
    User.where(lastname: nil).each do |user|
      user.lastname = 'LastName'
      user.save!
    end
    change_column_null(:users, :firstname, false)
    change_column_null(:users, :lastname, false)
  end
end
