class CreateSecurityContainerSecrets < ActiveRecord::Migration[4.2]
  def change
    create_table :security_container_secrets do |t|
      t.string :secret, null: false
      t.timestamps null: false
    end
    add_index :security_container_secrets, :secret, unique: true
  end
end
