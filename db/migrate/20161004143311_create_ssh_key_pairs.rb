class CreateSshKeyPairs < ActiveRecord::Migration[4.2]
  def change
    create_table :ssh_key_pairs do |t|
      t.string :name, null: false
      t.text :description
      t.string :username_encrypted
      t.string :key_encrypted
      t.string :key_signature, null: false
      t.references :organization, index: true

      t.timestamps null: false
    end
    add_foreign_key :ssh_key_pairs, :organizations, on_delete: :cascade
    add_index :ssh_key_pairs, [:name, :organization_id], unique: true
  end
end
