class UpdateProgArgsForQualysSecurityContainer < ActiveRecord::Migration[4.2]
  class SecurityContainer < ActiveRecord::Base
  end

  def up
    say_with_time 'Migrating Qualys-Authenticated Config to support custom port' do
      sc = SecurityContainer.find_by name: 'norad-registry.cisco.com:5000/qualys-authenticated:0.0.1'
      return unless sc
      say_with_time('Found the container.sc Updating the prog_args values; adding port field') do
        sc.prog_args += ' -z %{port}'
        sc.save!
      end
    end
  end

  def down
    say_with_time 'Migrating Qualys-Authenticated Config to support custom port' do
      sc = SecurityContainer.find_by name: 'norad-registry.cisco.com:5000/qualys-authenticated:0.0.1'
      return unless sc
      say_with_time('Found the container.sc Updating the prog_args values; deleting port field') do
        sc.prog_args.gsub! /\s+-z\s+%{port}/, ''
        sc.save!
      end
    end
  end
end
