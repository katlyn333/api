class CreateOrganizationTokens < ActiveRecord::Migration[4.2]
  def change
    create_table :organization_tokens do |t|
      t.string :value, null: false
      t.integer :state, null: false, default: 0
      t.references :organization, index: true, null: false

      t.timestamps null: false
    end
    add_index :organization_tokens, :value, unique: true
    add_foreign_key :organization_tokens, :organizations, on_delete: :cascade
  end
end
