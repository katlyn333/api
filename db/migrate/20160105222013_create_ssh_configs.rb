class CreateSshConfigs < ActiveRecord::Migration[4.2]
  def change
    create_table :ssh_configs do |t|
      t.references :machine, index: true, unique: true
      t.string :username
      t.text :key

      t.timestamps null: false
    end
    add_foreign_key :ssh_configs, :machines, on_delete: :cascade
  end
end
