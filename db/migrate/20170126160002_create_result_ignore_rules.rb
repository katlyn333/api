class CreateResultIgnoreRules < ActiveRecord::Migration[5.0]
  def change
    create_table :result_ignore_rules do |t|
      t.integer :ignore_scope_id, null: false
      t.string :ignore_scope_type, null: false
      t.string :signature, index: true, null: false
      t.text :comment
      t.string :created_by, null: false

      t.timestamps
    end
    add_index(:result_ignore_rules,
              [:ignore_scope_type, :ignore_scope_id],
              name: 'index_result_ignore_rules_on_i_s_type_and_i_s_id'
             )
  end
end
