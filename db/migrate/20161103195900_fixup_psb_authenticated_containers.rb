class FixupPsbAuthenticatedContainers < ActiveRecord::Migration[4.2]
  def up
    %w(psb-evaluative psb-informative).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{test}:0.0.1")
      next unless container
      new_args = container.prog_args.gsub(/%{test_regex}/, container.default_config['test_regex'])
      container.default_config = {}
      container.test_types = ['authenticated']
      container.prog_args = new_args
      container.save!
      container.security_container_configs.each do |config|
        config.values = {}
        config.save!
      end
    end
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
