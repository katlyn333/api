class AddCommonServiceTypesReferenceToServices < ActiveRecord::Migration[4.2]
  def change
    add_reference :services, :common_service_type, index: true, foreign_key: true
  end
end
