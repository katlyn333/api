#!/bin/bash

db_host="$1"
shift
cmd="$@"

update_db() {
    >&2 echo "Checking if DB Exists"
    bundle exec rake db:exists
    if [ $? -eq 0 ]; then
        >&2 echo "DB does exist. Migrating DB"
        bundle exec rake db:migrate
    else
        >&2 echo "DB doesn't exist. Setting up DB"
	bundle exec rake db:create
	bundle exec rake db:schema:load
	>&2 echo "Seeding DB"
	bundle exec rake db:seed:production
    fi
}

>&2 echo "Running Bundle Install"
bundle install

until psql -h "$db_host" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 2
done

>&2 echo "Postgres is up"
update_db

if [ -z "$cmd" ]; then
  echo "ERROR: NO COMMAND GIVEN!"
  exit 1
fi

>&2 echo "Executing command: ${cmd}"
exec $cmd
