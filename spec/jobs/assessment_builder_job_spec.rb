# frozen_string_literal: true
require 'rails_helper'

# The only way to meaningfully test this class is to check if it enqueues jobs for the
# ScheduleContainerJob. It's a litte weird to me to reference the ScheduleContainerJob here, but
# think of this as more of an integration test than a unit test

RSpec.describe AssessmentBuilderJob, type: :job do
  include ActiveJob::TestHelper

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform_later(@dc) }

  context 'for a multi host container' do
    before :each do
      container = create :security_container, multi_host: true
      @dc = build :docker_command, containers: [container.name]
    end

    context 'a command for a machine' do
      before :each do
        # Set FQDN to nil to ensure that the machine's IP address is used for this set of tests
        @dc.commandable = create :machine, fqdn: nil
        @dc.save!
      end

      it 'queues a single container' do
        expect(ScheduleContainerJob).to receive(:perform_later).once
        perform_enqueued_jobs { job }
      end

      it 'sends a job to the swarm with a single machine as the target' do
        expected_args = [
          anything,
          @dc.containers.first,
          anything,
          [hash_including(id: @dc.commandable.target_address)],
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end
    end

    context 'a command for an organization' do
      before :each do
        @container = create(
          :security_container,
          multi_host: true, prog_args: '%{target} %{port}', default_config: { 'port' => 443 }
        )
        @dc.containers = [@container.name]
        @dc.commandable = create :organization
        @dc.save!
        @org = @dc.commandable
        create(:security_container_config,
               security_container: @container,
               values: { 'port' => 123 },
               enabled_outside_of_requirement: true,
               configurable: @org)
        @num_machines = 3
        @num_machines.times do
          # Set IP to nil to ensure that the machine's FQDN is used for this set of tests
          @org.machines << build(:machine, ip: nil)
        end
      end

      it 'queues a single container' do
        expect(ScheduleContainerJob).to receive(:perform_later).once
        perform_enqueued_jobs { job }
      end

      it 'sends a job to the swarm with all org machines as the target' do
        expected_args = [
          anything,
          @dc.containers.first,
          anything,
          @dc.commandable.machines.map do |machine|
            hash_including(id: machine.target_address)
          end,
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end

      it 'queues mutliple containers if there are multiple configs' do
        org = @dc.commandable
        org.machines.each do |machine|
          config = build(:security_container_config, security_container: @container, values: { 'port' => 123 })
          machine.security_container_configs << config
        end
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines).times
        perform_enqueued_jobs { job }
      end

      context 'without a requirement or explicitly enabled config at the org level' do
        before :each do
          @org.security_container_configs.destroy_all
        end

        it 'only queues a container for machines that have an explicitly enabled config' do
          org = @dc.commandable
          org.machines.take(@num_machines - 1).each do |machine|
            config = build(:security_container_config, security_container: @container, values: { 'port' => 123 })
            machine.security_container_configs << config
          end
          expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines - 1).times
          perform_enqueued_jobs { job }
        end
      end
    end
  end

  context 'for a configurable test' do
    describe 'at the service level' do
      before :each do
        @container = create(
          :security_container,
          multi_host: false,
          test_types: ['ssl_crypto'],
          configurable: true,
          prog_args: '%{target} %{extra_config}',
          default_config: { 'extra_config' => 'abcdef' }
        )
        @dc = build :docker_command, containers: [@container.name]
        @dc.commandable = create :organization
        @dc.save!
        @org = @dc.commandable
        @m1 = create :machine, organization: @org
        create :generic_service, encryption_type: :ssl, machine: @m1
      end

      it 'uses the organization level config if none are specified for the machine' do
        custom_val = 1234
        create(:security_container_config,
               security_container: @container,
               values: { 'extra_config' => custom_val },
               enabled_outside_of_requirement: true,
               configurable: @org)
        expected_args = [
          anything,
          @dc.containers.first,
          array_including(custom_val.to_s),
          @dc.commandable.machines.map do |machine|
            hash_including(id: machine.target_address)
          end,
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end

      it 'uses the specific machine configuration' do
        custom_val = 4321
        create(:security_container_config,
               security_container: @container,
               values: { 'extra_config' => custom_val },
               enabled_outside_of_requirement: true,
               configurable: @m1)
        expected_args = [
          anything,
          @dc.containers.first,
          array_including(custom_val.to_s),
          @dc.commandable.machines.map do |machine|
            hash_including(id: machine.target_address)
          end,
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end
    end

    describe 'against the whole host' do
      before :each do
        @container = create(
          :security_container,
          multi_host: false,
          test_types: ['whole_host'],
          configurable: true,
          prog_args: '%{target} %{extra_config}',
          default_config: { 'extra_config' => 'abcdef' }
        )
        @dc = build :docker_command, containers: [@container.name]
        @dc.commandable = create :organization
        @dc.save!
        @org = @dc.commandable
        @m1 = create :machine, organization: @org
      end

      it 'uses the organization level config if none are specified for the machine' do
        custom_val = 1234
        create(:security_container_config,
               security_container: @container,
               values: { 'extra_config' => custom_val },
               enabled_outside_of_requirement: true,
               configurable: @org)
        expected_args = [
          anything,
          @dc.containers.first,
          array_including(custom_val.to_s),
          @dc.commandable.machines.map do |machine|
            hash_including(id: machine.target_address)
          end,
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end

      it 'uses the specific machine configuration' do
        custom_val = 4321
        create(:security_container_config,
               security_container: @container,
               values: { 'extra_config' => custom_val },
               enabled_outside_of_requirement: true,
               configurable: @m1)
        expected_args = [
          anything,
          @dc.containers.first,
          array_including(custom_val.to_s),
          @dc.commandable.machines.map do |machine|
            hash_including(id: machine.target_address)
          end,
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'for a single host container of whole_host type' do
    before :each do
      container = create :security_container, multi_host: false
      @dc = build :docker_command, containers: [container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues a single container' do
        expect(ScheduleContainerJob).to receive(:perform_later).once
        perform_enqueued_jobs { job }
      end

      it 'sends a job to the swarm with a single machine as the target' do
        expected_args = [
          anything,
          @dc.containers.first,
          anything,
          [hash_including(id: @dc.commandable.target_address)],
          any_args
        ]
        expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
        perform_enqueued_jobs { job }
      end
    end

    context 'a command for an organization' do
      before :each do
        @container = create(
          :security_container,
          multi_host: false, prog_args: '%{target}', default_config: {}
        )
        @dc.containers = [@container.name]
        @dc.commandable = create :organization
        @dc.save!
        @org = @dc.commandable
        create(:security_container_config,
               security_container: @container,
               values: {},
               enabled_outside_of_requirement: true,
               configurable: @org)
        @num_machines = 3
        @num_machines.times do
          @org.machines << build(:machine)
        end
      end

      it 'queues a container for each machine' do
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines).times
        perform_enqueued_jobs { job }
      end

      it 'sends a job to the swarm for each machine' do
        expect(DockerSwarm).to receive(:queue_container).exactly(@num_machines).times
        perform_enqueued_jobs { job }
      end

      it 'queues mutliple containers if there are multiple configs' do
        org = @dc.commandable
        org.machines.each do |machine|
          config = build(:security_container_config, security_container: @container, values: { 'port' => 123 })
          machine.security_container_configs << config
        end
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines).times
        perform_enqueued_jobs { job }
      end

      context 'without a requirement or explicitly enabled config at the org level' do
        before :each do
          @org.security_container_configs.destroy_all
        end

        it 'only queues a container for machines that have an explicitly enabled config' do
          org = @dc.commandable
          org.machines.take(@num_machines - 1).each do |machine|
            config = build(:security_container_config, security_container: @container, values: {})
            machine.security_container_configs << config
          end
          expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines - 1).times
          perform_enqueued_jobs { job }
        end
      end
    end
  end

  context 'for a single host container of web_application type' do
    before :each do
      @container = create :security_container, multi_host: false, test_types: ['web_application']
      @dc = build :docker_command, containers: [@container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues one container per web application service' do
        machine = @dc.commandable
        machine.services << create(:web_application_service, port: 80)
        machine.services << create(:web_application_service, port: 81)
        machine.services << create(:generic_service, encryption_type: :ssh, port: 22)
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(2).times
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'for a single host container of brute_force type' do
    before :each do
      @container = create :security_container, multi_host: false, test_types: ['brute_force']
      @dc = build :docker_command, containers: [@container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues one container per service that allows brute force' do
        machine = @dc.commandable
        machine.services << create(:web_application_service, allow_brute_force: true, port: 80)
        machine.services << create(:ssh_service, allow_brute_force: true, port: 22)
        machine.services << create(:generic_service, allow_brute_force: false, port: 123)
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(2).times
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'for a single host container of authenticated type' do
    before :each do
      @container = create :security_container, multi_host: false, test_types: ['authenticated']
      @dc = build :docker_command, containers: [@container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues one container per service that uses has an ssh service' do
        machine = @dc.commandable
        machine.services << create(:ssh_service, port: 22)
        machine.services << create(:ssh_service, port: 23)
        machine.services << create(:generic_service, encryption_type: :ssh, port: 24)
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(2).times
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'for a single host container of ssh_crypto type' do
    before :each do
      @container = create :security_container, multi_host: false, test_types: ['ssh_crypto']
      @dc = build :docker_command, containers: [@container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues no container if the machine has no services' do
        expect(@dc.commandable.services.count).to eq(0)
        expect(ScheduleContainerJob).to_not receive(:perform_later)
        perform_enqueued_jobs { job }
      end

      it 'queues one container per service that uses ssh encryption' do
        machine = @dc.commandable
        machine.services << create(:generic_service, encryption_type: :ssl)
        machine.services << create(:generic_service, encryption_type: :ssl)
        machine.services << create(:generic_service, encryption_type: :ssh)
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(1).times
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'for a single host container of ssl_crypto type' do
    before :each do
      @container = create :security_container, multi_host: false, test_types: ['ssl_crypto']
      @dc = build :docker_command, containers: [@container.name]
    end

    context 'a command for a machine' do
      before :each do
        @dc.commandable = create :machine
        @dc.save!
      end

      it 'queues no container if the machine has no services' do
        expect(@dc.commandable.services.count).to eq(0)
        expect(ScheduleContainerJob).to_not receive(:perform_later)
        perform_enqueued_jobs { job }
      end

      it 'queues one container per service that uses ssl encryption' do
        machine = @dc.commandable
        machine.services << create(:generic_service, encryption_type: :ssl)
        machine.services << create(:generic_service, encryption_type: :ssl)
        machine.services << create(:generic_service, encryption_type: :ssh)
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(2).times
        perform_enqueued_jobs { job }
      end

      context 'when a common service type is applied to the test' do
        before :each do
          @service_type = create :common_service_type, name: 'fancyapp'
          @container.common_service_type = @service_type
          @container.save!
        end

        it 'only runs against services that have a matching type or no type at all' do
          machine = @dc.commandable
          new_service_type = create :common_service_type, name: 'somethingelse', port: 1234
          create(:generic_service, encryption_type: :ssl, machine: machine, common_service_type: @service_type)
          create(:generic_service, encryption_type: :ssl, machine: machine, common_service_type: nil)
          create(:generic_service, encryption_type: :ssl, machine: machine, common_service_type: new_service_type)
          expect(ScheduleContainerJob).to receive(:perform_later).exactly(2).times
          perform_enqueued_jobs { job }
        end
      end
    end

    context 'a command for an organization' do
      before :each do
        @container = create :security_container, multi_host: false, test_types: ['ssl_crypto']
        @dc.containers = [@container.name]
        @dc.commandable = create :organization
        @dc.save!
        @org = @dc.commandable
        create(:security_container_config,
               security_container: @container,
               values: {},
               enabled_outside_of_requirement: true,
               configurable: @org)
        @num_machines = 3
        @num_machines.times do
          machine = build(:machine)
          @org.machines << machine
          machine.services << build(:generic_service, encryption_type: :ssl)
        end
      end

      it 'queues a container for each service on each machine' do
        expect(ScheduleContainerJob).to receive(:perform_later).exactly(@num_machines).times
        perform_enqueued_jobs { job }
      end
    end
  end

  context 'with a relay enabled' do
    before :each do
      org = create :organization
      @relay = create :docker_relay, organization: org
      @relay.update_column(:verified, true)
      org.machines << build(:machine)
      container = create :security_container, prog_args: '%{target}', default_config: {}
      create(:security_container_config,
             security_container: container,
             values: {},
             enabled_outside_of_requirement: true,
             configurable: org)
      @dc = create :docker_command, containers: [container.name], commandable: org
    end

    it 'adds relay queue info to the job' do
      expected_args = [
        anything,
        @dc.containers.first,
        anything,
        anything,
        anything,
        hash_including(
          relay_secret: @relay.file_encryption_key,
          relay_queue: @relay.queue_name,
          relay_exchange: @dc.commandable.token
        )
      ]
      expect(DockerSwarm).to receive(:queue_container).once.with(*expected_args)
      perform_enqueued_jobs { job }
    end
  end

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in the command_processing queue' do
    expect(described_class.new.queue_name).to eq('command_processing')
  end

  it 'changes the docker command state to signify that assessments were created' do
    org = create :organization
    @dc = create :docker_command, containers: [create(:security_container).name], commandable: org
    3.times do
      org.machines << build(:machine)
    end
    allow(DockerSwarm).to receive(:queue_container)
    perform_enqueued_jobs { job }
    expect(@dc.reload.state).to eq('assessments_created')
  end

  it 'changes the docker command state to errored if an exception is raised while creating asssessments' do
    @dc = create :docker_command, containers: [create(:security_container).name]
    expect_any_instance_of(described_class).to receive(:perform).and_raise(StandardError)
    perform_enqueued_jobs { job }
    expect(@dc.reload.state).to eq('errored')
  end

  it 'updates the assessment to be in progress' do
    org = create :organization
    @dc = create :docker_command, containers: [create(:security_container).name], commandable: org
    3.times do
      org.machines << build(:machine)
    end
    allow(DockerSwarm).to receive(:queue_container)
    perform_enqueued_jobs { job }
    @dc.assessments.each do |assessment|
      expect(assessment.state).to eq('in_progress')
    end
  end

  it 'updates the assessment if an exception is raised while scheduling' do
    org = create :organization
    @dc = create :docker_command, containers: [create(:security_container).name], commandable: org
    3.times do
      org.machines << build(:machine)
    end
    allow(DockerSwarm).to receive(:queue_container).and_raise(StandardError)
    perform_enqueued_jobs { job }
    @dc.assessments.each do |assessment|
      expect(assessment.state).to eq('complete')
      expect(assessment.results.size).to eq(1)
      expect(assessment.results.first.status).to eq('error')
    end
  end
end
