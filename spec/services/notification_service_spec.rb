# frozen_string_literal: true
require 'rails_helper'

RSpec.describe NotificationService do
  before :each do
    @dc = create :docker_command, containers: [create(:security_container).name]
    @org = @dc.resolved_organization
  end

  context 'notifications enabled' do
    before :each do
      @message_delivery = instance_double(ActionMailer::MessageDelivery)
    end

    it 'sends an email when scan_complete is triggered' do
      expect(NotificationMailer).to receive(:scan_complete).with(@dc.id).and_return(@message_delivery)
      expect(@message_delivery).to receive(:deliver_later)
      NotificationService.scan_complete(@dc)
    end
  end

  context 'notifications disabled' do
    before :each do
      @org.notification_channels.find_by(event: 'scan_complete').update_column(:enabled, false)
    end

    it 'does not send an email when scan_complete is triggered' do
      expect(NotificationMailer).not_to receive(:scan_complete).with(@dc.id)
      NotificationService.scan_complete(@dc)
    end
  end
end
