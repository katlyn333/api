# frozen_string_literal: true
require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  context 'for a local auth user' do
    context 'when a user is created' do
      it 'sends the confirmation email to the user' do
        perform_enqueued_jobs do
          user = create(:user_with_password)
          email = ActionMailer::Base.deliveries.last
          expect([user.email]).to eq(email.to)
          expect(email.subject).to eq('Please confirm your email address')
        end
      end
    end

    context 'when a user is confirmed' do
      it 'sends the welcome email to the user' do
        perform_enqueued_jobs do
          user = create(:user_with_password).reload
          user.confirm!
          email = ActionMailer::Base.deliveries.last
          expect([user.email]).to eq(email.to)
          expect(email.subject).to eq('Welcome to Norad')
        end
      end
    end

    context 'when a password is reset' do
      it 'send the password reset email to the user' do
        perform_enqueued_jobs do
          user = create(:user_with_password).reload
          user.confirm!
          user.reset_password!
          email = ActionMailer::Base.deliveries.last
          expect([user.email]).to eq email.to
          expect(email.subject).to eq 'Norad password reset'
          expect(email.body).to include user.local_authentication_record.password_reset_token
        end
      end
    end
  end

  context 'for an sso user' do
    context 'when a user is created' do
      it 'sends the welcome email to the user' do
        perform_enqueued_jobs do
          user = create :user_with_sso
          email = ActionMailer::Base.deliveries.last
          expect([user.email]).to eq(email.to)
          expect(email.subject).to eq('Welcome to Norad')
        end
      end
    end
  end
end
