# frozen_string_literal: true
require 'rails_helper'

RSpec.describe RelayMailer, type: :mailer do
  before :each do
    @org_admins = Array.new(2) { create(:user) }
    @org_readers = Array.new(2) { create(:user) }
    org = create :organization
    @relay = create :docker_relay, organization: org
    @org_admins.each { |admin| admin.add_role(:organization_admin, org) }
    @org_readers.each { |reader| reader.add_role(:organization_reader, org) }
  end

  context 'when a relay goes offline' do
    it 'sends the email to all organization admins' do
      expect { RelayMailer.offline_notification(@relay).deliver_now } \
        .to change(ActionMailer::Base.deliveries, :count).by(1)

      email = ActionMailer::Base.deliveries.last
      expect(@org_admins.map { |admin| "#{admin.uid}@cisco.com" }).to eq(email.to)
      expect(email.subject).to eq("#{ApplicationMailer::SUBJECT_PREFIX} Your Norad Relay is now Offline")
      expect(email.body.to_s).to match(/#{@relay.key_signature[-23..-1]}/i)
      expect(email.body.to_s).to match(/#{@relay.last_heartbeat}/)
    end
  end

  context 'when a relay comes online' do
    it 'sends the email to all organization admins' do
      expect { RelayMailer.online_notification(@relay).deliver_now } \
        .to change(ActionMailer::Base.deliveries, :count).by(1)

      email = ActionMailer::Base.deliveries.last
      expect(@org_admins.map { |admin| "#{admin.uid}@cisco.com" }).to eq(email.to)
      expect(email.subject).to eq("#{ApplicationMailer::SUBJECT_PREFIX} Your Norad Relay is now Online")
      expect(email.body.to_s).to match(/#{@relay.key_signature[-23..-1]}/i)
    end
  end
end
