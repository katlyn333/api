# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Security container results creation', type: :request do
  include NoradControllerTestHelpers
  let(:result_attrs) { attributes_for(:result, nid: 'qid12345') }

  context 'white box assessments' do
    it 'checks hmac using a secret for white box assessments' do
      container = create :whitebox_container
      machine = create :machine
      dc = create :docker_command, commandable: machine, containers: [container.name]
      wba = build :white_box_assessment, docker_command: dc, machine: machine
      secret = create :security_container_secret
      secret.assessments << wba
      payload = {
        results: [result_attrs],
        timestamp: Time.now.to_f.to_s
      }
      sig = OpenSSL::HMAC.hexdigest('sha256', secret.secret, payload.to_json)

      post wba.results_path, params: payload
      expect(response.status).to be 400
      signed_request(:post, wba.results_path, payload, sig)
      expect(response.status).to be 200
    end
  end

  context 'black box assessments' do
    it 'requires the request to be signed with security container secret' do
      container = create :security_container
      machine = create :machine
      dc = create :docker_command, commandable: machine, containers: [container.name]
      bba = build :black_box_assessment, docker_command: dc, machine: machine
      secret = create :security_container_secret
      secret.assessments << bba
      payload = {
        results: [result_attrs],
        timestamp: Time.now.to_f.to_s
      }
      sig = OpenSSL::HMAC.hexdigest('sha256', secret.secret, payload.to_json)

      post bba.results_path, params: payload
      expect(response.status).to be 400
      signed_request(:post, bba.results_path, payload, sig)
      expect(response.status).to be 200
    end
  end
end
