# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Application', type: :request do
  describe 'throttle requests by IP address' do
    before(:each) do
      request_limit =
        File.open('.env').grep(/RACK_ATTACK_REQUESTS/)
            .first.split('=')[1].strip
      stub_const('ENV', 'RACK_ATTACK_REQUESTS' => request_limit)
      @limit = request_limit.to_i
    end

    context 'number of requests lower than limit' do
      it 'does not change request status' do
        @limit.times do
          get '/resque', headers: { 'REMOTE_ADDR' => '1.2.3.4' }
          expect(response.status).to_not eq(429)
        end
      end
    end

    context 'number of requests higher than limit' do
      it 'changes request status to 429' do
        (@limit + 1).times do |i|
          get '/resque', headers: { 'REMOTE_ADDR' => '1.2.3.5' }
          expect(response.status).to eq(429) if i > @limit
        end
      end
    end
  end
end
