# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  uid        :string           not null
#  firstname  :string
#  lastname   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#  index_users_on_uid    (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe 'Users', type: :request do
  describe 'throttle POST requests to users authenticate by IP address' do
    before(:each) do
      ip_login_limit =
        File.open('.env').grep(/RACK_ATTACK_IP_LOGINS/)
            .first.split('=')[1].strip
      stub_const('ENV', 'RACK_ATTACK_IP_LOGINS' => ip_login_limit)
      @limit = ip_login_limit.to_i
    end

    context 'number of requests lower than limit' do
      it 'does not change request status' do
        @limit.times do |i|
          post "/v1/users/example#{i}/authenticate",
               headers: { 'REMOTE_ADDR' => '1.2.3.6' }
          expect(response.status).to_not eq(429)
        end
      end
    end

    context 'number of requests higher than limit' do
      it 'changes request status to 429' do
        (@limit + 1).times do |i|
          post "/v1/users/example#{i}/authenticate",
               headers: { 'REMOTE_ADDR' => '1.2.3.7' }
          expect(response.status).to eq(429) if i > @limit
        end
      end
    end
  end

  describe 'throttle POST requests to users authenticate by username' do
    before(:each) do
      username_login_limit =
        File.open('.env').grep(/RACK_ATTACK_USERNAME_LOGINS/)
            .first.split('=')[1].strip
      stub_const('ENV',
                 'RACK_ATTACK_USERNAME_LOGINS' => username_login_limit)
      @limit = username_login_limit.to_i
    end

    context 'number of requests lower than limit' do
      it 'does not change request status' do
        @limit.times do |i|
          post '/v1/users/good_test_user/authenticate',
               headers: { 'REMOTE_ADDR' => "#{i}.7.7.7" }
          expect(response.status).to_not eq(429)
        end
      end
    end

    context 'number of requests higher than limit' do
      it 'changes request status to 429' do
        (@limit + 1).times do |i|
          post '/v1/users/bad_test_user/authenticate',
               headers: { 'REMOTE_ADDR' => "7.7.7.#{i}" }
          expect(response.status).to eq(429) if i > @limit
        end
      end
    end
  end
end
