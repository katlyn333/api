# frozen_string_literal: true
require 'rails_helper'

describe ApiTokenAuthorizer, type: :authorizer do
  before :each do
    @admin = create :user
    @reader = create :user
    @non_member = create :user
    @org = create :organization
    Membership.create_admin!(@admin, @org)
    Membership.create_reader!(@reader, @org)
  end

  context 'at the instance level' do
    context 'for read actions' do
      it 'permits admins to read token' do
        expect(@org.organization_token.authorizer).to be_readable_by(@admin)
      end

      it 'does not permit readers to read token' do
        expect(@org.organization_token.authorizer).not_to be_readable_by(@reader)
      end

      it 'does not permit non-members to read token' do
        expect(@org.organization_token.authorizer).not_to be_readable_by(@non_member)
      end
    end

    context 'for update actions' do
      it 'permits admins to recreate token' do
        expect(@org.organization_token.authorizer).to be_updatable_by(@admin)
      end

      it 'does not permit readers to recreate token' do
        expect(@org.organization_token.authorizer).not_to be_updatable_by(@reader)
      end

      it 'does not permit non-members to recreate token' do
        expect(@org.organization_token.authorizer).not_to be_updatable_by(@non_member)
      end
    end
  end
end
