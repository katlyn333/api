# frozen_string_literal: true
require 'rails_helper'

describe RequirementGroupAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
    @admin_user.add_role :requirement_group_admin, @requirement_group
  end

  context 'when authorizing at the class level' do
    it 'lets any user create' do
      expect(RequirementGroupAuthorizer).to be_creatable_by(@admin_user)
      expect(RequirementGroupAuthorizer).to be_creatable_by(@unprivileged_user)
    end

    it 'lets everyone read' do
      expect(RequirementGroupAuthorizer).to be_readable_by(@admin_user)
      expect(RequirementGroupAuthorizer).to be_readable_by(@unprivileged_user)
    end
  end

  context 'when authorizing at the instance level' do
    before :each do
      @requirement_group = create :requirement_group
    end

    it 'lets admin users update' do
      expect(@requirement_group.authorizer).to be_updatable_by(@admin_user)
      expect(@requirement_group.authorizer).to_not be_updatable_by(@unprivileged_user)
    end

    it 'lets admin users destroy' do
      expect(@requirement_group.authorizer).to be_deletable_by(@admin_user)
      expect(@requirement_group.authorizer).to_not be_deletable_by(@unprivileged_user)
    end
  end
end
