# frozen_string_literal: true
require 'rails_helper'

describe ApiTokenAuthorizer, type: :authorizer do
  before :each do
    @user1 = create :user
    @user2 = create :user
  end

  context 'at the instance level' do
    context 'for read actions' do
      it 'permits owning user to read' do
        expect(@user1.api_token.authorizer).to be_readable_by(@user1)
      end

      it 'does not permit other users to read' do
        expect(@user1.api_token.authorizer).not_to be_readable_by(@user2)
      end
    end

    context 'for update actions' do
      it 'permits owning user to recreate' do
        expect(@user1.api_token.authorizer).to be_updatable_by(@user1)
      end

      it 'does not permit other users to recreate' do
        expect(@user1.api_token.authorizer).not_to be_updatable_by(@user2)
      end
    end
  end
end
