# frozen_string_literal: true
require 'rails_helper'

describe IaasDiscoveryAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @reader_user = create :user
    @reader_user_other = create :user
    @admin_user_other = create :user
    @org1 = create :organization
    @org2 = create :organization
    config1 = create :iaas_configuration, organization: @org1
    @admin_user.add_role :organization_admin, @org1
    @admin_user_other.add_role :organization_admin, @org2
    @reader_user.add_role :organization_reader, @org1
    @reader_user_other.add_role :organization_reader, @org2
    @iaas_discovery = create :iaas_discovery, iaas_configuration: config1
    @options = { in: @org1 }
  end

  it 'allows org admins to create' do
    expect(IaasDiscovery.authorizer).to be_creatable_by(@admin_user, @options)
    expect(IaasDiscovery.authorizer).to_not be_creatable_by(@admin_user_other, @options)
    expect(IaasDiscovery.authorizer).to_not be_creatable_by(@reader_user, @options)
    expect(IaasDiscovery.authorizer).to_not be_creatable_by(@reader_user_other, @options)
  end

  it 'allows org admins to read' do
    expect(@iaas_discovery.authorizer).to be_readable_by(@admin_user)
    expect(@iaas_discovery.authorizer).to_not be_readable_by(@admin_user_other)
    expect(@iaas_discovery.authorizer).to_not be_readable_by(@reader_user)
    expect(@iaas_discovery.authorizer).to_not be_readable_by(@reader_user_other)

    expect(IaasDiscovery.authorizer).to be_readable_by(@admin_user, @options)
    expect(IaasDiscovery.authorizer).to_not be_readable_by(@admin_user_other, @options)
    expect(IaasDiscovery.authorizer).to_not be_readable_by(@reader_user, @options)
    expect(IaasDiscovery.authorizer).to_not be_readable_by(@reader_user_other, @options)
  end
end
