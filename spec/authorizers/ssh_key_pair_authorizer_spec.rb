# frozen_string_literal: true
require 'rails_helper'

describe SshKeyPairAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @admin_user.add_role :organization_admin, @org
    @reader_user = create :user
    @reader_user.add_role :organization_reader, @org
    @unprivileged_user = create :user

    @org = create :organization
    @ssh_config = create :ssh_key_pair, organization: @org
  end

  context 'ssh key pair action' do
    it 'try to create' do
      expect(SshKeyPair.authorizer).to be_creatable_by(@admin_user, in: @org)
      expect(SshKeyPair.authorizer).to_not be_creatable_by(@reader_user, in: @org)
      expect(SshKeyPair.authorizer).to_not be_creatable_by(@unprivileged_user, in: @org)
    end

    it 'try to list' do
      expect(SshKeyPair.authorizer).to be_readable_by(@admin_user, in: @org)
      expect(SshKeyPair.authorizer).to be_readable_by(@reader_user, in: @org)
      expect(SshKeyPair.authorizer).to_not be_readable_by(@unprivileged_user, in: @org)
    end

    it 'try to delete' do
      expect(@ssh_config.authorizer).to be_deletable_by(@admin_user)
      expect(@ssh_config.authorizer).to_not be_deletable_by(@reader_user)
      expect(@ssh_config.authorizer).to_not be_deletable_by(@unprivileged_user)
    end

    it 'try to update' do
      expect(@ssh_config.authorizer).to be_updatable_by(@admin_user)
      expect(@ssh_config.authorizer).to_not be_updatable_by(@reader_user)
      expect(@ssh_config.authorizer).to_not be_updatable_by(@unprivileged_user)
    end

    it 'try to read' do
      expect(@ssh_config.authorizer).to be_readable_by(@admin_user)
      expect(@ssh_config.authorizer).to be_readable_by(@reader_user)
      expect(@ssh_config.authorizer).to_not be_readable_by(@unprivileged_user)
    end
  end
end
