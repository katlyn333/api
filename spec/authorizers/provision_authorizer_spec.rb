# frozen_string_literal: true
require 'rails_helper'

describe ProvisionAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @admin_user_other = create :user
    @unprivileged_user = create :user

    @req_group = create :requirement_group
    @req_group_other = create :requirement_group

    @admin_user.add_role :requirement_group_admin, @req_group
    @admin_user_other.add_role :requirement_group_admin, @req_group_other

    @requirement = create :requirement, requirement_group: @req_group
    @security_container = create :security_container
    @provision = create :provision, requirement: @requirement, security_container: @security_container
    @options = { in: @requirement }
  end

  it 'allows requirement_group admins to create' do
    expect(Provision.authorizer).to be_creatable_by(@admin_user, @options)
    expect(Provision.authorizer).to_not be_creatable_by(@admin_user_other, @options)
    expect(Provision.authorizer).to_not be_creatable_by(@unprivileged_user, @options)
  end

  it 'allows anyone to read' do
    expect(Provision.authorizer).to be_readable_by(@admin_user, @options)
    expect(Provision.authorizer).to be_readable_by(@admin_user_other, @options)
    expect(Provision.authorizer).to be_readable_by(@unprivileged_user, @options)
  end

  it 'disallows everyone from updating' do
    expect(Provision.authorizer).to_not be_updatable_by(@admin_user, @options)
    expect(Provision.authorizer).to_not be_updatable_by(@admin_user_other, @options)
    expect(Provision.authorizer).to_not be_updatable_by(@unprivileged_user, @options)
  end

  it 'allows requiremnent_group admins to delete' do
    expect(@provision.authorizer).to be_deletable_by(@admin_user)
    expect(@provision.authorizer).to_not be_deletable_by(@admin_user_other)
    expect(@provision.authorizer).to_not be_deletable_by(@unprivileged_user)
  end
end
