# frozen_string_literal: true
require 'rails_helper'

describe OrganizationAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @reader_user = create :user
    @unprivileged_user = create :user
  end

  context 'when authorizing at the class level' do
    it 'lets any user create' do
      expect(OrganizationAuthorizer).to be_creatable_by(@admin_user)
      expect(OrganizationAuthorizer).to be_creatable_by(@reader_user)
    end
  end

  context 'when authorizing at the instance level' do
    before :each do
      @organization = create :organization
      @admin_user.add_role :organization_admin, @organization
      @reader_user.add_role :organization_reader, @organization
    end

    it 'lets admin users update' do
      expect(@organization.authorizer).to be_updatable_by(@admin_user)
      expect(@organization.authorizer).to_not be_updatable_by(@reader_user)
      expect(@organization.authorizer).to_not be_updatable_by(@unprivileged_user)
    end

    it 'lets admin and reader users read' do
      expect(@organization.authorizer).to be_readable_by(@admin_user)
      expect(@organization.authorizer).to be_readable_by(@reader_user)
      expect(@organization.authorizer).to_not be_readable_by(@unprivileged_user)
    end

    it 'lets admin users destroy' do
      expect(@organization.authorizer).to be_deletable_by(@admin_user)
      expect(@organization.authorizer).to_not be_deletable_by(@reader_user)
      expect(@organization.authorizer).to_not be_deletable_by(@unprivileged_user)
    end
  end
end
