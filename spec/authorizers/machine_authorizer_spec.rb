# frozen_string_literal: true
require 'rails_helper'

describe MachineAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
    @unprivileged_user_other = create :user
    @admin_user_other = create :user
  end

  context 'machine action' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @admin_user.add_role :organization_admin, @org1
      @admin_user_other.add_role :organization_admin, @org2
      @unprivileged_user.add_role :organization_reader, @org1
      @unprivileged_user_other.add_role :organization_reader, @org2
      @machine = create :machine, organization: @org1
      @options = { in: @org1 }
    end

    it 'try to create' do
      expect(Machine.authorizer).to be_creatable_by(@admin_user, @options)
      expect(Machine.authorizer).to_not be_creatable_by(@admin_user_other, @options)
      expect(Machine.authorizer).to_not be_creatable_by(@unprivileged_user, @options)
      expect(Machine.authorizer).to_not be_creatable_by(@unprivileged_user_other, @options)
    end

    it 'try to delete' do
      expect(@machine.authorizer).to be_deletable_by(@admin_user)
      expect(@machine.authorizer).to_not be_deletable_by(@admin_user_other)
      expect(@machine.authorizer).to_not be_deletable_by(@unprivileged_user)
      expect(@machine.authorizer).to_not be_deletable_by(@unprivileged_user_other)
    end

    it 'try to update' do
      expect(@machine.authorizer).to be_updatable_by(@admin_user)
      expect(@machine.authorizer).to_not be_updatable_by(@admin_user_other)
      expect(@machine.authorizer).to_not be_updatable_by(@unprivileged_user)
      expect(@machine.authorizer).to_not be_updatable_by(@unprivileged_user_other)
    end

    it 'try to read from class level' do
      expect(Machine.authorizer).to be_readable_by(@admin_user, @options)
      expect(Machine.authorizer).to_not be_readable_by(@admin_user_other, @options)
      expect(Machine.authorizer).to be_readable_by(@unprivileged_user, @options)
      expect(Machine.authorizer).to_not be_readable_by(@unprivileged_user_other, @options)
    end

    it 'try to read from instance level' do
      expect(@machine.authorizer).to be_readable_by(@admin_user)
      expect(@machine.authorizer).to_not be_readable_by(@admin_user_other)
      expect(@machine.authorizer).to be_readable_by(@unprivileged_user)
      expect(@machine.authorizer).to_not be_readable_by(@unprivileged_user_other)
    end
  end
end
