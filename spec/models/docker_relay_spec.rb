# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_relays
#
#  id                            :integer          not null, primary key
#  organization_id               :integer          not null
#  public_key                    :text             not null
#  queue_name                    :string           not null
#  state                         :integer          default("online"), not null
#  last_heartbeat                :datetime         not null
#  verified                      :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  key_signature                 :string           not null
#  file_encryption_key_encrypted :string
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_b84167028c  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'aasm/rspec'
require 'models/shared_examples/organization_error_watcher'

RSpec.describe DockerRelay, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:organization) }
    it { should validate_presence_of(:public_key) }
    it { should have_readonly_attribute(:organization_id) }
    it 'validates the length of the file encryption key' do
      relay = DockerRelay.new file_encryption_key: Base64.strict_encode64(OpenSSL::PKey::RSA.new(1024).to_pem)
      relay.valid?
      expect(relay.errors.messages[:file_encryption_key]).to include('must be an RSA key of length 4096')
    end

    context 'on update' do
      before :each do
        @relay = create :docker_relay
      end

      it 'validates the presence of the queue_name' do
        @relay.queue_name = nil
        expect(@relay.valid?).to eq(false)
      end

      it 'validates the presence of the state attribute' do
        @relay.state = nil
        expect(@relay.valid?).to eq(false)
      end

      it 'validates the presence of the last heartbeat' do
        @relay.last_heartbeat = nil
        expect(@relay.valid?).to eq(false)
      end
    end
  end

  context 'when executing callbacks' do
    context 'before validation' do
      it 'generates a file encryption key if one is not specified' do
        relay = DockerRelay.new
        expect(relay.file_encryption_key).to eq(nil)
        relay.valid?
        expect(relay.file_encryption_key).to_not eq(nil)
      end
    end

    context 'on create' do
      it 'sets verified to false by default' do
        @relay = create :docker_relay, verified: true
        expect(@relay.verified).to eq(false)
      end

      it 'sets verified to true if organization is configured to do so' do
        org = create :organization
        org.configuration.update_column(:auto_approve_docker_relays, true)
        relay = create :docker_relay, verified: false, organization: org
        expect(relay.verified).to eq(true)
      end

      it 'generates a queue name' do
        @relay = build :docker_relay, queue_name: nil
        expect(@relay.queue_name).to be(nil)
        @relay.save!
        expect(@relay.queue_name).to_not be(nil)
      end

      it 'checks for RelayErrors after a Relay is created' do
        org = create :organization
        expect(RelayError).to receive(:check).with(org)
        create :docker_relay, organization: org
      end

      it 'checks for MachineUnreachableError after a Relay is created' do
        relay = build :docker_relay
        expect(UnreachableMachineError).to receive(:check).with(relay.organization)
        relay.save!
      end

      context 'when encrypting sensitive values' do
        it 'stores the file_encryption_key as an encrypted value' do
          relay = create :docker_relay, verified: true
          expect(relay.file_encryption_key_encrypted).to_not eq(relay.file_encryption_key)
        end
      end
    end

    context 'on save' do
      before :each do
        @key1 = OpenSSL::PKey::RSA.new(File.read('./spec/support/key1_for_testing.pem'))
        @key2 = OpenSSL::PKey::RSA.new(File.read('./spec/support/key2_for_testing.pem'))
        @relay = build :docker_relay, public_key: Base64.encode64(@key1.public_key.to_pem).tr("\n", '')
      end

      it 'generates the key signature based on the provided public key' do
        expect(@relay.key_signature).to eq(nil)
        @relay.save!
        expected = '94:fc:0a:f5:9e:ac:86:b9:ca:0f:59:ad:6a:9a:76:1a:45:9c:f6:5c:a1:36:2a:73:32:97:37:60:06:2d:95:1a'
        expect(@relay.key_signature).to eq(expected)
      end

      it 'updates the key signature if the key is changed' do
        @relay.save!
        expected = 'd3:38:0f:a5:66:0d:b6:40:c7:42:7d:dc:87:3c:ff:42:19:c4:c0:05:6a:c1:5b:8f:07:7d:84:64:cc:a8:8c:65'
        expect(@relay.key_signature).to_not eq(expected)
        @relay.public_key = Base64.encode64(@key2.public_key.to_pem).tr("\n", '')
        @relay.save!
        @relay.reload
        expect(@relay.key_signature).to eq(expected)
      end

      it 'checks for RelayErrors after a Relay is updated' do
        expect(RelayError).to receive(:check).with(@relay.organization)
        @relay.verified = true
        @relay.save!
      end

      it 'checks for MachineUnreachableError after a Relay is verified' do
        expect(UnreachableMachineError).to receive(:check).with(@relay.organization)
        @relay.verified = true
        @relay.save!
      end
    end

    context 'on destroy' do
      before :each do
        @relay = create :docker_relay
      end

      it 'checks for RelayErrors after a Relay is destroyed' do
        expect(RelayError).to receive(:check).with(@relay.organization)
        @relay.destroy!
      end

      it 'checks for MachineUnreachableError after a Relay is destroyed' do
        expect(UnreachableMachineError).to receive(:check).with(@relay.organization)
        @relay.destroy!
      end
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:organization) }
  end

  context 'when defining constants' do
    it 'should have a key length of 4096 bits' do
      expect(DockerRelay::FILE_ENCRYPTION_KEY_LENGTH).to eq(4096)
    end
  end

  context 'instance methods' do
    context '#process_heartbeat' do
      before :each do
        @offline_relay = create :docker_relay, state: :offline
      end

      it 'updates the last_heartbeat time' do
        old_heartbeat = @offline_relay.last_heartbeat
        @offline_relay.process_heartbeat
        expect(@offline_relay.last_heartbeat).to be > old_heartbeat
      end

      it 'turns the Relay online if it were offline' do
        expect(@offline_relay.online?).to be(false)
        @offline_relay.process_heartbeat
        expect(@offline_relay.online?).to be(true)
      end
    end
  end

  context 'state machine' do
    before :each do
      @relay = create :docker_relay
      @offline_relay = create :docker_relay, state: :offline
    end

    it 'initializes in an online state' do
      expect(@relay.online?).to eq true
    end

    it 'only allows a transition to online if the Relay is offline' do
      expect(@relay).to transition_from(:offline).to(:online).on_event(:go_online)
    end

    it 'only allows a transition to offline if the Relay is online' do
      expect(@relay).to transition_from(:online).to(:offline).on_event(:go_offline)
    end

    context 'sending relay notifications' do
      include ActiveJob::TestHelper

      before :each do
        clear_enqueued_jobs
        clear_performed_jobs
      end

      it 'sends an email after the relay comes online' do
        admin = create :user
        admin.add_role :organization_admin, @relay.organization
        @relay.update_column(:state, 0) # Force the Relay offline
        perform_enqueued_jobs do
          @relay.go_online
          email = ActionMailer::Base.deliveries.last
          expect(email.to).to include("#{admin.uid}@cisco.com")
          expect(email.subject).to match(/online/i)
        end
      end

      it 'sends an email after the relay goes offline' do
        admin = create :user
        admin.add_role :organization_admin, @relay.organization
        perform_enqueued_jobs do
          @relay.update_column(:state, 1)
          @relay.go_offline
          email = ActionMailer::Base.deliveries.last
          expect(email.to).to include("#{admin.uid}@cisco.com")
          expect(email.subject).to match(/offline/i)
        end
      end
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end
end
