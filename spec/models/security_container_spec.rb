# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_containers
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  category               :integer          default("whitebox"), not null
#  prog_args              :string           not null
#  default_config         :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  multi_host             :boolean          default(FALSE), not null
#  test_types             :string           default([]), not null, is an Array
#  configurable           :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  help_url               :string
#
# Indexes
#
#  index_security_containers_on_common_service_type_id  (common_service_type_id)
#  index_security_containers_on_name                    (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (common_service_type_id => common_service_types.id)
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe SecurityContainer, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:name) }
    it { should allow_value(/\A[A-z][A-z0-9-]+\z/.random_example).for(:name) }
    it { should validate_presence_of(:test_types) }

    it 'should verify that the machine parameter is present' do
      sc = build :security_container, prog_args: '-p %{port}'
      expect(sc.valid?).to be false
      expect(sc.errors.messages[:prog_args]).to include 'must specify target parameter'
      sc.prog_args += ' %{target}'
      sc.valid?
      expect(sc.errors.messages[:prog_args]).to eq([])
    end

    it 'enforces that the whole_host test type is mutually exclusive' do
      sc = SecurityContainer.new
      sc.test_types << 'whole_host'
      sc.valid?
      expect(sc.errors.messages[:test_types]).to eq([])
      sc.test_types << 'ssl_crypto'
      sc.valid?
      expect(sc.errors.messages[:test_types]).to include('can only contain the whole_host option if it is specified')
    end

    it 'requires at least one test type to be specified' do
      sc = SecurityContainer.new
      expect(sc.test_types.empty?).to eq(true)
      sc.valid?
      expect(sc.errors.messages[:test_types]).to include('must specify at least one type')
      sc.test_types << 'whole_host'
      sc.valid?
      expect(sc.errors.messages[:test_types]).to eq([])
    end

    it 'requires test types to be in the valid test types list' do
      sc = SecurityContainer.new(test_types: ['blah'])
      sc.valid?
      expect(sc.errors.messages[:test_types].first).to match(/must only contain the following/)
    end

    it 'requires the default_config value to be a Hash' do
      sc = SecurityContainer.new(default_config: [])
      sc.valid?
      expect(sc.errors.messages[:default_config]).to include('must be a Hash')
      sc.default_config = {}
      sc.valid?
      expect(sc.errors.messages[:default_config]).to eq([])
    end
  end

  context 'when maintaining associations' do
    it { should have_many(:assessments) }
  end

  context 'when executing callbacks' do
    describe 'before validation' do
      it 'sets default_config to an empty Hash if it is nil' do
        sc = SecurityContainer.new(default_config: nil)
        sc.valid?
        expect(sc.default_config).to eq({})
        sc.default_config = { 'a' => 1 }
        sc.valid?
        expect(sc.default_config).to eq('a' => 1)
      end
    end
  end
end
