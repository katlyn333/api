# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  uid        :string           not null
#  firstname  :string
#  lastname   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#  index_users_on_uid    (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'when validating' do
    it { should validate_presence_of :uid }
    it { should validate_presence_of :email }
    it { should allow_value(/\A[A-Za-z]/.random_example).for(:uid) }
  end

  context 'when maintaining associations' do
    it { should have_many(:organizations).through(:memberships) }
    it { should have_one(:api_token) }
  end

  context 'when executing callbacks' do
    it 'creates a default org for new users' do
      user = create :user
      org = user.organizations.where(uid: "#{user.uid}-org-#{Organization::DEFAULT_SUFFIX}").first
      expect(org).to_not eq nil
      expect(user.has_role?(:organization_admin, org)).to be true
    end
  end
end
