# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: machine_scan_schedules
#
#  id         :integer          not null, primary key
#  machine_id :integer
#  period     :integer          default("daily"), not null
#  at         :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_machine_scan_schedules_on_machine_id  (machine_id)
#
# Foreign Keys
#
#  fk_rails_6b47f8c66a  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/scan_schedule'

RSpec.describe MachineScanSchedule, type: :model do
  it_should_behave_like 'a Scan Schedule' do
    let(:schedule_type) { :machine_scan_schedule }
    let(:schedule_for) { :machine }
  end
end
