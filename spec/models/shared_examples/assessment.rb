# frozen_string_literal: true
require 'rails_helper'

RSpec.shared_examples 'an Assessment' do
  context 'validations' do
    it { should have_readonly_attribute(:identifier) }

    it 'should validate that the provided identifier is a SHA1 hash' do
      assessment = described_class.new identifier: OpenSSL::Digest::SHA1.hexdigest('').gsub(/\A\w/, 'Z')
      assessment.valid?
      expect(assessment.errors.messages[:identifier]).to include 'must be a SHA1 hash'
    end

    it 'should allow blank values for the identifier' do
      assessment = described_class.new identifier: nil
      assessment.valid?
      expect(assessment.errors.messages[:identifier]).to eq([])
      assessment = described_class.new identifier: ''
      assessment.valid?
      expect(assessment.errors.messages[:identifier]).to eq([])
    end

    it { should validate_presence_of(:machine) }
    it { should validate_presence_of(:docker_command) }
    it { should validate_presence_of(:security_container) }
    it { should validate_presence_of(:security_container_secret) }

    it 'requires the presence of service when associated with a non-whole-host security container' do
      security_container = create(:security_container, test_types: ['authenticated'])
      assessment = build assessment_type, security_container: security_container, service: nil
      assessment.valid?
      expect(assessment.errors.messages[:service]).to include("can't be blank")
    end

    it 'allows the service to be blank when associated with a whole-host security container' do
      security_container = create(:security_container, test_types: ['whole_host'])
      assessment = build assessment_type, security_container: security_container, service: nil
      assessment.valid?
      expect(assessment.errors.messages[:service]).to be_empty
    end
  end

  context 'when maintaing associations' do
    it { should have_many(:results) }
    it { should belong_to(:docker_command) }
    it { should belong_to(:machine) }
    it { should belong_to(:service) }
    it { should belong_to(:security_container) }
    it { should belong_to(:security_container_secret) }
  end

  context 'class constants' do
    it 'sets the SHA1 hex length' do
      expect(described_class::SHA1_HEX_LENGTH).to eq(40)
    end

    it 'defines the ranking of the statuses' do
      expect(described_class::RANKED_ASSESSMENT_STATUSES).to eq([:erroring, :failing, :warning, :passing, :informing])
    end

    it 'defines the hour at which an assessment becomes stale' do
      expect(described_class::STALE_HOUR).to eq(48)
    end
  end

  context 'public instance methods' do
    before :each do
      @assessment = create assessment_type
    end

    it 'uses the identifier attribute for URLs via the #to_param method' do
      expect(@assessment.to_param).to eq(@assessment.identifier)
    end

    describe 'the #timeout method' do
      before :each do
        @assessment.timeout!
      end

      it 'creates a result to indicate the timeout' do
        expect(@assessment.results.first.nid).to eq('norad:timeout')
      end

      it 'moves the assessment to a completed state' do
        expect(@assessment.state).to eq('complete')
      end
    end

    describe 'the #create_result_set! method' do
      before :each do
        @results_array = Array.new(3) do
          attributes_for(:result, assessment: nil)
        end
      end

      it 'creates a batch of results for the assessment' do
        @assessment.create_result_set!(@results_array)
        expect(@assessment.results.pluck(:nid)).to eq(@results_array.map { |r| r[:nid] })
      end

      it 'moves the assessment to a completed state' do
        @assessment.create_result_set!(@results_array)
        expect(@assessment.state).to eq('complete')
      end

      it 'applies an Ignore Rule after the results are created' do
        machine = @assessment.machine
        create :result_ignore_rule, signature: @results_array.sample[:signature], ignore_scope: machine
        ignore_query = ResultIgnoreRule.select(:signature).for_ignore_scopes(machine, machine.organization)
        rule_applicator = double('IgnoreRuleApplicator')
        allow(IgnoreRuleApplicator).to receive(:new).with(ignore_query, @assessment) do
          rule_applicator
        end

        expect(rule_applicator).to receive(:apply)
        @assessment.create_result_set!(@results_array)
      end
    end

    it 'returns the requirements associated with the assessment via #associated_requirements' do
      enforcement = create :enforcement, organization: @assessment.machine.organization
      requirement = create :requirement, requirement_group: enforcement.requirement_group
      create :provision, requirement: requirement, security_container: @assessment.security_container
      expect(@assessment.associated_requirements.first).to eq(requirement)
    end

    it 'returns the URL path via #results_path' do
      expect(@assessment.results_path).to match(%r{/.*/#{@assessment.identifier}})
    end

    it 'returns the associated results via #latest_results' do
      result = double('Result')
      allow(@assessment).to receive(:results).and_return([result])
      expect(@assessment.latest_results).to eq([result])
    end
  end

  context 'public class methods' do
    context 'for finding instances' do
      it 'provides a custom definiton for ::find' do
        assessment = create assessment_type
        id = assessment.id
        identifier = assessment.identifier
        expect(described_class.find(id)).to eq assessment
        expect(described_class.find(identifier)).to eq assessment
      end

      it 'raises an exception if unable to retrieve the object' do
        assessment = create assessment_type
        id = assessment.id
        identifier = assessment.identifier
        expect(described_class.find(id)).to eq assessment
        expect(described_class.find(identifier)).to eq assessment

        id = assessment.id + 1_000_000
        identifier = "#{assessment.identifier}-somerandomjunk"
        expect { described_class.find(id) }.to raise_error(ActiveRecord::RecordNotFound)
        expect { described_class.find(identifier) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    it 'returns a count of each result state' do
      machine = create :machine
      container = create :security_container
      command = create :docker_command, commandable: machine, containers: [container.name]
      eass = create assessment_type, machine: machine, docker_command: command
      iass = create assessment_type, machine: machine, docker_command: command
      create :result, nid: 'serverspec:123456', status: :pass, assessment: eass
      create :result, nid: 'serverspec:123456', status: :fail, assessment: eass
      create :result, nid: 'serverspec:123456', status: :info, assessment: iass
      create :result, nid: 'serverspec:123456', status: :error, assessment: iass
      h = machine.latest_assessment_stats
      expect(h[:passing]).to eq 1
      expect(h[:failing]).to eq 1
      expect(h[:erroring]).to eq 1
      expect(h[:informing]).to eq 1
    end

    it 'returns the latest set of assessments' do
      machine = create :machine
      container = create :security_container, category: :whitebox, prog_args: '%{target} %{ssh_key} %{ssh_user}'
      command = create :docker_command, commandable: machine, containers: [container.name]
      Array.new(3) do
        create assessment_type, machine: machine, docker_command: command
      end
      command = create :docker_command, commandable: machine, containers: [container.name]
      assessments = Array.new(3) do
        create assessment_type, machine: machine, docker_command: command
      end.to_a.select(&:id)
      expect(described_class.latest.to_a.select(&:id)).to match_array(assessments)
    end

    it 'provides the .stale method for finding old assessments' do
      assessment = create assessment_type
      assessment.start!

      stale_assessments = Array.new(3) do
        assessment = create assessment_type
        assessment.state = :in_progress
        assessment.state_transition_time = (described_class::STALE_HOUR + 3).hours.ago
        assessment.save!
        assessment
      end

      expect(described_class.stale.to_a).to match_array(stale_assessments)
    end

    describe 'the .most_recent_by_docker_command method' do
      it 'defaults to returning assessments for the 5 most recent docker commands' do
        assessments = Array.new(5) do
          create assessment_type
        end
        expected_dc_ids = assessments.map(&:docker_command_id)
        returned_ids = described_class.most_recent_by_docker_command_id.pluck(:docker_command_id)
        expect(returned_ids).to match_array(expected_dc_ids)
      end

      it 'can return assessments for the n most recent docker commands' do
        assessments = Array.new(10) do
          create assessment_type
        end
        expected_dc_ids = assessments.map(&:docker_command_id)
        returned_ids = described_class.most_recent_by_docker_command_id(10).pluck(:docker_command_id)
        expect(returned_ids).to match_array(expected_dc_ids)
      end
    end

    it 'provides the .for_docker_command method for looking up assesments by docker command' do
      create assessment_type
      dc1 = create :docker_command, containers: [create(:security_container).name]
      dc1_assessments = Array.new(3) do
        create assessment_type, docker_command: dc1
      end

      expect(described_class.for_docker_command(dc1)).to match_array(dc1_assessments)
    end

    describe 'the .status_from_stats method' do
      it 'returns pending if no other statuses exist' do
        # Build a stats hash
        h = described_class::RANKED_ASSESSMENT_STATUSES.each_with_object({}) do |s, memo|
          memo[s] = 0
        end
        expect(described_class.status_from_stats(h)).to eq(:pending)
      end

      it 'returns the first positive status it encounters' do
        random_status = described_class::RANKED_ASSESSMENT_STATUSES.sample
        # Build a stats hash
        h = described_class::RANKED_ASSESSMENT_STATUSES.each_with_object({}) do |s, memo|
          memo[s] = s != random_status ? 0 : 1
        end
        expect(described_class.status_from_stats(h)).to eq(random_status)
      end
    end
  end

  context 'when executing callbacks' do
    context 'before create' do
      it 'initializes the transition timestamp to match the creation time' do
        assessment = build assessment_type, state_transition_time: nil
        assessment.save!
        expect(assessment.state_transition_time).to eq(assessment.created_at)
      end
    end

    context 'around create' do
      it 'generates an identifier' do
        assessment = build assessment_type, identifier: nil
        expect(assessment.identifier).to be nil
        assessment.save!
        expect(assessment.identifier).to_not be nil
      end
    end
  end

  context 'the state machine' do
    before :each do
      @assessment = create assessment_type
    end

    it 'updates the state transition time after reach transition' do
      expect(@assessment.state_transition_time).to eq(@assessment.created_at)
      old_time = @assessment.state_transition_time
      @assessment.start!
      expect(@assessment.state_transition_time).to_not eq(old_time)
      old_time = @assessment.state_transition_time
      @assessment.complete!
      expect(@assessment.state_transition_time).to_not eq(old_time)
    end
  end
end
