# frozen_string_literal: true
RSpec.shared_examples 'a Scan Schedule' do
  context 'when validating' do
    it { should validate_presence_of(schedule_for) }
    it { should validate_presence_of(:at) }

    let(:valid_format_error_msg) { 'time must be in 24-hour format, e.g. HH:MM' }

    context 'the at attribute for weekly scans' do
      before :each do
        @sched = described_class.new(period: 'weekly')
      end

      let(:specify_day_error_msg) { 'must specify a day to run weekly scans, e.g. Mon 12:32' }

      it 'requires the day to be present' do
        @sched.at = '12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include('must specify a day to run weekly scans, e.g. Mon 12:32')
      end

      it 'requires the hour to be 0 <= n < 24' do
        @sched.at = 'Mon 25:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'verifies the minutes are 0 <= n < 60' do
        @sched.at = 'Mon 2:61'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'verifies that only numeric values are provided' do
        @sched.at = 'Mon 2*:2a'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'allows two digits to represent the hour' do
        @sched.at = 'Mon 24:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'allows one digit to represent the hour' do
        @sched.at = 'Mon 4:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'requires that the day portion begins with the first 3 letters of a valid day' do
        @sched.at = 'Mon 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'allows the day of the week to not be abbreviated' do
        @sched.at = 'Monday 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'allows the day of the week to be truncated' do
        @sched.at = 'Monda 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'ignores capitalization' do
        @sched.at = 'FRIDAY 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'requires the first letter of the day to be present' do
        @sched.at = 'riday 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(specify_day_error_msg)
      end

      it 'does not allow non-day values for the day' do
        @sched.at = 'nonsense 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(specify_day_error_msg)
      end
    end

    context 'the at attribute for daily scans' do
      before :each do
        @sched = described_class.new(period: 'daily')
      end

      it 'allows a valid 24 hour timestamp with no day provided' do
        @sched.at = '12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'rejects the input if a day is present' do
        @sched.at = 'Mon 12:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include('cannot specify a day of the week for daily scans')
      end

      it 'requires the hour to be 0 <= n < 24' do
        @sched.at = '35:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'requires the minutes to be 0 <= n < 60' do
        @sched.at = '2:61'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'requires numeric values' do
        @sched.at = '2*:2a'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'requires two digits for the minutes value' do
        @sched.at = '2:2'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to include(valid_format_error_msg)
      end

      it 'allows two digits to represent the hour' do
        @sched.at = '24:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end

      it 'allows one digit to represent the hour' do
        @sched.at = '4:20'
        @sched.valid?
        expect(@sched.errors.messages[:at]).to eq([])
      end
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(schedule_for) }
  end

  context 'when implementing template methods' do
    it 'should provide implementation for scan_target' do
      sched = build schedule_type
      expect { sched.scan_target }.to_not raise_error
    end
  end

  context 'when executing callbacks' do
    context 'after commit', with_resque_doubled: true do
      let(:schedule_name) { "#{described_class.name.underscore}_#{@sched.id}" }

      describe 'on save' do
        let(:schedule_config) do
          {
            class: 'ScheduleScanJob',
            args: @sched.to_global_id.to_s,
            cron: @sched.send(:cron_string),
            persist: true
          }
        end

        it 'should schedule future runs with Resque when creating a new schedule' do
          @sched = create schedule_type
          expect(@resque_module_double).to have_received(:set_schedule).with(schedule_name, schedule_config)
        end
      end

      describe 'on destroy' do
        it 'should remove the schedule' do
          @sched = create schedule_type
          @sched.destroy!
          expect(@resque_module_double).to have_received(:remove_schedule).with(schedule_name)
        end
      end
    end
  end
end
