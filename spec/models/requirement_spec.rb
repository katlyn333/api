# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: requirements
#
#  id                   :integer          not null, primary key
#  name                 :string
#  description          :string
#  requirement_group_id :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_requirements_on_requirement_group_id  (requirement_group_id)
#
# Foreign Keys
#
#  fk_rails_cce12a8273  (requirement_group_id => requirement_groups.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe Requirement, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
