# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: local_authentication_records
#
#  id                       :integer          not null, primary key
#  password_digest          :string
#  password_reset_token     :string
#  password_reset_sent_at   :datetime
#  authentication_method_id :integer
#  email_confirmation_token :string
#  email_confirmed_at       :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_local_authentication_records_on_authentication_method_id  (authentication_method_id) UNIQUE
#  index_local_authentication_records_on_email_confirmation_token  (email_confirmation_token) UNIQUE
#  index_local_authentication_records_on_email_confirmed_at        (email_confirmed_at)
#  index_local_authentication_records_on_password_reset_sent_at    (password_reset_sent_at)
#  index_local_authentication_records_on_password_reset_token      (password_reset_token) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe LocalAuthenticationRecord, type: :model do
  context 'when validating' do
    it { should validate_length_of(:password).is_at_least(8) }
    it { should validate_presence_of(:password_digest) }
  end

  context 'when maintaining associations' do
    it { should belong_to(:local_authentication_method) }
    it { should have_one(:user) }
  end

  context 'when managing passwords' do
    before :each do
      @user = create(:user_with_password).reload
    end

    context 'when confirmed' do
      it 'can change password' do
        @user.confirm!
        @user.reset_password!
        @user.local_authentication_record.provided_token = @user.local_authentication_record.password_reset_token
        @user.local_authentication_record.password = 'password12'
        @user.local_authentication_record.password_confirmation = 'password12'
        expect(@user.local_authentication_record.save).to be true
        expect(@user.local_authentication_record.authenticate('password12').id).to eq @user.id
      end
    end

    context 'when unconfirmed' do
      it "can't change password" do
        @user.reset_password!
        @user.local_authentication_record.provided_token = @user.local_authentication_record.password_reset_token
        @user.local_authentication_record.password = 'password12'
        @user.local_authentication_record.password_confirmation = 'password12'
        expect { @user.local_authentication_record.save! }.to raise_exception ActiveRecord::RecordInvalid
      end
    end

    context "when token isn't valid" do
      it "can't change password" do
        @user.confirm!
        old_token = @user.local_authentication_record.password_reset_token
        @user.reset_password!
        @user.local_authentication_record.provided_token = old_token
        @user.local_authentication_record.password = 'password12'
        @user.local_authentication_record.password_confirmation = 'password12'
        expect { @user.local_authentication_record.save! }.to raise_exception ActiveRecord::RecordInvalid
      end
    end
  end
end
