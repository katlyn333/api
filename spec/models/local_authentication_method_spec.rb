# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe LocalAuthenticationMethod, type: :model do
  context 'when maintaining associations' do
    it { should have_one(:local_authentication_record) }
  end
end
