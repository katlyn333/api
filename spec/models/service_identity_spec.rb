# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: service_identities
#
#  id                 :integer          not null, primary key
#  username_encrypted :string
#  password_encrypted :string
#  service_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_service_identities_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_b8cfe24029  (service_id => services.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe ServiceIdentity, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:service) }
  end

  context 'when maintaining associations' do
    it { should belong_to(:service) }
  end
end
