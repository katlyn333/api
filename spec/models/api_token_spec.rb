# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: api_tokens
#
#  id         :integer          not null, primary key
#  value      :string           not null
#  state      :integer          default("active"), not null
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_api_tokens_on_user_id  (user_id)
#  index_api_tokens_on_value    (value) UNIQUE
#
# Foreign Keys
#
#  fk_rails_f16b5e0447  (user_id => users.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe ApiToken, type: :model do
end
