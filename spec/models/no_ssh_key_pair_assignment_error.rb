# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/organization_errors'

RSpec.describe NoSshKeyPairAssignmentError, type: :model do
  it_behaves_like 'an Organization Error class'

  before :each do
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
    allow_any_instance_of(SshKeyPair).to receive(:check_for_organization_errors)
    allow_any_instance_of(SshKeyPairAssignment).to receive(:check_for_organization_errors)
    allow_any_instance_of(SecurityContainerConfig).to receive(:check_for_organization_errors)
    allow_any_instance_of(Machine).to receive(:check_for_organization_errors)
    @organization = create :organization
    @machine = create :machine, organization: @organization
  end

  def errors_for_org(org = @organization)
    org.organization_errors.where(type: described_class.name).reload
  end

  def create_authenticated_test_config(opts)
    create :security_container_config, { enabled_outside_of_requirement: true, security_container: @test }.merge(opts)
  end

  def check_for_error(org = @organization)
    described_class.check org
  end

  describe '::check method' do
    context 'error creation' do
      before :each do
        @test = create :security_container, test_types: ['authenticated']
      end

      context 'when removing the only SSH Key Pair Assignment from a machine in an Organization' do
        before :each do
          @keypair = create :ssh_key_pair_assignment, machine: @machine
        end

        it 'creates an error when authenticated tests are enabled at the organization level' do
          create_authenticated_test_config(organization: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          @machine.ssh_key_pair_assignment.destroy!
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end

        it 'creates an error when authenticated tests are enabled for a machine in the organization' do
          create_authenticated_test_config(organization: nil, machine: create(:machine, organization: @organization))
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          @machine.ssh_key_pair_assignment.destroy!
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end
      end

      context 'when security containter configs are saved' do
        before :each do
          expect(@machine.ssh_key_pair_assignment).to be(nil)
        end

        it 'creates an error if a config is created for an organization' do
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          create_authenticated_test_config(organization: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end

        it 'creates an error if a config is created for a machine' do
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          create_authenticated_test_config(organization: nil, machine: create(:machine, organization: @organization))
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end

        it 'creates an error if a config is enabled for an organization' do
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          config = create_authenticated_test_config(organization: @organization)
          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          config.update_column(:enabled_outside_of_requirement, true)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end

        it 'creates an error if a config is enabled for an organization' do
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          config = create_authenticated_test_config(
            organization: nil, machine: create(:machine, organization: @organization)
          )
          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)

          config.update_column(:enabled_outside_of_requirement, true)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)
        end
      end
    end

    context 'error removal' do
      before :each do
        @test = create :security_container, test_types: ['authenticated']
      end

      context 'when an SSH Key Pair is assigned' do
        it 'removes the error when organization level config exists' do
          create_authenticated_test_config(organization: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          create :ssh_key_pair_assignment, machine: @machine
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config exists' do
          create_authenticated_test_config(organization: nil, machine: create(:machine, organization: @organization))
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          create :ssh_key_pair_assignment, machine: @machine
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end

      context 'when test configs are removed' do
        it 'removes the error when an organization level config is destroyed' do
          config = create_authenticated_test_config(organization: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.destroy
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config is destroyed' do
          config = create_authenticated_test_config(
            organization: nil, machine: create(:machine, organization: @organization)
          )
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.destroy
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end

      context 'when test configs are disabled' do
        it 'removes the error when an organization level config is disabled' do
          config = create_authenticated_test_config(organization: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config is disabled' do
          config = create_authenticated_test_config(
            organization: nil, machine: create(:machine, organization: @organization)
          )
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end
    end
  end
end
