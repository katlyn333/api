# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: services
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  description            :text
#  port                   :integer          not null
#  port_type              :integer          default("tcp"), not null
#  encryption_type        :integer          default("cleartext"), not null
#  machine_id             :integer
#  type                   :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  allow_brute_force      :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  discovered             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_common_service_type_id  (common_service_type_id)
#  index_services_on_machine_id              (machine_id)
#  index_services_on_machine_id_and_port     (machine_id,port) UNIQUE
#  index_services_on_type                    (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (common_service_type_id => common_service_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe SshService, type: :model do
  context 'when executing callbacks' do
    context 'before validating' do
      before :each do
        @ssh = build :ssh_service
      end

      it 'ensures the port type is TCP' do
        @ssh.port_type = :udp
        @ssh.valid?
        expect(@ssh.port_type).to eq('tcp')
      end

      it 'ensures the encryption type is SSH' do
        @ssh.encryption_type = :ssl
        @ssh.valid?
        expect(@ssh.encryption_type).to eq('ssh')
      end

      it 'sets the port to 22 if none is set' do
        @ssh.port = nil
        @ssh.valid?
        expect(@ssh.port).to eq(22)
      end
    end

    context 'before saving' do
      it 'is always associated with the ssh service type' do
        ssh = build :ssh_service, port: 1234
        expect(ssh.common_service_type).to be(nil)
        ssh.save!
        cst = CommonServiceType.find_by(name: 'ssh', port: 22, transport_protocol: 'tcp')
        expect(ssh.common_service_type).to eq(cst)
      end
    end
  end
end
