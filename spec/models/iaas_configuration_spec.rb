# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: iaas_configurations
#
#  id              :integer          not null, primary key
#  provider        :integer          not null
#  user_encrypted  :string
#  key_encrypted   :string
#  region          :string
#  project         :string
#  auth_url        :string
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_iaas_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_a8120b8d08  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe IaasConfiguration, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:organization) }
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:key) }
    it { should validate_presence_of(:provider) }

    context 'aws configuration' do
      before :each do
        @config = IaasConfiguration.new(provider: 'aws')
      end

      it 'validates presence of region' do
        @config.valid?
        expect(@config.errors.messages[:region]).to include("can't be blank")
      end

      it 'validates absence of auth_url' do
        @config.auth_url = 'blah'
        @config.valid?
        expect(@config.errors.messages[:auth_url]).to include('must be blank')
      end

      it 'validates absence of project' do
        @config.project = 'blah'
        @config.valid?
        expect(@config.errors.messages[:project]).to include('must be blank')
      end
    end

    context 'openstack configuration' do
      before :each do
        @config = IaasConfiguration.new(provider: 'openstack')
        @config.valid?
      end

      it 'validates presence of auth_url' do
        expect(@config.errors.messages[:auth_url]).to include("can't be blank")
      end

      it 'validates presence of project' do
        expect(@config.errors.messages[:project]).to include("can't be blank")
      end

      it 'validates the format of auth_url' do
        expect(@config.errors.messages[:auth_url]).to include('must end with /tokens')
      end
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:organization) }
  end
end
