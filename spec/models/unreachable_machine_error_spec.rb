# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/organization_errors'

RSpec.describe UnreachableMachineError, type: :model do
  it_behaves_like 'an Organization Error class'

  before :each do
    allow_any_instance_of(Machine).to receive(:check_for_organization_errors)
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
    @organization = create :organization
  end

  describe '::check method' do
    context 'error creation' do
      context 'when no primary Relay exists' do
        before :each do
          expect(@organization.primary_relay).to be(nil)
          expect(@organization.unreachable_machine_error).to be(nil)
        end

        it 'creates an error when a machine with an RFC 1918 IP address is added to an organization' do
          create :machine, ip: '10.1.1.1', organization: @organization
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to_not be(nil)
        end

        it 'creates an error when an existing machine is updated to have an RFC 1918 IP address' do
          machine = create :machine, ip: '128.107.33.76', organization: @organization
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to be(nil)
          machine.ip = '192.168.1.100'
          machine.save!
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to_not be(nil)
        end
      end

      context 'when Relays are modified' do
        before :each do
          @relay = create(:docker_relay, organization: @organization, state: 'online').tap do |r|
            r.update_column(:verified, true)
          end
          create :machine, ip: '192.168.1.100', organization: @organization
          described_class.check(@organization)
          expect(@organization.unreachable_machine_error).to be(nil)
        end

        context "when an Organization's primary Relay is unverified" do
          it 'creates an error if a machine has an RFC 1918 IP address' do
            expect(@relay.verified).to eq(true)
            @relay.verified = false
            @relay.save!
            described_class.check(@organization)
            expect(@organization.reload.unreachable_machine_error).to_not be(nil)
          end
        end

        context "when an Organization's only verified Relay is removed" do
          it 'creates an error if a machine has an RFC 1918 IP address' do
            @relay.destroy
            described_class.check(@organization)
            expect(@organization.reload.unreachable_machine_error).to_not be(nil)
          end
        end
      end
    end

    context 'error removal' do
      context 'when no primary Relay exists' do
        before :each do
          expect(@organization.primary_relay).to be(nil)
          @machine = create :machine, ip: '10.1.1.1', organization: @organization
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to_not be(nil)
        end

        it 'removes an error when a machine with an RFC 1918 IP address is removed from an organization' do
          @machine.destroy
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to be(nil)
        end

        it 'creates an error when an existing machine is updated to have an RFC 1918 IP address' do
          @machine.ip = '128.107.33.76'
          @machine.save!
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to be(nil)
        end
      end

      context 'when Relays are modified' do
        before :each do
          create :machine, ip: '192.168.1.100', organization: @organization
          described_class.check(@organization)
          expect(@organization.unreachable_machine_error).to_not be(nil)
        end

        it 'removes the error from an Organization with unreachable machines when a Relay is added' do
          create(:docker_relay, organization: @organization, state: 'online').tap do |r|
            r.update_column(:verified, true)
          end
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to be(nil)
        end

        it 'removes the error from an Organization with unreachable machines when a Relay is verified' do
          relay = create(:docker_relay, organization: @organization, state: 'online')
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to_not be(nil)
          relay.verified = true
          relay.save!
          described_class.check(@organization)
          expect(@organization.reload.unreachable_machine_error).to be(nil)
        end
      end
    end
  end
end
