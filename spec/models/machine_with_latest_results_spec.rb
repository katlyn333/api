# frozen_string_literal: true
require 'rails_helper'
require 'models/shared_examples/ignore_scope'

RSpec.describe MachineWithLatestResults do
  it 'returns all results for the most recent command via #latest_results' do
    machine = create :machine
    sectest = create :security_container
    command = create :docker_command, commandable: machine, containers: [sectest.name]
    assessment = create :assessment, docker_command: command, machine: machine
    result = create :result, assessment: assessment
    allow(machine).to receive(:latest_command).and_return(command)
    decorated_machine = described_class.new(machine)

    expect(decorated_machine.latest_results).to match_array(Result.where(id: result))
  end
end
