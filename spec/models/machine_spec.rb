# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: machines
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  ip              :inet
#  fqdn            :string
#  description     :text
#  machine_status  :integer          default("pending"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  name            :string           not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/organization_error_watcher'
require 'models/shared_examples/ignore_scope'
require 'models/shared_examples/protected_object'

RSpec.describe Machine, type: :model do
  context 'when validating' do
    # Since we enforce some not null constraints, let's build a valid machine for the Shoulda
    # matchers to use.
    subject { build(:machine) }

    it { should validate_uniqueness_of(:name).scoped_to(:organization_id) }
    it { should validate_length_of(:name).is_at_most(36) }
  end

  context 'when validating with custom methods' do
    describe 'IP range provided' do
      let(:machine) { build :machine, ip: '10.0.0.0/8' }
      it 'should not be valid ' do
        expect(machine).to_not be_valid
        expect(machine.errors[:ip]).to include('address must be a valid, single IPv4 address')
      end
    end

    describe 'invalid IP address provided' do
      let(:machine) { build :machine, ip: '256.256.256.256' }
      it 'is prevented from being set at the database layer' do
        expect(machine.ip).to be(nil)
      end
    end

    describe 'properly formatted, single IP address' do
      let(:machine) { build :machine, ip: '192.168.1.1' }
      it 'should be valid' do
        expect(machine).to be_valid
      end
    end
  end

  context 'when maintaining associations' do
    it { should have_one(:ssh_key_pair).through(:ssh_key_pair_assignment) }
    it { should have_one(:ssh_key_pair_assignment) }
    it { should have_many(:service_discoveries) }
    it { should have_many(:result_ignore_rules).dependent(:destroy) }
  end

  context 'when executing callbacks' do
    context 'before valiation' do
      it 'generates a UUID for a name if none is provided' do
        machine = Machine.new(name: nil)
        expect(machine.name).to be(nil)
        # Run the before_valiation callbacks
        machine.valid?
        expect(machine.name).to_not be(nil)
      end
    end

    context 'after saving' do
      it 'checks for the Unreachable Machine Error after a machine is created' do
        machine = build :machine
        expect(UnreachableMachineError).to receive(:check).with(machine.organization)
        machine.save!
      end

      it 'checks for the Unreachable Machine Error after a machine is updated' do
        machine = create :machine
        expect(UnreachableMachineError).to receive(:check).with(machine.organization)
        machine.name = 'new name'
        machine.save!
      end
    end

    context 'after destroying' do
      it 'checks for the necessity of the Unreachable Machine Error after a machine is destroyed' do
        machine = create :machine
        expect(UnreachableMachineError).to receive(:check).with(machine.organization)
        machine.destroy
      end
    end
  end

  context 'public class methods' do
    def create_assessment(machine, command, type, results)
      assessment = create type, machine: machine, docker_command: command
      results.each do |r|
        create :result, nid: 'qualys:qid1234', status: r, assessment: assessment
      end
      assessment
    end

    it 'returns a count of each result state for regardless of assessment type' do
      machine = create :machine
      container = create :security_container
      command = create :docker_command, commandable: machine, containers: [container.name]
      create_assessment machine, command, :white_box_assessment, [:pass, :fail]
      create_assessment machine, command, :white_box_assessment, [:info, :error]
      create_assessment machine, command, :black_box_assessment, [:pass, :fail]
      create_assessment machine, command, :black_box_assessment, [:info, :error]
      h = machine.latest_assessment_stats
      expect(h[:passing]).to eq 2
      expect(h[:failing]).to eq 2
      expect(h[:erroring]).to eq 2
      expect(h[:informing]).to eq 2
    end

    context 'assessment statuses' do
      before :each do
        container = create :security_container
        @machine = create :machine
        @command = create :docker_command, commandable: @machine, containers: [container.name]
      end

      it 'marks machine as erroring in presence of error status' do
        create_assessment @machine, @command, :white_box_assessment, [:pass, :fail, :warn]
        create_assessment @machine, @command, :white_box_assessment, [:info, :error]
        create_assessment @machine, @command, :black_box_assessment, [:pass, :fail, :warn]
        create_assessment @machine, @command, :black_box_assessment, [:info, :error]
        expect(@machine.assessment_status).to eq :erroring
      end

      it 'marks machine as failing in presence of fail status' do
        create_assessment @machine, @command, :white_box_assessment, [:pass, :fail, :warn]
        create_assessment @machine, @command, :white_box_assessment, [:info, :pass]
        create_assessment @machine, @command, :black_box_assessment, [:pass, :fail, :warn]
        create_assessment @machine, @command, :black_box_assessment, [:info, :pass]
        expect(@machine.assessment_status).to eq :failing
      end

      it 'marks machine as warning in presence of warn status' do
        create_assessment @machine, @command, :white_box_assessment, [:pass, :warn]
        create_assessment @machine, @command, :white_box_assessment, [:info, :pass]
        create_assessment @machine, @command, :black_box_assessment, [:pass, :warn]
        create_assessment @machine, @command, :black_box_assessment, [:info, :pass]
        expect(@machine.assessment_status).to eq :warning
      end

      it 'marks machine as passing in presence of passing (and optionally, info) status' do
        create_assessment @machine, @command, :white_box_assessment, [:pass, :pass]
        create_assessment @machine, @command, :white_box_assessment, [:info, :pass]
        create_assessment @machine, @command, :black_box_assessment, [:pass, :pass]
        create_assessment @machine, @command, :black_box_assessment, [:info, :pass]
        expect(@machine.assessment_status).to eq :passing
      end

      it 'marks machine as informing in presence of only info status' do
        create_assessment @machine, @command, :white_box_assessment, [:info, :info]
        create_assessment @machine, @command, :black_box_assessment, [:info, :info]
        expect(@machine.assessment_status).to eq :informing
      end

      it 'marks machine as pending when lacking results' do
        expect(@machine.assessment_status).to eq :pending
      end
    end
  end

  context 'when receiving instance methods' do
    context '#ssh_key_values' do
      before :each do
        org = create :organization
        @config = org.configuration
        @machine = create :machine, organization: org
      end

      it 'returns the values of an SshConfig object if Organization is configured to use them' do
        @config.use_relay_ssh_key = false
        @config.save!
        ssh_key = create :ssh_key_pair, organization: @machine.organization
        @machine.ssh_key_pair = ssh_key

        values = @machine.ssh_key_values

        expect(values[:ssh_user]).to eq(ssh_key.username)
        expect(values[:ssh_key]).to eq(ssh_key.key)
      end

      it 'returns a placeholder value if Organization is configured to use Relay values' do
        @config.use_relay_ssh_key = true
        @config.save!
        ssh_key = create :ssh_key_pair, organization: @machine.organization
        @machine.ssh_key_pair = ssh_key

        values = @machine.ssh_key_values

        expect(values[:ssh_user]).to eq('%{ssh_user}')
        expect(values[:ssh_key]).to eq('%{ssh_key}')
      end
    end

    it 'returns the latest effective docker command via #latest_command' do
      machine = create :machine
      create :docker_command, commandable: machine
      org_command = create :docker_command, commandable: machine.organization

      expect(machine.latest_command).to eq(org_command)
    end

    it 'returns the object it inherits its RBAC from via #rbac_parent' do
      machine = build_stubbed :machine
      expect(machine.rbac_parent).to eq(machine.organization)
    end
  end

  context 'deleting a machine' do
    it 'deletes machine and docker commands' do
      machine = create :machine
      container = create :security_container
      command = create :docker_command, commandable: machine, containers: [container.name]

      Machine.find(command.machine_id).destroy!

      expect { Machine.find(command.machine_id) }.to raise_error(ActiveRecord::RecordNotFound)
      expect { command.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end

  it_behaves_like 'an Ignore Scope'
  it_behaves_like 'a Protected Object'
end
