# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_ignore_rules
#
#  id                :integer          not null, primary key
#  ignore_scope_id   :integer          not null
#  ignore_scope_type :string           not null
#  signature         :string           not null
#  comment           :text
#  created_by        :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_result_ignore_rules_on_i_s_type_and_i_s_id                (ignore_scope_type,ignore_scope_id)
#  index_result_ignore_rules_on_i_s_type_and_i_s_id_and_signature  (ignore_scope_type,ignore_scope_id,signature) UNIQUE
#  index_result_ignore_rules_on_signature                          (signature)
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/protected_object'

RSpec.describe ResultIgnoreRule, type: :model do
  describe 'associations' do
    it { should belong_to(:ignore_scope) }
  end

  describe 'attribute characteristics' do
    it { should have_readonly_attribute(:signature) }
  end

  describe 'validations' do
    it { should validate_presence_of(:signature) }
    it { should allow_value(OpenSSL::Digest::SHA256.new.hexdigest).for(:signature) }
    it { should_not allow_value(OpenSSL::Digest::SHA1.new.hexdigest).for(:signature) }

    it { should validate_presence_of(:created_by) }
  end

  describe 'class methods' do
    describe '.for_ignore_scopes' do
      before :each do
        @machine = create :machine
        @org_ignore_rule = create :result_ignore_rule, ignore_scope: @machine.organization
        @machine_ignore_rule = create :result_ignore_rule, ignore_scope: @machine
      end

      it 'returns the ignore rules for all scopes passed to it' do
        rules = described_class.for_ignore_scopes(@machine.organization, @machine)
        expect(rules).to match_array([@org_ignore_rule, @machine_ignore_rule])
      end

      it 'returns the ignore rules for only the one scope passed to it' do
        rules = described_class.for_ignore_scopes(@machine.organization)
        expect(rules).to match_array([@org_ignore_rule])
      end
    end
  end

  describe 'callbacks' do
    before :each do
      @rule_applicator = double('IgnoreRuleApplicator')
      allow(@rule_applicator).to receive_messages([:apply, :undo])
    end

    describe 'after creating' do
      it 'sends the #apply method to the instance of IgnoreRuleApplicator' do
        rule = build :result_ignore_rule
        allow(IgnoreRuleApplicator).to receive(:new).with(rule.signature, rule.ignore_scope) do
          @rule_applicator
        end
        expect(@rule_applicator).to receive(:apply)
        rule.save!
      end
    end

    describe 'after destroying' do
      it 'notifies the Result class to unignore matching results' do
        # Since this is a destroy callback, we unfortunately need to first run through the creation
        # process
        allow(IgnoreRuleApplicator).to receive(:new).and_return(@rule_applicator)
        rule = create :result_ignore_rule

        # Now let's test the destroy callback
        allow(IgnoreRuleApplicator).to receive(:new).with(rule.signature, rule.ignore_scope) do
          @rule_applicator
        end
        expect(@rule_applicator).to receive(:undo)
        rule.destroy!
      end
    end
  end

  it_behaves_like 'a Protected Object'
end
