# frozen_string_literal: true
require 'rails_helper'
require 'query_objects/shared_examples/results_query'
require 'query_objects/shared_examples/query_object'

RSpec.describe ResultsQuery do
  it_behaves_like 'a Query Object'

  it_behaves_like 'a Results Query Object'

  it 'returns a result for a given command via #for_command' do
    sectest = create :security_container
    command = create :docker_command, containers: [sectest.name]
    assessment = create :assessment, docker_command: command, security_container: sectest
    result = create :result, assessment: assessment
    create :result # create a result not tied to the previous objects
    query = described_class.new

    expect(query.for_commands(command)).to match_array(Result.where(id: result.id))
  end
end
