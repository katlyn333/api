# frozen_string_literal: true
require 'rails_helper'
require 'query_objects/shared_examples/query_object'

RSpec.describe MachineCommandsQuery do
  it_behaves_like 'a Query Object' do
    let(:subject) { described_class.new(double) }
  end

  it 'returns Docker Commands for a set of machines via #relation' do
    machine = create :machine
    command1 = create :docker_command, commandable: machine
    command2 = create :docker_command, commandable: machine
    create :docker_command # create a command not tied to the machine
    query = MachineCommandsQuery.new(Machine.where(id: machine))

    expect(query.relation).to match_array(DockerCommand.where(id: [command1, command2]))
  end

  it 'returns a unique docker command for each machine via #unique_commands' do
    machine1 = create :machine
    machine2 = create :machine, organization: machine1.organization
    2.times do
      create :docker_command, commandable: machine1
      create :docker_command, commandable: machine2
    end
    query = MachineCommandsQuery.new(Machine.where(id: [machine1, machine2]))

    expect(query.unique_commands.map(&:machine_id)).to match_array([machine1.id, machine2.id])
  end

  it 'returns unique commands newer than a cutoff' do
    machine1 = create :machine
    machine2 = create :machine, organization: machine1.organization
    create :docker_command, machine: machine1
    org_command = create :docker_command, commandable: machine1.organization
    machine_command2 = create :docker_command, commandable: machine2
    query = MachineCommandsQuery.new(Machine.where(id: [machine1, machine2]))
    allow(query).to receive(:unique_commands).and_return(DockerCommand.where(machine: [machine1, machine2]))

    new_commands = query.unique_commands_newer_than(org_command.created_at)
    expect(new_commands).to match_array(DockerCommand.where(id: machine_command2))
  end
end
