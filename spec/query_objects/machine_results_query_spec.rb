# frozen_string_literal: true
require 'rails_helper'
require 'query_objects/shared_examples/results_query'
require 'query_objects/shared_examples/query_object'

RSpec.describe MachineResultsQuery do
  it_behaves_like 'a Query Object' do
    let(:subject) { described_class.new(double) }
  end

  it_behaves_like 'a Results Query Object' do
    let(:subject) { described_class.new(double) }
  end

  it 'returns Results for a set of machines via the #relation method' do
    machine = create :machine
    assessment = create :assessment, machine: machine
    result = create :result, assessment: assessment
    create :result # create some other result that is not tied to the machine
    query = described_class.new(Machine.where(id: machine.id))

    expect(query.relation).to match_array(Result.where(id: result.id))
  end
end
