# frozen_string_literal: true
RSpec.shared_examples 'a Results Query Object' do
  describe 'instance methods' do
    it 'defines a #for_command method' do
      expect(subject).to respond_to(:for_commands)
    end
  end
end
