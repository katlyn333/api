# frozen_string_literal: true
RSpec.shared_examples 'a Query Object' do
  describe 'instance methods' do
    it 'defines a #relation method' do
      expect(subject).to respond_to(:relation)
    end
  end
end
