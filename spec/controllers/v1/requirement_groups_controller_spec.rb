# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::RequirementGroupsController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @admin_user = create :user
  end

  describe 'GET #index' do
    before :each do
      @rg_no_privs1 = create :requirement_group
      @rg_no_privs2 = create :requirement_group
      @rg_with_admin1 = create(:requirement_group).tap { |rg| @_current_user.add_role :requirement_group_admin, rg }
      @rg_with_admin2 = create(:requirement_group).tap { |rg| @_current_user.add_role :requirement_group_admin, rg }
    end

    it 'returns all requirement groups to any user' do
      norad_get :index, {}
      returned_ids = response_body['response'].map { |o| o['id'] }
      expect(returned_ids).to match_array([@rg_no_privs1.id, @rg_no_privs2.id, @rg_with_admin1.id, @rg_with_admin2.id])
      expect(response.status).to eq(200)
    end

    context 'when with_admin params set' do
      it 'returns only the requirement groups the current user has admin privileges on' do
        norad_get :index, with_admin: true
        returned_ids = response_body['response'].map { |o| o['id'] }
        expect(returned_ids).to match_array([@rg_with_admin1.id, @rg_with_admin2.id])
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'POST #create' do
    it 'makes a new Requirement Group' do
      payload = { requirement_group: { name: 'Test ReqGroup', description: 'Test ReqGroup' } }
      allow(controller).to receive(:current_user).and_return(@admin_user)
      expect { norad_post :create, payload }.to change(RequirementGroup, :count).by(1)
    end

    it 'exposes Requirement Group' do
      payload = { requirement_group: { name: 'Test ReqGroup', description: 'Test ReqGroup' } }
      allow(controller).to receive(:current_user).and_return(@admin_user)
      norad_post :create, payload
      expect(response).to match_response_schema('requirement_group')
    end

    it 'adds user as admin' do
      payload = { requirement_group: { name: 'Test ReqGroup', description: 'Test ReqGroup' } }
      allow(controller).to receive(:current_user).and_return(@admin_user)
      norad_post :create, payload
      req_group = RequirementGroup.find(response_body['response']['id'])
      expect(@admin_user.has_role?(:requirement_group_admin, req_group)).to be(true)
    end
  end

  describe 'PUT #update' do
    before :each do
      @rg_no_privs = create :requirement_group
      @rg_with_admin = create(:requirement_group).tap { |rg| @_current_user.add_role :requirement_group_admin, rg }
    end

    context 'as a requirement group admin' do
      it 'updates the name and description of the requirement group' do
        new_name = @rg_with_admin.name + SecureRandom.hex
        norad_put :update, id: @rg_with_admin.to_param, requirement_group: { name: new_name }
        expect(@rg_with_admin.reload.name).to eq(new_name)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('requirement_group')
      end
    end

    context 'as an unprivileged user' do
      it 'cannot update the name and description of the requirement group' do
        new_name = @rg_no_privs.name + SecureRandom.hex
        norad_put :update, id: @rg_no_privs.to_param, requirement_group: { name: new_name }
        expect(@rg_no_privs.reload.name).to_not eq(new_name)
        expect(response.status).to eq(403)
      end
    end
  end
end
