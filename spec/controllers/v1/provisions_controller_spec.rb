# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ProvisionsController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @req_group = create :requirement_group
    @req_group.requirements << build(:requirement, requirement_group: @req_group)
    @requirement = @req_group.requirements.first
    @sec_contain = create :security_container
  end

  def create_payload
    { requirement_id: @requirement.to_param, provision: { security_container_id: @sec_contain.to_param } }
  end

  describe 'POST #create' do
    context 'as Requirement Group admin' do
      before :each do
        @_current_user.add_role :requirement_group_admin, @req_group
      end

      it 'creates a new provision' do
        payload = create_payload
        expect { norad_post :create, payload }.to change(Provision, :count).by(1)
      end

      it 'exposes provision' do
        payload = create_payload
        norad_post :create, payload
        expect(response).to match_response_schema('provision')
      end
    end

    context 'as non-admin user' do
      it 'cannot create a new provision' do
        payload = create_payload
        expect { norad_post :create, payload }.to change(Provision, :count).by(0)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @prov = create :provision, security_container: @sec_contain, requirement: @requirement
    end

    context 'as Requirement Group admin' do
      before :each do
        @_current_user.add_role :requirement_group_admin, @req_group
      end

      it 'deletes a provision' do
        expect { norad_delete :destroy, id: @prov }.to change(@requirement.provisions, :count).by(-1)
      end
    end

    context 'as non-admin' do
      it 'does not delete a provision' do
        expect { norad_delete :destroy, id: @prov }.to change(@requirement.provisions, :count).by(0)
      end
    end
  end
end
