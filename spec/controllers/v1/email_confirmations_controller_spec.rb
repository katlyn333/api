# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::EmailConfirmationsController, type: :controller do
  include NoradControllerTestHelpers
  include ActiveJob::TestHelper

  before :each do
    mock_auth_type :local
    @user = create(:user_with_password).reload
  end

  after(:each) { mock_auth_type :reverse_proxy }

  describe 'POST #create' do
    context 'for valid token' do
      let(:confirmation_params) do
        { email_confirmation_token: @user.reload.local_authentication_record.email_confirmation_token }
      end

      it 'should confirm email' do
        perform_enqueued_jobs do
          expect(@user.confirmed?).to be false
          expect { norad_post :create, confirmation_params }.to change(ActionMailer::Base.deliveries, :count).by(1)
          expect(@user.reload.confirmed?).to be true
          expect(response.status).to eq 204
        end
      end
    end

    context 'for invalid token' do
      let(:confirmation_params) do
        { email_confirmation_token: 'blah' }
      end

      it 'should not confirm email' do
        perform_enqueued_jobs do
          expect(@user.confirmed?).to be false
          expect { norad_post :create, confirmation_params }
            .to raise_exception(ActiveRecord::RecordNotFound)
            .and change(ActionMailer::Base.deliveries, :count).by(0)
          expect(@user.reload.confirmed?).to be false
        end
      end
    end
  end
end
