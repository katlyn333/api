# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::MachineScanSchedulesController, type: :controller, with_resque_doubled: true do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @machine = create :machine
      @sched = create :machine_scan_schedule, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the schedules associated with a machine' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_scan_schedules')
      end
    end

    context 'as an organization reader' do
      it 'returns the schedules associated with a machine' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_scan_schedules')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of schedules for a machine' do
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @machine = create :machine
      @sched = create :machine_scan_schedule, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the details of a schedule' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :show, id: @sched.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_scan_schedule')
      end
    end

    context 'as an organization reader' do
      it 'read the details of a specific schedule' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :show, id: @sched.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_scan_schedule')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific schedule' do
        norad_get :show, id: @sched.id
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
    end

    let(:sched_params) { { machine_scan_schedule: { at: '24:24' } } }

    context 'as an organization admin' do
      it 'creates a new schedule for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        create_params = sched_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.scan_schedules, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_scan_schedule')
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new schedule for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        create_params = sched_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new schedule for a machine' do
        @_current_user.add_role :organization_admin, @machine2.organization
        create_params = sched_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @sched1 = create :machine_scan_schedule, machine: @machine1
      @sched2 = create :machine_scan_schedule, machine: @machine2
    end

    let(:sched_params) { { machine_scan_schedule: { at: '23:23' } } }

    context 'as an organization admin' do
      it 'updates a schedule for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@sched1.at).to eq('24:24')
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@sched1.reload.at).to eq('23:23')
      end
    end

    context 'as an organization reader' do
      it 'cannot update a schedule for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot update a machine in organization' do
        @_current_user.add_role :organization_admin, @machine2.organization
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @sched1 = create :machine_scan_schedule, machine: @machine1
      @sched2 = create :machine_scan_schedule, machine: @machine2
    end

    context 'as an organization admin' do
      it 'deletes a schedule for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect { norad_delete :destroy, id: @sched1 }.to change(@machine1.scan_schedules, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete a schedule for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect { norad_delete :destroy, id: @sched1 }.to change(@machine1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a schedule for a machine' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect { norad_delete :destroy, id: @sched1 }.to change(@machine1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
