# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ServiceIdentitiesController, type: :controller do
  include NoradControllerTestHelpers

  describe 'POST #create' do
    before :each do
      @machine1 = create :machine
      @service = create :service, machine: @machine1
      @machine2 = create :machine
    end

    let(:identity_params) { { service_identity: attributes_for(:service_identity) } }

    context 'as an organization admin' do
      it 'creates a new identity for a service' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@service.service_identity).to be(nil)
        create_params = identity_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.service_identity).to_not be(nil)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service_identity')
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new identity for a service' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@service.service_identity).to be(nil)
        create_params = identity_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.service_identity).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new identity for a service' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect(@service.service_identity).to be(nil)
        create_params = identity_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.service_identity).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @service1 = create :service, machine: @machine1
      @service2 = create :service, machine: @machine2
      @identity = create :service_identity, service: @service1
      @new_username = "user-#{SecureRandom.hex}"
    end

    let(:identity_params) { { service_identity: { username: @new_username } } }

    context 'as an organization admin' do
      it 'updates  an identity for a service' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@identity.username).to_not eq(@new_username)
        update_params = identity_params.merge(id: @identity.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@identity.reload.username).to eq(@new_username)
      end
    end

    context 'as an organization reader' do
      it 'cannot update identity for a service' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@identity.username).to_not eq(@new_username)
        update_params = identity_params.merge(id: @identity.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot identity for a service' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect(@identity.username).to_not eq(@new_username)
        update_params = identity_params.merge(id: @identity.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end
end
