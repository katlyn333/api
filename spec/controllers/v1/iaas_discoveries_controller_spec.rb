# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::IaasDiscoveriesController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      create :iaas_configuration, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of iaas_discoveries for an organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('iaas_discoveries')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot read the details of a specific iaas_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific iaas_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      config = create :iaas_configuration, organization: @org1
      @iaas_discovery_org1 = create :iaas_discovery, iaas_configuration: config
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of a specific iaas_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @iaas_discovery_org1.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('iaas_discovery')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot read the details of a specific iaas_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :show, id: @iaas_discovery_org1
        expect(response.status).to eq(403)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific iaas_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @iaas_discovery_org1
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      create :iaas_configuration, organization: @org1
      @org2 = create :organization
      create :iaas_configuration, organization: @org2
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can create new iaas_discovery for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@org1.iaas_discoveries.first).to be(nil)
        norad_post :create, organization_id: @org1.to_param
        expect(@org1.iaas_discoveries.first).not_to be(nil)
        expect(response.status).to eq(200)
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot create new iaas_discovery for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        expect(@org1.iaas_discoveries.first).to be(nil)
        norad_post :create, organization_id: @org1.to_param
        expect(@org1.iaas_discoveries.first).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot create new iaas_discovery for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@org1.iaas_discoveries.first).to be(nil)
        norad_post :create, organization_id: @org1.to_param
        expect(@org1.iaas_discoveries.first).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end
end
