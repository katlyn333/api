# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::SecurityContainerConfigsController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @org = create :organization
    @machine = create :machine, organization_id: @org.id
  end

  describe 'GET #index' do
    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      it 'exposes all organization-level configs' do
        @org_config = create :security_container_config, configurable: @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_security_container_configs')
      end

      it 'exposes all machine-level configs' do
        @machine_config = create :security_container_config, configurable: @machine
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_security_container_configs')
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      it 'exposes all organization-level configs' do
        @org_config = create :security_container_config, configurable: @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_security_container_configs')
      end

      it 'exposes all machine-level configs' do
        @machine_config = create :security_container_config, configurable: @machine
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_security_container_configs')
      end
    end

    context 'as a user outside organization' do
      it 'exposes all organization-level configs' do
        @org_config = create :security_container_config, configurable: @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(403)
      end

      it 'exposes all machine-level configs' do
        @machine_config = create :security_container_config, configurable: @machine
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      it 'exposes a config at organization-level' do
        config = create :security_container_config, configurable: @org
        norad_get :show, id: config.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_security_container_config')
      end

      it 'exposes a config at machine-level' do
        config = create :security_container_config, configurable: @machine
        norad_get :show, id: config.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_security_container_config')
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      it 'exposes a config at organization-level' do
        config = create :security_container_config, configurable: @org
        norad_get :show, id: config.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_security_container_config')
      end

      it 'exposes a config at machine-level' do
        config = create :security_container_config, configurable: @machine
        norad_get :show, id: config.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine_security_container_config')
      end
    end

    context 'as a user outside organization' do
      it 'responds with error message at organization-level' do
        config = create :security_container_config, configurable: @org
        norad_get :show, id: config.to_param
        expect(response.status).to eq(403)
      end

      it 'responds with error message at machine-level' do
        config = create :security_container_config, configurable: @machine
        norad_get :show, id: config.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @container = create(:security_container,
                          prog_args: '-p %{port} %{target}',
                          default_config: { port: '443' })
    end

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      context 'with valid params' do
        let(:creation_params) do
          {
            security_container_config: {
              values: { port: 4443 },
              security_container_id: @container.id
            }
          }
        end

        it 'adds a config for a container at Organization-level' do
          expect do
            norad_post :create, creation_params.merge(organization_id: @org.to_param)
          end.to change(@org.security_container_configs, :count).by(1)
          expect(response).to match_response_schema('organization_security_container_config')
        end

        it 'adds a config for a container at Machine-level' do
          expect do
            norad_post :create, creation_params.merge(machine_id: @machine.to_param)
          end.to change(@machine.security_container_configs, :count).by(1)
          expect(response).to match_response_schema('machine_security_container_config')
        end
      end

      context 'with invalid params' do
        let(:creation_params) do
          {
            security_container_config: {
              values: { junk: 4443 },
              security_container_id: @container.id
            }
          }
        end

        it 'creates nothing at organization-level and an error is returned' do
          expect do
            norad_post :create, creation_params.merge(organization_id: @org.to_param)
          end.to change(@org.security_container_configs, :count).by(0)
          expect(response.status).to eq(422)
        end

        it 'creates nothing at machine-level and an error is returned' do
          expect do
            norad_post :create, creation_params.merge(machine_id: @machine.to_param)
          end.to change(@machine.security_container_configs, :count).by(0)
          expect(response.status).to eq(422)
        end
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      let(:creation_params) do
        {
          security_container_config: {
            values: { port: 4443 },
            security_container_id: @container.id
          }
        }
      end

      it 'adds a config for a container at Organization-level' do
        expect do
          norad_post :create, creation_params.merge(organization_id: @org.to_param)
        end.to change(@org.security_container_configs, :count).by(0)
      end

      it 'adds a config for a container at Machine-level' do
        expect do
          norad_post :create, creation_params.merge(machine_id: @machine.to_param)
        end.to change(@machine.security_container_configs, :count).by(0)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @container = create :security_container
    end

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      it 'deletes a config at organization-level' do
        config = create :security_container_config, security_container: @container, configurable: @org
        expect do
          norad_delete :destroy, id: config.to_param
        end.to change(@org.security_container_configs, :count).by(-1)
      end

      it 'deletes a config at machine-level' do
        config = create :security_container_config, security_container: @container, configurable: @machine
        expect do
          norad_delete :destroy, id: config.to_param
        end.to change(@machine.security_container_configs, :count).by(-1)
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      it 'deletes a config at organization-level' do
        config = create :security_container_config, security_container: @container, configurable: @org
        expect do
          norad_delete :destroy, id: config.to_param
        end.to change(@org.security_container_configs, :count).by(0)
      end

      it 'deletes a config at machine-level' do
        config = create :security_container_config, security_container: @container, configurable: @machine
        expect do
          norad_delete :destroy, id: config.to_param
        end.to change(@machine.security_container_configs, :count).by(0)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      container = create :security_container, prog_args: '-p %{port} %{target}', default_config: { port: '443' }
      @config = create :security_container_config, security_container: container, configurable: @org
      @machine_config = create :security_container_config, security_container: container, configurable: @machine
    end

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      context 'with valid params' do
        let(:new_attributes) do
          { values: { port: '4443' } }
        end

        it 'allows updating the config at organization-level' do
          expect(@config.values['port']).to_not eq new_attributes[:values][:port]
          norad_put :update, id: @config.to_param, security_container_config: new_attributes
          @config.reload
          expect(@config.values['port']).to eq new_attributes[:values][:port]
          expect(response).to match_response_schema('organization_security_container_config')
        end

        it 'allows updating the config at machine-level' do
          expect(@machine_config.values['port']).to_not eq new_attributes[:values][:port]
          norad_put :update, id: @machine_config.to_param, security_container_config: new_attributes
          @machine_config.reload
          expect(@machine_config.values['port']).to eq new_attributes[:values][:port]
          expect(response).to match_response_schema('machine_security_container_config')
        end
      end

      context 'with invalid params' do
        let(:invalid_attributes) do
          { values: { junky: '4443' } }
        end

        it 'responds with an error message at organization-level' do
          norad_put :update, id: @config.to_param, security_container_config: invalid_attributes
          expect(response.status).to eq(422)
        end

        it 'responds with an error message at machine-level' do
          norad_put :update, id: @machine_config.to_param, security_container_config: invalid_attributes
          expect(response.status).to eq(422)
        end
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      let(:new_attributes) do
        { values: { port: '4443' } }
      end

      it 'responds with error message at organization-level' do
        expect(@config.values['port']).to_not eq new_attributes[:values][:port]
        norad_put :update, id: @config.to_param, security_container_config: new_attributes
        expect(response.status).to eq(403)
      end

      it 'responds with error message at machine-level' do
        expect(@config.values['port']).to_not eq new_attributes[:values][:port]
        norad_put :update, id: @machine_config.to_param, security_container_config: new_attributes
        expect(response.status).to eq(403)
      end
    end
  end
end
