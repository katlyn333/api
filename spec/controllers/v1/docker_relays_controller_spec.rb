# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_relays
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  public_key      :text             not null
#  queue_name      :string           not null
#  state           :integer          default(0), not null
#  last_heartbeat  :datetime         not null
#  verified        :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_public_key       (public_key) UNIQUE
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_696422edd6  (organization_id => organizations.id)
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::DockerRelaysController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @organization = create :organization
      @relays = Array.new(3).map do
        create :docker_relay, organization: @organization
      end
    end

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @organization
      end

      it 'returns the set of relays for the organization' do
        expect(@_current_user.has_role?(:organization_admin, @organization)).to eq(true)
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('docker_relays_without_key')
      end
    end

    context 'as a non organization admin' do
      it 'raises an AccessDenied exception' do
        expect(@_current_user.has_role?(:organization_admin, @organization)).to eq(false)
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @encoded_key = Base64.encode64(OpenSSL::PKey::RSA.new(4096).to_pem).tr("\n", '')
      @organization = create :organization
    end

    it 'requires a valid organization token' do
      old_relay_count = @organization.docker_relays.count
      invalid_token = @organization.token + 'ABCDEF'
      payload = { organization_token: invalid_token, docker_relay: { public_key: @encoded_key } }
      expect { norad_post :create, payload }.to raise_error(ActiveRecord::RecordNotFound)
      expect(old_relay_count).to eq(@organization.docker_relays.reload.count)
      payload = { organization_token: @organization.token, docker_relay: { public_key: @encoded_key } }
      expect { norad_post :create, payload }.to change(@organization.docker_relays, :count).by(1)
    end

    it 'requires the public key to be sent in the request' do
      payload = { organization_token: @organization.token }
      expect { norad_post :create, payload }.to raise_error(ActionController::ParameterMissing)
      payload = { organization_token: @organization.token, docker_relay: { public_key: @encoded_key } }
      expect { norad_post :create, payload }.to change(@organization.docker_relays, :count).by(1)
    end
  end

  describe 'PUT #update' do
    before :each do
      @relay = create :docker_relay
    end

    context 'an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @relay.organization
      end

      it 'can verify the agent' do
        expect(@relay.verified).to eq(false)
        norad_put(:update, id: @relay.to_param, docker_relay: { verified: true })
        @relay.reload
        expect(response).to match_response_schema('docker_relay_without_key')
        expect(@relay.verified).to eq(true)
      end

      it 'can un-verify the agent' do
        @relay.verified = true
        @relay.save!

        norad_put(:update, id: @relay.to_param, docker_relay: { verified: false })
        @relay.reload
        expect(response).to match_response_schema('docker_relay_without_key')
        expect(@relay.verified).to eq(false)
      end
    end

    context 'an unauthorized user' do
      it 'is unable to verify the agent' do
        expect(@relay.verified).to eq(false)
        norad_put :update, id: @relay.to_param, verified: true
        @relay.reload
        expect(response.status).to eq(403)
        expect(@relay.verified).to eq(false)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @relay = create :docker_relay
    end

    context 'an organization admin' do
      it 'can destroy the agent' do
        @_current_user.add_role :organization_admin, @relay.organization
        expect { norad_delete :destroy, id: @relay.to_param }.to change(DockerRelay, :count).by(-1)
      end
    end

    context 'an unauthorized user' do
      it 'cannot destroy the agent' do
        expect { norad_delete :destroy, id: @relay.to_param }.to change(DockerRelay, :count).by(0)
      end
    end

    context 'with authentication checks enforced' do
      before :each do
        allow(controller).to receive(:require_api_token).and_call_original
        @keypair = OpenSSL::PKey::RSA.new(4096)
        @relay = create :docker_relay, public_key: Base64.encode64(@keypair.public_key.to_pem).tr("\n", '')
      end

      it 'requires the request to have an API token header of an organization admin' do
        expect { norad_delete :destroy, id: @relay.to_param }.to change(DockerRelay, :count).by(0)
        expect(response.status).to be 401
        request.headers[:Authorization] = "Token token=#{@_current_user.api_token.value}"
        expect { norad_delete :destroy, id: @relay.to_param }.to change(DockerRelay, :count).by(0)
        expect(response.status).to be 403
        @_current_user.add_role :organization_admin, @relay.organization
        expect { norad_delete :destroy, id: @relay.to_param }.to change(DockerRelay, :count).by(-1)
        expect(response.status).to be 204
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @relay = create :docker_relay
    end

    context 'an organization admin' do
      it 'can lookup the relay' do
        @_current_user.add_role :organization_admin, @relay.organization
        norad_get :show, id: @relay.to_param
        expect(response).to match_response_schema('docker_relay_with_key')
      end
    end

    context 'an unauthorized user' do
      it 'cannot lookup the relay' do
        norad_get :show, id: @relay.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'with authentication checks enforced' do
      before :each do
        allow(controller).to receive(:require_api_token).and_call_original
        @keypair = OpenSSL::PKey::RSA.new(4096)
        @relay = create :docker_relay, public_key: Base64.encode64(@keypair.public_key.to_pem).tr("\n", '')
      end

      it 'requires the request to have an API token header of an organization admin' do
        norad_get :show, id: @relay.to_param
        expect(response.status).to be 401
        request.headers[:Authorization] = "Token token=#{@_current_user.api_token.value}"
        norad_get :show, id: @relay.to_param
        expect(response.status).to be 403
        @_current_user.add_role :organization_admin, @relay.organization
        norad_get :show, id: @relay.to_param
        expect(response).to match_response_schema('docker_relay_with_key')
      end
    end
  end
end
