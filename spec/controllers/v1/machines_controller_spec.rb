# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::MachinesController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine_org1 = create :machine, organization: @org1
      @machine_org2 = create :machine, organization: @org2
    end

    context 'as an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'read the set of machines for the organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machines')
      end
    end

    context 'as an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'read the set of machines for the organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(200)
        Thread.current['current_user'] = @_current_user
        expect(response).to match_response_schema('machines')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of machines for the organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, organization_id: @org1.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine_org1 = create :machine, organization: @org1
      @machine_org2 = create :machine, organization: @org2
    end

    context 'as an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'read the details of a specific machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @machine_org1.id
        expect(response.status).to eq(200)
        Thread.current['current_user'] = @_current_user
        expect(response).to match_response_schema('machine')
      end
    end

    context 'as an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'read the details of a specific machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :show, id: @machine_org1.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('machine')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @machine_org1
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
    end

    context 'as an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'create new machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { organization_token: @org1.token, machine: attributes_for(:machine) }
        expect { norad_post :create, payload }.to change(@org1.machines, :count).by(1)
        expect(response.status).to eq(200)
      end
    end

    context 'as an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot create new machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        payload = { organization_token: @org1.token, machine: attributes_for(:machine) }
        expect { norad_post :create, payload }.to change(@org1.machines, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot create new machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { organization_token: @org1.token, machine: attributes_for(:machine) }
        expect { norad_post :create, payload }.to change(@org1.machines, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine_org1 = create :machine, organization: @org1
      @machine_org2 = create :machine, organization: @org2
    end

    context 'as an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'update a machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { id: @machine_org1.id, machine: { description: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(200)
        @machine_org1.reload
        expect(@machine_org1.description).to eq('changed')
      end
    end

    context 'as an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot update a machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        payload = { id: @machine_org1.id, machine: { description: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot update a machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { id: @machine_org1.id, machine: { description: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine_org1 = create :machine, organization: @org1
      @machine_org2 = create :machine, organization: @org2
    end

    context 'as an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'delete a machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect { norad_delete :destroy, id: @machine_org1 }.to change(@org1.machines, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot delete a machine in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        expect { norad_delete :destroy, id: @machine_org1 }.to change(@org1.machines, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'cannot delete a machine in this organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect { norad_delete :destroy, id: @machine_org1.id }.to change(@org1.machines, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
