# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::SshKeyPairsController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @organization = create :organization
      @keypair = create :ssh_key_pair, organization: @organization
    end

    context 'as an organization admin' do
      it 'returns the keypairs associated with an organization' do
        @_current_user.add_role :organization_admin, @organization
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('ssh_key_pairs')
      end
    end

    context 'as an organization reader' do
      it 'returns the keypairs associated with an organization' do
        @_current_user.add_role :organization_reader, @organization
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('ssh_key_pairs')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of keypairs for an organization' do
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @organization = create :organization
      @keypair = create :ssh_key_pair, organization: @organization
    end

    context 'as an organization admin' do
      it 'returns the details of a keypair' do
        @_current_user.add_role :organization_admin, @organization
        norad_get :show, id: @keypair.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('ssh_key_pair')
      end
    end

    context 'as an organization reader' do
      it 'read the details of a specific keypair' do
        @_current_user.add_role :organization_reader, @organization
        norad_get :show, id: @keypair.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('ssh_key_pair')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific keypair' do
        norad_get :show, id: @keypair.id
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @organization1 = create :organization
      @organization2 = create :organization
    end

    let(:keypair_params) { { ssh_key_pair: attributes_for(:ssh_key_pair, organization: nil) } }

    context 'as an organization admin' do
      it 'creates a new keypair for an organization' do
        @_current_user.add_role :organization_admin, @organization1
        create_params = keypair_params.merge(organization_id: @organization1.to_param)
        expect { norad_post :create, create_params }.to change(@organization1.ssh_key_pairs, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('ssh_key_pair')
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new keypair for an organization' do
        @_current_user.add_role :organization_reader, @organization1
        create_params = keypair_params.merge(organization_id: @organization1.to_param)
        expect { norad_post :create, create_params }.to change(@organization1.ssh_key_pairs, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new keypair for an organization' do
        @_current_user.add_role :organization_admin, @organization2
        create_params = keypair_params.merge(organization_id: @organization1.to_param)
        expect { norad_post :create, create_params }.to change(@organization1.ssh_key_pairs, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @organization1 = create :organization
      @organization2 = create :organization
      @keypair = create :ssh_key_pair, organization: @organization1
      @new_name = "new_name_#{rand}"
    end

    let(:keypair_params) { { ssh_key_pair: attributes_for(:ssh_key_pair, organization: nil, name: @new_name) } }

    context 'as an organization admin' do
      it 'updates a keypair for an organization' do
        @_current_user.add_role :organization_admin, @organization1
        expect(@keypair.name).to_not eq(@new_name)
        update_params = keypair_params.merge(id: @keypair.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@keypair.reload.name).to eq(@new_name)
      end
    end

    context 'as an organization reader' do
      it 'cannot update a keypair for an organization' do
        @_current_user.add_role :organization_reader, @organization1
        expect(@keypair.name).to_not eq(@new_name)
        update_params = keypair_params.merge(id: @keypair.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot update a keypair in organization' do
        @_current_user.add_role :organization_reader, @organization2
        expect(@keypair.name).to_not eq(@new_name)
        update_params = keypair_params.merge(id: @keypair.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @organization1 = create :organization
      @organization2 = create :organization
      @keypair = create :ssh_key_pair, organization: @organization1
    end

    context 'as an organization admin' do
      it 'deletes a keypair for an organization' do
        @_current_user.add_role :organization_admin, @organization1
        expect { norad_delete :destroy, id: @keypair }.to change(@organization1.ssh_key_pairs, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete a keypair for an organization' do
        @_current_user.add_role :organization_reader, @organization1
        expect { norad_delete :destroy, id: @keypair }.to change(@organization1.ssh_key_pairs, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a keypair for an organization' do
        @_current_user.add_role :organization_admin, @organization2
        expect { norad_delete :destroy, id: @keypair }.to change(@organization1.ssh_key_pairs, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
