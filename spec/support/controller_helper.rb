# frozen_string_literal: true
require 'support/api_schema_matcher'
require 'support/norad_api_request_methods'

module NoradControllerTestHelpers
  include NoradApiRequestMethods

  def response_body
    JSON.parse(response.body)
  end

  def signed_request(verb, path, payload, signature)
    send(verb.to_sym, path, params: payload, headers: { 'HTTP_NORAD_SIGNATURE': signature })
  end
end
