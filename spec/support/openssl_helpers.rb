# frozen_string_literal: true
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
module OpensslHelpers
  def self.key(length = 4096)
    OpenSSL::PKey::RSA.new length
  end

  def self.csr(cn, local_key = key)
    csr = OpenSSL::X509::Request.new
    csr.version = 0
    csr.subject = OpenSSL::X509::Name.new(
      [
        ['CN', cn, OpenSSL::ASN1::UTF8STRING],
        ['C', 'USA', OpenSSL::ASN1::PRINTABLESTRING],
        ['ST', 'Tennessee', OpenSSL::ASN1::PRINTABLESTRING],
        ['L', 'Knoxville', OpenSSL::ASN1::PRINTABLESTRING],
        ['O', 'ASIG', OpenSSL::ASN1::UTF8STRING]
      ]
    )
    csr.public_key = local_key.public_key
    csr.sign(local_key, OpenSSL::Digest::SHA256.new)
  end

  def self.add_san(csr, san)
    extensions = [
      OpenSSL::X509::ExtensionFactory.new.create_extension('subjectAltName', san)
    ]

    # add SAN extension to the CSR
    attribute_values = OpenSSL::ASN1::Set [OpenSSL::ASN1::Sequence(extensions)]
    [
      OpenSSL::X509::Attribute.new('extReq', attribute_values),
      OpenSSL::X509::Attribute.new('msExtReq', attribute_values)
    ].each do |attribute|
      csr.add_attribute attribute
    end
  end

  def self.ca(ca_keypair = key)
    ca_cert = OpenSSL::X509::Certificate.new
    ca_cert.not_before = Time.now.to_i
    ca_cert.subject = OpenSSL::X509::Name.new(
      [
        ['CN', 'Norad Test CA', OpenSSL::ASN1::UTF8STRING],
        ['C', 'USA', OpenSSL::ASN1::PRINTABLESTRING],
        ['ST', 'Tennessee', OpenSSL::ASN1::PRINTABLESTRING],
        ['L', 'Knoxville', OpenSSL::ASN1::PRINTABLESTRING],
        ['O', 'ASIG', OpenSSL::ASN1::UTF8STRING]
      ]
    )
    ca_cert.issuer = ca_cert.subject
    ca_cert.not_after = Time.now.to_i + 300 # 5 minutes
    ca_cert.serial = 1
    ca_cert.public_key = ca_keypair.public_key
    ef = OpenSSL::X509::ExtensionFactory.new
    ef.subject_certificate = ca_cert
    ef.issuer_certificate = ca_cert
    ca_cert.add_extension(ef.create_extension('basicConstraints', 'CA:TRUE', true))
    ca_cert.add_extension(ef.create_extension('keyUsage', 'keyCertSign, cRLSign', true))
    ca_cert.add_extension(ef.create_extension('subjectKeyIdentifier', 'hash', false))
    ca_cert.add_extension(ef.create_extension('authorityKeyIdentifier', 'keyid:always', false))
    ca_cert.sign(ca_keypair, OpenSSL::Digest::SHA256.new)
  end

  def self.sign_certificate(cert_req, ca_cert, ca_keypair)
    cert = OpenSSL::X509::Certificate.new
    cert.subject = cert_req.subject
    cert.issuer = ca_cert.subject
    cert.not_before = Time.now.to_i
    cert.not_after = Time.now.to_i + 10
    cert.serial = SecureRandom.random_number 1_000_000
    cert.public_key = cert_req.public_key
    cert.sign ca_keypair, OpenSSL::Digest::SHA256.new
  end
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize
