# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: machines
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  ip              :inet
#  fqdn            :string
#  description     :text
#  machine_status  :integer          default("pending"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  name            :string           not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :machine do
    organization
    # 167772161 corresponds to '10.0.0.1'.  By using this method we should never run in to the situation
    # of an invalid IP address being generated.  Also rubocop made me write it with underscores.
    sequence(:ip) { |n| IPAddr.new(167_772_161 + n, Socket::AF_INET).to_s }
    sequence(:fqdn) { |n| "factory.server.local#{n}" }
    description 'Factory machine description'
    name { SecureRandom.uuid }
  end
end
