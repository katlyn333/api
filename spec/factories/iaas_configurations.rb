# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: iaas_configurations
#
#  id              :integer          not null, primary key
#  provider        :integer          not null
#  user_encrypted  :string
#  key_encrypted   :string
#  region          :string
#  project         :string
#  auth_url        :string
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_iaas_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_a8120b8d08  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :iaas_configuration do
    provider 1 # AWS
    user 'MyString'
    key 'MyString'
    region 'MyString'
    project nil # must be blank for AWS
    auth_url nil # must be blank for AWS
    organization
  end
end
