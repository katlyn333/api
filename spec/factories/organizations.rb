# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  uid        :string           not null
#  slug       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_slug  (slug) UNIQUE
#  index_organizations_on_uid   (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :organization do
    uid { "Organization #{SecureRandom.hex(8)}" }
    association :configuration, factory: :organization_configuration, strategy: :build
  end
end
