# frozen_string_literal: true
FactoryGirl.define do
  factory :unreachable_machine_error do
    organization
    type 'UnreachableMachineError'
    message UnreachableMachineError::MESSAGE
  end
end
