# frozen_string_literal: true
FactoryGirl.define do
  factory :local_authentication_method do
    before(:create) do |l|
      create :local_authentication_record, local_authentication_method: l
    end
  end
end
