# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_container_configs
#
#  id                             :integer          not null, primary key
#  security_container_id          :integer
#  enabled_outside_of_requirement :boolean          default(FALSE), not null
#  machine_id                     :integer
#  organization_id                :integer
#  values_encrypted               :string
#
# Indexes
#
#  index_security_container_configs_on_machine_id             (machine_id)
#  index_security_container_configs_on_organization_id        (organization_id)
#  index_security_container_configs_on_security_container_id  (security_container_id)
#
# Foreign Keys
#
#  fk_rails_191d669b7b  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7f035feecf  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_a879277af4  (security_container_id => security_containers.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :security_container_config do
    security_container
    configurable { FactoryGirl.build(:machine) }
    values { security_container.default_config }
    enabled_outside_of_requirement true
  end
end
