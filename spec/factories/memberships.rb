# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: memberships
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_memberships_on_organization_id              (organization_id)
#  index_memberships_on_user_id                      (user_id)
#  index_memberships_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_64267aab58  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_99326fb65d  (user_id => users.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :membership do
    organization
    user
  end
end
