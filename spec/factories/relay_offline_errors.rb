# frozen_string_literal: true
FactoryGirl.define do
  factory :relay_offline_error do
    organization
    type 'RelayOfflineError'
    message RelayOfflineError::MESSAGE
  end
end
