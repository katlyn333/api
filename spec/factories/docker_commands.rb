# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  containers            :json
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :docker_command do
    association :commandable, factory: :machine
    containers { [create(:security_container).name] }
  end
end
