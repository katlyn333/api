# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: results
#
#  id            :integer          not null, primary key
#  status        :integer          default("fail"), not null
#  output        :text
#  title         :string
#  description   :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  nid           :string           not null
#  sir           :integer          default("unevaluated"), not null
#  assessment_id :integer
#  ignored       :boolean          default(FALSE), not null
#  signature     :string           not null
#
# Indexes
#
#  index_results_on_assessment_id  (assessment_id)
#  index_results_on_signature      (signature)
#
# Foreign Keys
#
#  fk_rails_5e3335002f  (assessment_id => assessments.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :result do
    nid { "namespace:#{SecureRandom.hex(8)}" }
    sir 'medium'
    output 'Factory test output'
    title 'Factory result name'
    description 'Factory description name'
    status 'info'
    assessment { |a| a.association :white_box_assessment }
    signature { OpenSSL::Digest::SHA256.hexdigest(SecureRandom.hex) }
  end
end
