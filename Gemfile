# frozen_string_literal: true
source 'https://rubygems.org'

group :production, :development, :test, :client_test do
  # Application Gems
  gem 'pry-rails', '~> 0.3'
  gem 'rails', '5.0.1'

  gem 'active_model_serializers', '~> 0.10'

  # AMQP Client
  gem 'bunny', '~> 2.5'

  # RBAC
  gem 'authority', '~> 3.2'
  gem 'rolify', '~> 5.1'

  # Authn
  gem 'net-ldap', '~> 0.15'

  # To use ActiveModel has_secure_password
  gem 'bcrypt', '3.1.11'

  # Database Stuff
  gem 'pg', '0.18.4'

  # Secrets management
  gem 'dotenv-rails', '~> 2.1'

  # Vault encryption/decryption
  gem 'vault-rails', '~> 0.3.0'

  # Stuff for docker
  gem 'docker-api', '1.29.2'

  # Application server
  gem 'unicorn', '5.1.0'

  # State Machine
  gem 'aasm', '~> 4.10'

  # Middleware for blocking & throttling abusive requests
  gem 'rack-attack', '~> 5.0'
end

group :production do
  # Cache support
  gem 'hiredis', '~> 0.6'
  gem 'readthis', '~> 2.0'

  # Email signing
  gem 'dkim', '1.0.0'

  # Errbit support
  gem 'airbrake', '~> 5.6'

  # Job Scheduling
  gem 'resque-scheduler', '4.3.0'
  gem 'whenever', '~> 0.9'

  gem 'lograge', '~> 0.4'
  gem 'logstash-event', '~> 1.2'
end

group :production, :development, :client_test do
  # Asset Discovery
  gem 'fog-aws', '0.11.0'
  gem 'fog-openstack', '0.1.11'

  # Background jobs
  gem 'resque',
      git: 'https://github.com/resque/resque.git',
      ref: '20d885065ac19e7f7d7a982f4ed1296083db0300',
      require: ['resque', 'resque/failure/airbrake', 'resque/failure/multiple', 'resque/failure/redis']

  # Library for unzipping Gitlab build artifacts
  gem 'rubyzip', '~> 1.2'
end

# Development, Test, and Code Quality Gems
group :development do
  gem 'annotate', '~> 2.7'
  gem 'byebug', '~> 9.0'
  gem 'guard-rspec', '~> 4.7', require: false
  gem 'sinatra', '~> 2.0.0.beta2'
end

group :test do
  gem 'database_cleaner', '~> 1.5'
  gem 'fuubar', '~> 2.2'
  gem 'json-schema', '~> 2.7'
  gem 'regexp-examples', '~> 1.2'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov', '~> 0.12', require: false
end

group :client_test do
  # Provides endpoints for UI tests
  gem 'client_test_route_helpers', '~> 0.1', path: 'engines/client_test_route_helpers'

  # Used by client_test_route_helpers to enforce the shared DB connection to only be used by one thread at a time
  gem 'connection_pool', '~> 2.2'
end

group :development, :test do
  gem 'factory_girl_rails', '~> 4.7'
  gem 'pry', '~> 0.10'
  gem 'pry-byebug', '3.4.0'
  gem 'rspec-rails', '~> 3.5'
end

group :linters do
  gem 'rubocop', '~> 0.47'
end

group :security_auditors do
  gem 'brakeman', '~> 3.4'
  gem 'bundler-audit', '~> 0.5'
end
