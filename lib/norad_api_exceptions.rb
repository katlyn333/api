# frozen_string_literal: true
module NoradApiExceptions
  class NoradApiError < StandardError
  end

  class NotAuthorized < NoradApiError
  end
end
