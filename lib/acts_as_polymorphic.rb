# frozen_string_literal: true
module ActsAsPolymorphic
  extend ActiveSupport::Concern

  included do
    private_class_method :acts_as_polymorphic_setup_belongs_to_assocs
    private_class_method :acts_as_polymorphic_define_getter
    private_class_method :acts_as_polymorphic_define_setter
    private_class_method :acts_as_polymorphic_define_validation
  end

  module ClassMethods
    cattr_accessor :acts_as_polymorphic_assocs

    def acts_as_poly(meth, *assocs)
      acts_as_polymorphic_setup_belongs_to_assocs(assocs, assocs.extract_options!)
      acts_as_polymorphic_define_getter(meth, assocs)
      acts_as_polymorphic_define_setter(meth, assocs)
      acts_as_polymorphic_define_validation(meth, assocs)
    end
    alias acts_as_polymorphic acts_as_poly

    def acts_as_polymorphic_setup_belongs_to_assocs(assocs, opts)
      assocs.each do |assoc|
        belongs_to assoc, opts
      end
    end

    def acts_as_polymorphic_define_getter(meth, assocs)
      define_method meth do
        assocs.reduce(nil) do |res, assoc|
          res = send assoc
          break res if res
        end
      end
    end

    def acts_as_polymorphic_define_setter(meth, assocs)
      define_method "#{meth}=" do |val|
        target = assocs.detect { |assoc| assoc.to_s == val.class.name.underscore }
        raise ActiveRecord::AssociationTypeMismatch unless val.nil? || target
        send("#{target}=", val) if target
        (assocs - [target]).each { |a| send("#{a}=", nil) }
      end
    end

    def acts_as_polymorphic_define_validation(meth, assocs)
      define_method "only_one_#{meth}_column_is_populated" do
        cols = assocs.select { |assoc| send "#{assoc}_id" }
        errors.add(meth, 'only one column for the association can be populated') if cols.size > 1
      end
      validate "only_one_#{meth}_column_is_populated".to_sym
    end
  end
end

ActiveRecord::Base.send(:include, ActsAsPolymorphic)
