# frozen_string_literal: true
desc 'Ensure scan schedules stay in sync'
task sync_scan_schedules: :environment do
  machine_schedules, organization_schedules = Resque.schedule.keys.partition { |k| k.start_with?('machine') }
  process_jobs = proc do |klass, job_list|
    job_list.each do |job_name|
      begin
        scan = klass.find(job_name[/\d+\z/].to_i)
        cron_string = scan.send(:cron_string)
        schedule = Resque.fetch_schedule(job_name)
        if cron_string != schedule['cron']
          scan.valid? ? scan.send(:update_scan_in_queue) : Resque.remove_schedule(job_name)
          Rails.logger.info 'Discrepency in database and scan schedule found. Updating schedule.'
        end
      rescue ActiveRecord::RecordNotFound
        Rails.logger.info 'Discrepency in database and scan schedule found. Removing schedule.'
        Resque.remove_schedule job_name
      end
    end
  end

  process_jobs.call MachineScanSchedule, machine_schedules
  process_jobs.call OrganizationScanSchedule, organization_schedules
end
