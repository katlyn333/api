# frozen_string_literal: true
namespace :common_service_types do
  @file_download_path = Rails.root.join('tmp', 'downloads').to_s
  @nmap_services_url = 'https://svn.nmap.org/nmap/nmap-services'
  @target_file = Rails.root.join('db', 'seeds', 'common_service_types.yml').to_s
  desc 'Create display names for services.'
  task :create_data_file do
    fetch_nmap_services_file
    lines = read_file_to_array("#{@file_download_path}/nmap-services")
    stripped_lines = strip_comments(lines)
    services = stripped_lines.map { |line| extract_service_info(line) }
    named_services = services.select { |service| service[:name] != 'unknown' }
    services_list = compile_services_list(named_services)
    create_yaml_file(services_list)
  end

  def extract_service_info(line)
    {
      name: extract_name(line),
      port: extract_port(line),
      protocol: extract_protocol(line),
      frequency: extract_frequency(line),
      display_name: extract_display_name(line)
    }
  end

  def extract_name(line)
    line.split(' ')[0]
  end

  def extract_port(line)
    line.split(' ')[1].split('/')[0].to_i
  end

  def extract_protocol(line)
    line.split(' ')[1].split('/')[1]
  end

  def extract_frequency(line)
    line.split(' ')[2]
  end

  def extract_display_name(line)
    line.split('#')[1]&.strip
  end

  def fetch_nmap_services_file
    system("wget -P #{@file_download_path} #{@nmap_services_url}")
  end

  def strip_comments(lines)
    lines.select { |x| x[0] != '#' }
  end

  def read_file_to_array(file_name)
    fh = File.open(file_name)
    return_array = []
    while (line = fh.gets)
      return_array << line
    end
    return_array
  end

  def compile_services_list(list_of_hashes)
    return_value = {}
    list_of_hashes.each do |entry|
      return_value[entry[:name]] ||= {}
      return_value[entry[:name]][entry[:port]] ||= {}
      return_value[entry[:name]][entry[:port]][entry[:protocol]] =
        entry[:display_name]
    end
    return_value
  end

  def create_file_hash(services_list)
    file_hash = services_list.dup
    file_hash.each do |name, service_hash|
      service_hash.each do |port, port_hash|
        file_hash[name][port] = port_hash.keys
      end
    end
    file_hash
  end

  def create_yaml_file(services_list)
    file_hash = create_file_hash(services_list)
    File.open(@target_file, 'w') do |file|
      file.write file_hash.to_yaml
    end
  end
end
