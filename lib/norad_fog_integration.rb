# frozen_string_literal: true
module NoradFogIntegration
  class AssetDiscovery
    def initialize(configuration)
      @provider = configuration.provider
      @user = configuration.user
      @key = configuration.key
      @region = configuration.region
      @project = configuration.project
      @auth_url = configuration.auth_url
      @organization = configuration.organization
    end

    def start
      connect_to_provider.servers.each do |server|
        ip = extract_ip(server)
        next if server.id.blank? || ip.blank?
        sync_machine(@organization.machines.find_by(name: server.id), server.id, ip)
      end
    end

    private

    def connect_to_provider
      raise NotImplemented, 'This method should be defined in the child class'
    end

    def extract_ip
      raise NotImplemented, 'This method should be defined in the child class'
    end

    def sync_machine(machine, id, ip)
      machine.nil? ? add_machine(id, ip) : update_machine(machine, ip)
    end

    def add_machine(id, ip)
      @organization.machines << Machine.new(ip: ip, name: id)
    end

    def update_machine(machine, ip)
      return if machine.ip == ip
      machine.ip = ip
      machine.save!
    end
  end

  class AwsAssetDiscovery < AssetDiscovery
    private

    def connect_to_provider
      Fog::Compute.new(
        provider: @provider,
        region: @region,
        aws_access_key_id: @user,
        aws_secret_access_key: @key
      )
    end

    def extract_ip(server)
      server.public_ip_address || server.private_ip_address
    end
  end

  class OpenStackAssetDiscovery < AssetDiscovery
    private

    def connect_to_provider
      Fog::Compute::OpenStack.new(
        openstack_auth_url: @auth_url,
        openstack_username: @user,
        openstack_api_key: @key,
        openstack_project_name: @project
      )
    end

    def extract_ip(server)
      server.addresses&.values&.first&.first&.dig('addr')
    end
  end
end
