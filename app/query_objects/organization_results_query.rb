# frozen_string_literal: true
class OrganizationResultsQuery < ResultsQuery
  attr_reader :relation

  def initialize(organization_relation)
    @organization_relation = organization_relation
    @relation = results_for_organization
  end

  def for_command_excluding_machines(command, exclusion_list)
    for_commands(command).where.not(assessments: { machine: exclusion_list })
  end

  private

  attr_reader :machine_results_query, :organization_relation

  def results_for_organization
    Result.joins(:assessment).where(assessments: { machine: Machine.where(organization: organization_relation) })
  end
end
