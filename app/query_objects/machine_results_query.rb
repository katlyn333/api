# frozen_string_literal: true
class MachineResultsQuery < ResultsQuery
  attr_reader :relation

  def initialize(machine_relation)
    @relation = Result.joins(:assessment).where(assessments: { machine: machine_relation })
  end
end
