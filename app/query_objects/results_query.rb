# frozen_string_literal: true
class ResultsQuery
  attr_reader :relation

  def initialize
    @relation = Result.all
  end

  def for_commands(commands)
    relation.joins(:assessment).where(assessments: { docker_command: commands })
  end
end
