# frozen_string_literal: true
class AssessmentBuilderJob < ActiveJob::Base
  ActiveRecord::Base.logger = Resque.logger if defined?(Resque)
  queue_as :command_processing

  rescue_from(StandardError) do |exception|
    Airbrake.notify_sync exception
    Resque.logger.error "Exception while starting job: #{exception.inspect}" if defined?(Resque)
    # XXX: The first argument passed to the perform method is the docker command.
    command = @arguments.first
    ActiveRecord::Base.transaction do
      command.error_details = 'Something went wrong while queuing the containers'
      command.cancel!
    end
  end

  before_perform do
    command = @arguments.first
    command.update_column(:started_at, DateTime.current)
  end

  after_perform do
    @dc.complete!
  end

  def perform(docker_command)
    @dc = docker_command
    containers = SecurityContainer.where(name: @dc.containers)
    jobs(containers).flatten.each do |job|
      job.scans.each do |scan|
        ScheduleContainerJob.perform_later(job.to_h.merge(scan.to_h))
      end
    end
  end

  private

  def jobs(containers)
    @dc.commandable.class.name == 'Organization' ? build_for_org(containers) : build_for_machine(containers)
  end

  def build_for_org(containers)
    org = @dc.commandable
    containers.map do |c|
      if enabled_by_org?(c, org)
        next queue_multi_host(c, org) if c.multi_host
        org.machines.map { |m| MachineScanJob.new(m, c, @dc) }
      else
        org.machines.with_configuration_for(c).map { |m| MachineScanJob.new(m, c, @dc) }
      end
    end
  end

  def queue_multi_host(container, org)
    special_machines = multi_host_special_machines(container, org)
    machine_jobs = special_machines.map do |m|
      MachineScanJob.new(m, container, @dc)
    end
    return machine_jobs if org.machines.where.not(id: special_machines).empty?
    machine_jobs << OrganizationScanJob.new(org, container, special_machines, @dc)
  end

  def build_for_machine(containers)
    containers.map do |c|
      MachineScanJob.new(@dc.commandable, c, @dc)
    end
  end

  def multi_host_special_machines(container, org)
    machine_configs = container.security_container_configs.for_machines.where(machine_id: org.machines)
    machine_configs.map(&:configurable)
  end

  def enabled_by_org?(container, org)
    org.required_containers.where(id: container.id).exists? ||
      org.security_container_configs.explicitly_enabled.where(security_container_id: container.id).exists?
  end
end

# XXX Currently multi host scanning is only supported for whole host scans like Qualys
class OrganizationScanJob
  attr_reader :scans

  def initialize(org, container, excluded_machines, command)
    @dc = command
    @organization = org
    @secret = SecurityContainerSecret.create!
    @container_name = container.name
    @scans = create_scans(container, org.machines.where.not(id: excluded_machines))
    @relay_exchange = org.token
    @relay_queue = org.docker_relay_queue
    @relay_secret = org.relay_encryption_key
    @multi_host = container.multi_host
  end

  def to_h
    {
      container_secret: @secret.secret,
      container_name: @container_name,
      relay_secret: @relay_secret,
      relay_queue: @relay_queue,
      relay_exchange: @relay_exchange
    }
  end

  private

  def relevant_services(container, machine)
    machine.services.testable_by(container)
  end

  # XXX Currently multi host scanning is only supported for whole host scans like Qualys
  def create_scans(container, machines)
    @scans = [MultiHostScan.new(container, @organization, machines, @secret.id, @dc)]
  end
end

class MachineScanJob < OrganizationScanJob
  def initialize(machine, container, command)
    org = machine.organization
    @dc = command
    @secret = SecurityContainerSecret.create!
    @container_name = container.name
    @relay_exchange = org.token
    @relay_queue = org.docker_relay_queue
    @relay_secret = org.relay_encryption_key
    @scans = create_scans(container, machine)
  end

  private

  def create_scans(container, machine)
    if container.whole_host?
      @scans = [WholeHostScan.new(container, machine, @secret.id, @dc)]
    else
      ActiveRecord::Base.transaction do
        @scans = relevant_services(container, machine).map do |s|
          ServiceScan.new(s, container, machine, @secret.id, @dc)
        end
      end
    end
  end
end

class NoradScan
  attr_reader :target, :args

  def to_h
    {
      targets: [target], # Beacon expects an Array
      args: args
    }
  end

  private

  def create_assessment(container, machine, secret_id, service = nil)
    assessment = @dc.assessments.create!(assessment_attrs(container, machine, secret_id, service))
    {
      id: machine.target_address,
      assessment: assessment.results_path,
      assessment_global_id: assessment.to_global_id.to_s,
      args: args_array(assessment)
    }
  end

  def assessment_attrs(container, machine, secret_id, service = nil)
    {
      machine: machine,
      title: container.name,
      security_container: container,
      security_container_secret_id: secret_id,
      service: service,
      type: container.whitebox? ? 'WhiteBoxAssessment' : 'BlackBoxAssessment'
    }
  end

  def container_config(machine, container)
    config = container.config_for_machine(machine.id) || container.config_for_organization(machine.organization.id)
    config ? config.args_hash : container.args_hash
  end

  def create_command_array(container, args_hash)
    format(container.prog_args, args_hash).split(' ').map do |arg|
      arg.gsub(/#{SecurityContainer::SPACE_PLACEHOLDER}/, ' ')
    end
  end
end

# Scan a single host with one set of arguments
class WholeHostScan < NoradScan
  def initialize(container, machine, secret_id, command)
    @dc = command
    @target = create_assessment(container, machine, secret_id)
    @args = @target.delete(:args)
  end

  private

  def args_array(assessment)
    cargs = container_config(assessment.machine, assessment.security_container) || {}
    create_command_array(assessment.security_container, { target: assessment.machine.target_address }.merge(cargs))
  end
end

# Scan a host's services with different arguments per service
class ServiceScan < NoradScan
  def initialize(service, container, machine, secret_id, command)
    @dc = command
    @target = create_assessment(container, machine, secret_id, service)
    @args = @target.delete(:args)
  end

  private

  def args_array(assessment)
    sargs = assessment.service.service_configuration.merge(target: assessment.machine.target_address)
    cargs = container_config(assessment.machine, assessment.security_container) || {}
    create_command_array(assessment.security_container, sargs.merge(cargs))
  end
end

# Scan multiple machines with the same set of arguments
# XXX Currently multi host scanning is only supported for whole host scans like Qualys
class MultiHostScan < NoradScan
  def initialize(container, organization, machines, secret_id, command)
    @dc = command
    @target = create_assessment(container, machines, secret_id)
    @args = args_array(container, organization, machines)
  end

  def to_h
    {
      targets: target,
      args: args
    }
  end

  private

  def create_assessment(container, machines, secret_id)
    ActiveRecord::Base.transaction do
      machines.map do |machine|
        assessment = @dc.assessments.create!(assessment_attrs(container, machine, secret_id))
        { id: machine.target_address,
          assessment: assessment.results_path,
          assessment_global_id: assessment.to_global_id.to_s }
      end
    end
  end

  def args_array(container, organization, machines)
    cargs = container_config(organization, container) || {}
    args = { target: machines.map(&:target_address).join(',') }.merge(cargs)
    create_command_array(container, args)
  end

  def container_config(organization, container)
    config = container.config_for_organization(organization.id)
    config ? config.args_hash : container.args_hash
  end
end
