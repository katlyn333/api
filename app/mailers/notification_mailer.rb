# frozen_string_literal: true
class NotificationMailer < ApplicationMailer
  def scan_complete(docker_command_id)
    @docker_command = DockerCommand.find(docker_command_id)
    @organization = @docker_command.resolved_organization
    mail subject: "#{SUBJECT_PREFIX} Scan complete", bcc: @organization.users.pluck(:email)
  end
end
