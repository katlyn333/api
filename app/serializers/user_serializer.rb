# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  uid        :string           not null
#  firstname  :string
#  lastname   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#  index_users_on_uid    (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :uid, :firstname, :lastname, :created_at, :updated_at, :api_token

  def api_token
    object.api_token.value if object.id == Thread.current['current_user'].try(:id)
  end
end
