# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: service_discoveries
#
#  id                  :integer          not null, primary key
#  machine_id          :integer
#  container_secret_id :integer
#  state               :integer
#  error_message       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_discoveries_on_container_secret_id  (container_secret_id)
#  index_service_discoveries_on_in_progress_state    (machine_id,state) UNIQUE
#  index_service_discoveries_on_machine_id           (machine_id)
#  index_service_discoveries_on_pending_state        (machine_id,state) UNIQUE
#
# Foreign Keys
#
#  fk_rails_92a60df35e  (container_secret_id => security_container_secrets.id) ON DELETE => cascade
#  fk_rails_fc86baf86f  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class ServiceDiscoverySerializer < ActiveModel::Serializer
  attributes :id, :machine_id, :state, :error_message, :updated_at, :created_at
end
