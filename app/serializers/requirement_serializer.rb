# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: requirements
#
#  id                   :integer          not null, primary key
#  name                 :string
#  description          :string
#  requirement_group_id :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_requirements_on_requirement_group_id  (requirement_group_id)
#
# Foreign Keys
#
#  fk_rails_cce12a8273  (requirement_group_id => requirement_groups.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class RequirementSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :requirement_group_id
  has_many :provisions
end
