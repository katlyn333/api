# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_relays
#
#  id                            :integer          not null, primary key
#  organization_id               :integer          not null
#  public_key                    :text             not null
#  queue_name                    :string           not null
#  state                         :integer          default("online"), not null
#  last_heartbeat                :datetime         not null
#  verified                      :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  key_signature                 :string           not null
#  file_encryption_key_encrypted :string
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_b84167028c  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class DockerRelaySerializer < ActiveModel::Serializer
  attributes :id,
             :public_key,
             :queue_name,
             :state,
             :last_heartbeat,
             :verified,
             :organization_id,
             :key_signature,
             :public_file_encryption_key

  def public_file_encryption_key
    return nil unless instance_options[:include_public_keys]
    Base64.strict_encode64(object.file_encryption_key_to_rsa.public_key.to_pem)
  end

  def public_key
    return nil unless instance_options[:include_public_keys]
    object.public_key
  end
end
