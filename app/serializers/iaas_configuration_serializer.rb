# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: iaas_configurations
#
#  id              :integer          not null, primary key
#  provider        :integer          not null
#  user_encrypted  :string
#  key_encrypted   :string
#  region          :string
#  project         :string
#  auth_url        :string
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_iaas_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_a8120b8d08  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class IaasConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :provider, :user, :key, :region, :project, :auth_url, :organization_id, :created_at, :updated_at
  has_many :iaas_discoveries

  def iaas_discoveries
    object.iaas_discoveries.limit(5)
  end
end
