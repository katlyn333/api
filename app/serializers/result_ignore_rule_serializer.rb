# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_ignore_rules
#
#  id                :integer          not null, primary key
#  ignore_scope_id   :integer          not null
#  ignore_scope_type :string           not null
#  signature         :string           not null
#  comment           :text
#  created_by        :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_result_ignore_rules_on_i_s_type_and_i_s_id                (ignore_scope_type,ignore_scope_id)
#  index_result_ignore_rules_on_i_s_type_and_i_s_id_and_signature  (ignore_scope_type,ignore_scope_id,signature) UNIQUE
#  index_result_ignore_rules_on_signature                          (signature)
#
# rubocop:enable Metrics/LineLength

class ResultIgnoreRuleSerializer < ActiveModel::Serializer
  attributes :id, :ignore_scope_id, :ignore_scope_type, :signature, :comment, :created_by, :created_at, :updated_at
end
