# frozen_string_literal: true
class DockerSwarm
  Docker.url = ENV['SWARM_MASTER_URL']
  DOCKER_NETWORK_NAME = ENV.fetch('DOCKER_NETWORK_NAME')
  DOCKER_DNS_SERVERS = ENV.fetch('DOCKER_DNS_SERVERS')&.split(',')

  class << self
    # rubocop:disable Metrics/ParameterLists
    def queue_container(name, image, args, targets, secret, relay_opts = {})
      image = image.to_s
      container_options = build_options_hash(name, image, args, targets, secret)
      relay_opts[:relay_queue] ? send_to_relay(container_options, relay_opts) : start_container(container_options)
    end
    # rubocop:enable Metrics/ParameterLists

    def queue_discovery_container(discovery, relay_opts = {})
      discovery_environment = build_discovery_environment(discovery)
      container_opts = {
        'Image' => 'norad-registry.cisco.com:5000/service-discovery:latest',
        'Env' => discovery_environment,
        'Cmd' => [discovery.machine.target_address]
      }
      relay_opts[:relay_queue] ? send_to_relay(container_opts, relay_opts) : start_container(container_opts)
    end

    private

    def start_container(container_options)
      pull_image(container_options.fetch('Image'))
      Rails.logger.debug "Starting container for: #{container_options['Image']}"
      container_options.merge!(network_options)
      container = Docker::Container.create(container_options)
      container.start
    end

    def send_to_relay(container_options, relay_opts)
      container_options['encoded_relay_private_key'] = relay_opts[:relay_secret]
      statement = { docker_options: container_options }
      Publisher.publish relay_opts[:relay_exchange], statement, relay_opts[:relay_queue]
    end

    # Call this method before starting a container in order to ensure that the node has the latest
    # version of the image you are wanting to run
    def pull_image(image)
      # Analogous to `docker pull <image_name>`
      Docker::Image.create fromImage: image
    end

    def build_options_hash(name, image, args, targets, secret)
      {
        'name' => name,
        'Image' => image,
        'Env' => env_variables(targets, secret),
        'Cmd' => args
      }
    end

    def env_variables(targets, secret)
      [
        "NORAD_ROOT=#{ENV.fetch('API_URL').gsub(%r{/v1}, '')}",
        "ASSESSMENT_PATHS=#{targets.to_json}",
        "NORAD_SECRET=#{secret}"
      ]
    end

    def build_discovery_environment(discovery)
      services_url = Rails.application.routes.url_helpers.v1_machine_services_path(discovery.machine_id)
      discovery_url = Rails.application.routes.url_helpers.v1_service_discovery_path(discovery)
      [
        "NORAD_ROOT=#{ENV.fetch('API_URL').gsub(%r{/v1}, '')}",
        "NORAD_SERVICES_URL=#{services_url}",
        "NORAD_DISCOVERY_URL=#{discovery_url}",
        "NORAD_SECRET=#{discovery.shared_secret}"
      ]
    end

    def network_options
      { NetworkMode: DOCKER_NETWORK_NAME, Dns: DOCKER_DNS_SERVERS }
    end
  end
end
