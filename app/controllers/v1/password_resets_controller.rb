# frozen_string_literal: true
module V1
  class PasswordResetsController < ApplicationController
    allow_unauthenticated :create, :update, :show
    before_action :set_record, only: [:show, :update]

    # verify password reset is valid
    def show
      render json: @record
    end

    # send password reset email
    def create
      User.confirmed_users.find_by(create_params)&.reset_password!
      head :no_content # prevent email enumeration by always returning success
    end

    # update user's password
    def update
      if @record.update(update_params)
        head :no_content
      else
        render_errors_for(@record)
      end
    end

    private

    def set_record
      # constant time lookup to help mitigate timing attacks via PG lookups
      @record = PasswordReset.padded_find_by!(password_reset_token: params[:provided_token])
    end

    def create_params
      params.permit(:email)
    end

    def update_params
      params.permit(:provided_token, :password, :password_confirmation)
    end
  end
end
