# frozen_string_literal: true
module V1
  class SshKeyPairAssignmentsController < ApplicationController
    def create
      machine, keypair = set_machine_and_keypair
      authorize_action_for SshKeyPairAssignment, in: machine.organization
      assignment = machine.build_ssh_key_pair_assignment(ssh_key_pair_id: keypair.id)
      if assignment.save
        render json: assignment
      else
        render_errors_for(assignment)
      end
    end

    def destroy
      machine = Machine.find(params[:machine_id])
      assignment = machine.ssh_key_pair_assignment
      authorize_action_for assignment
      assignment.destroy
      head :no_content
    end

    private

    def set_machine_and_keypair
      machine = Machine.find(params[:machine_id])
      keypair = machine.organization.ssh_key_pairs.find(params[:ssh_key_pair_id])
      [machine, keypair]
    end
  end
end
