# frozen_string_literal: true
module V1
  class IaasConfigurationsController < ApplicationController
    before_action :set_iaas_config, only: [:show, :update, :destroy]

    def show
      render json: @iaas_configuration
    end

    def create
      org = Organization.find(params[:organization_id])
      authorize_action_for IaasConfiguration, in: org
      config = org.build_iaas_configuration(config_params)
      if config.save
        render json: config
      else
        render_errors_for(config)
      end
    end

    def update
      if @iaas_configuration.update(config_params)
        render json: @iaas_configuration
      else
        render_errors_for(@iaas_configuration)
      end
    end

    def destroy
      @iaas_configuration.destroy

      head :no_content
    end

    private

    def set_iaas_config
      @iaas_configuration = if params[:id]
                              IaasConfiguration.find_by(id: params[:id])
                            else
                              # Support the singular routes
                              IaasConfiguration.joins(:organization).where(
                                organizations: { slug: params[:organization_id] }
                              ).first
                            end
      raise ActiveRecord::RecordNotFound unless @iaas_configuration
      authorize_action_for @iaas_configuration
    end

    def config_params
      params[:iaas_configuration].delete('organization_id')
      params.require(:iaas_configuration).permit(:provider, :user, :key, :region, :project, :auth_url)
    end
  end
end
