# frozen_string_literal: true
module V1
  class EmailConfirmationsController < ApplicationController
    allow_unauthenticated :create

    def create
      confirm
    end

    private

    def confirm
      find_for_confirmation.confirm!
      head :no_content
    end

    def find_for_confirmation
      LocalAuthenticationRecord.find_by!(confirmation_params)
    end

    def confirmation_params
      params.permit(:email_confirmation_token)
    end
  end
end
