# frozen_string_literal: true

module V1
  class AssessmentsController < ApplicationController
    before_action :set_assessment, only: :show
    before_action :set_machine, only: [:index, :latest]

    def index
      authorize_action_for Assessment, in: @machine.organization
      render json: assessments
    end

    def latest
      params[:limit] = 'latest'
      index
    end

    def show
      authorize_action_for @assessment
      render json: @assessment
    end

    private

    def set_assessment
      @assessment = Assessment.find(params[:id])
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def assessments
      return @machine.assessments.for_docker_command(params[:docker_command_id]) if params[:docker_command_id]
      scoped_assessments
    end

    def scoped_assessments
      limit = params[:limit].to_s
      return @machine.assessments.latest if limit == 'latest'
      return @machine.assessments.order(created_at: :desc) if limit == 'all'
      return @machine.assessments.order(docker_command_id: :desc) if limit == 'all'
      @machine.assessments.most_recent_by_docker_command_id
    end
  end
end
