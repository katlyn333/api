# frozen_string_literal: true
module V1
  class SecurityContainerConfigsController < ApplicationController
    include BelongsToPolyParent
    before_action :set_record, only: [:show, :update, :destroy]

    private

    def creation_params
      params
        .require(:security_container_config)
        .permit(:security_container_id, :values, :enabled_outside_of_requirement).tap do |whitelisted|

        whitelisted[:values] = params[:security_container_config].delete(:values).tap do |vals|
          vals&.send(:permitted=, true)
        end
      end
    end

    def update_params
      params.require(:security_container_config).permit(:values, :enabled_outside_of_requirement).tap do |whitelisted|
        whitelisted[:values] = params[:security_container_config].delete(:values).tap do |vals|
          vals&.send(:permitted=, true)
        end
      end
    end
  end
end
