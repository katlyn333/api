# frozen_string_literal: true
module V1
  # This class essentially acts as an abstraction for DockerCommands. In the future we will
  # incorporate AgentCommands as well. This controller is used to provide a single endpoint for end
  # users to launch scans. As such, this controller will create and return DockerCommand objects.
  # This means that the RBAC for DockerCommands will apply and be enforced.
  class ScansController < ApplicationController
    before_action :set_target
    # FIXME: At this point we are only supporting non-Agent deployments
    def create
      authorize_action_for DockerCommand, in: organization
      containers = enabled_containers
      command = @target.docker_commands.build(containers: containers)
      if command.save
        render json: command
      else
        render_errors_for(command)
      end
    end

    private

    def set_target
      @target = params[:machine_id] ? Machine.find(params[:machine_id]) : Organization.find(params[:organization_id])
    end

    def organization
      @target.respond_to?(:organization) ? @target.organization : @target
    end

    def machine_ids
      @target.respond_to?(:organization) ? [@target.id] : @target.machines.select(:id)
    end

    def enabled_containers
      (explicitly_enabled_containers + organization.required_containers.pluck(:name)).uniq
    end

    def explicitly_enabled_containers
      SecurityContainerConfig
        .includes(:security_container)
        .explicitly_enabled
        .where('(machine_id IN (?)) OR (organization_id = ?)', machine_ids, organization.id)
        .map { |config| config.security_container.name }
    end
  end
end
