# frozen_string_literal: true
module V1
  class DockerCommandsController < ApplicationController
    include BelongsToPolyParent
    before_action :set_record, only: [:show, :destroy]

    undef_method :update

    private

    def creation_params
      params.require(:docker_command).permit(:containers).tap do |whitelisted|
        whitelisted[:containers] = params[:docker_command].delete :containers
      end
    end
  end
end
