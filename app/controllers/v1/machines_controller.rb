# frozen_string_literal: true
module V1
  class MachinesController < ApplicationController
    before_action :set_machine, only: [:show, :update, :destroy]

    def index
      org = Organization.find params[:organization_id]
      authorize_action_for Machine, in: org
      render json: org.machines
    end

    def show
      authorize_action_for @machine
      render json: @machine
    end

    def create
      org = lookup_org
      authorize_action_for Machine, in: org
      machine = org.machines.build(machine_params)
      if machine.save
        render json: machine
      else
        render_errors_for(machine)
      end
    end

    def update
      authorize_action_for @machine
      if @machine.update(machine_params)
        render json: @machine
      else
        render_errors_for(@machine)
      end
    end

    def destroy
      authorize_action_for @machine
      @machine.destroy

      head :no_content
    end

    private

    def set_machine
      @machine = Machine.find(params[:id])
    end

    def machine_params
      params.require(:machine).permit(:ip, :fqdn, :description, :name)
    end

    def lookup_org
      Organization.by_token(params[:organization_token])
    end
  end
end
