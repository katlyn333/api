# frozen_string_literal: true
module BelongsToPolyParent
  extend ActiveSupport::Concern

  included do
    before_action :set_parent_object, only: [:create, :index]

    def index
      render json: @parent.send(child_association_name)
    end

    def show
      render json: @record
    end

    def create
      record = @parent.send(child_association_name).build(creation_params)
      if record.save
        render json: record
      else
        render_errors_for(record)
      end
    end

    def update
      if @record.update(update_params)
        render json: @record
      else
        render_errors_for @record
      end
    end

    def destroy
      if @record.destroy
        head :no_content
      else
        render_errors_for(@record)
      end
    end

    private

    def klass
      controller_name.classify.constantize
    end

    def child_association_name
      controller_name.pluralize
    end

    def set_record
      @record = klass.find(params[:id])
      authorize_action_for @record
    end

    def set_parent_object
      @parent = params[:machine_id] ? Machine.find(params[:machine_id]) : Organization.find(params[:organization_id])
      authorize_action_for klass, in: @parent.rbac_parent
    end
  end
end
