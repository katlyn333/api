# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: service_identities
#
#  id                 :integer          not null, primary key
#  username_encrypted :string
#  password_encrypted :string
#  service_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_service_identities_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_b8cfe24029  (service_id => services.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class ServiceIdentity < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Vault Stuff
  include Vault::EncryptedModel
  vault_attribute :password
  vault_attribute :username

  # Validations
  validates :service, presence: true
  validates :password, presence: true
  validates :username, presence: true

  # Associations
  belongs_to :service
end
