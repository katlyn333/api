# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_tokens
#
#  id              :integer          not null, primary key
#  value           :string           not null
#  state           :integer          default("active"), not null
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_tokens_on_organization_id  (organization_id)
#  index_organization_tokens_on_value            (value) UNIQUE
#
# Foreign Keys
#
#  fk_rails_79797e6ba9  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class OrganizationToken < Token
  # Attribute Information
  enum state: [:active, :inactive]

  # RBAC
  include Authority::Abilities

  # Associations
  belongs_to :organization
end
