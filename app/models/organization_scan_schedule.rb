# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_scan_schedules
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  period          :integer          default("daily"), not null
#  at              :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_scan_schedules_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_129af2457b  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class OrganizationScanSchedule < ScanSchedule
  include Authority::Abilities
  resourcify

  validates :organization, presence: true
  belongs_to :organization

  def scan_target
    organization
  end
end
