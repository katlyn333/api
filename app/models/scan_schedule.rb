# frozen_string_literal: true
class ScanSchedule < ApplicationRecord
  self.abstract_class = true

  # Attribute Information
  enum period: { daily: 0, weekly: 1 }
  VALID_DAYS = %w(sun mon tue wed thu fri sat).freeze

  # Validations
  validates :at, presence: true
  validate :at_format_for_weekly_scan, if: ->(schedule) { schedule.at && schedule.weekly? }
  validate :at_format_for_daily_scan, if: ->(schedule) { schedule.at && schedule.daily? }

  # Callbacks
  after_commit :update_scan_in_queue, on: [:create, :update], unless: 'Rails.env.development?'
  after_commit :delete_scan_from_queue, on: :destroy, unless: 'Rails.env.development?'

  def scan_target
    raise NotImplemented, 'This method should be defined in the child class'
  end

  private

  # Validation definitions
  def at_format_for_weekly_scan
    day, hour, minute = parse_at_attributes
    errors.add(:at, 'must specify a day to run weekly scans, e.g. Mon 12:32') unless day
    at_format(hour, minute)
  end

  def at_format_for_daily_scan
    day, hour, minute = parse_at_attributes
    errors.add(:at, 'cannot specify a day of the week for daily scans') if day
    at_format(hour, minute)
  end

  def at_format(hour, minute)
    errors.add(:at, 'time must be in 24-hour format, e.g. HH:MM') unless valid_hour?(hour) && minute
  end

  # Validation helpers
  def parse_at_attributes
    at.scan(/\A(#{VALID_DAYS.join('|')})?(?:[a-z])*\s*([0-2]?[0-9]):([0-5][0-9])\z/i).flatten
  end

  def valid_hour?(hour)
    hour && hour.to_i - 24 <= 0
  end

  # Callback definitions
  def update_scan_in_queue
    config = {
      class: 'ScheduleScanJob',
      args: to_global_id.to_s,
      cron: cron_string,
      persist: true
    }
    Resque.set_schedule("#{self.class.name.underscore}_#{id}", config)
  end

  def delete_scan_from_queue
    Resque.remove_schedule("#{self.class.name.underscore}_#{id}")
  end

  def cron_string
    day, hour, minute = parse_at_attributes
    # Strings for weekly scans will look something like this:
    # 50 14 * * Mon
    # Strings for daily scans will look something like this:
    # 50 14 * * *
    "#{minute} #{hour} * * #{period == 'weekly' ? day : '*'} UTC"
  end
end
