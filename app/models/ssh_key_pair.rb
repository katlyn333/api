# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  key_signature      :string           not null
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class SshKeyPair < ApplicationRecord
  # Vault Stuff
  include Vault::EncryptedModel
  vault_attribute :key
  vault_attribute :username

  # RBAC
  include Authority::Abilities

  # Validations
  validates :name, presence: true
  validates :name, uniqueness: { scope: :organization_id }
  validates :organization, presence: true
  validates :username, presence: true
  validates :key, presence: true
  validate :key_is_valid

  # Associations
  belongs_to :organization, inverse_of: :ssh_key_pairs
  has_many :ssh_key_pair_assignments, inverse_of: :ssh_key_pair
  has_many :machines, through: :ssh_key_pair_assignments, inverse_of: :ssh_key_pair

  # Callbacks
  before_validation do
    # Remove newlines if the sender forgot to do so
    self.key = key.to_s.tr("\n", '')
  end

  before_create do
    der = OpenSSL::PKey.read(Base64.decode64(key)).public_key.to_der
    self.key_signature = OpenSSL::Digest::SHA256.hexdigest(der).scan(/../).join ':'
  end

  include OrganizationErrorWatcher
  watch_for_organization_errors NoSshKeyPairError

  private

  # Validation definitions
  def key_is_valid
    data = Base64.strict_decode64(key)
    raise ArgumentError if data =~ /ENCRYPTED/
    OpenSSL::PKey.read(data, '')
  rescue ArgumentError
    errors.add :key, 'must be Base64 encoded and in a valid RSA/DSA format with no passphrase'
  end
end
