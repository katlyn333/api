# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: local_authentication_records
#
#  id                       :integer          not null, primary key
#  password_digest          :string
#  password_reset_token     :string
#  password_reset_sent_at   :datetime
#  authentication_method_id :integer
#  email_confirmation_token :string
#  email_confirmed_at       :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_local_authentication_records_on_authentication_method_id  (authentication_method_id) UNIQUE
#  index_local_authentication_records_on_email_confirmation_token  (email_confirmation_token) UNIQUE
#  index_local_authentication_records_on_email_confirmed_at        (email_confirmed_at)
#  index_local_authentication_records_on_password_reset_sent_at    (password_reset_sent_at)
#  index_local_authentication_records_on_password_reset_token      (password_reset_token) UNIQUE
#
# rubocop:enable Metrics/LineLength

module UserReturnable
  # Upgrades has_secure_password's authenticate to return local_auth_record.user instead of local_auth_record.
  # This follows the convention set by User#authenticate which returns a user obj if auth is successful.
  def authenticate(params)
    auth_result = super(params)
    auth_result ? auth_result.user : auth_result
  end
end

class LocalAuthenticationRecord < ApplicationRecord
  attr_accessor :provided_token

  PASSWORD_RESET_TOKEN_EXPIRATION = 2.days

  has_secure_token :password_reset_token
  has_secure_password

  # Modules / Concerns
  include EmailConfirmable
  include ActiveSupport::SecurityUtils

  # Prepends
  prepend UserReturnable

  # Validations
  validates :password, length: { minimum: 8 }, allow_blank: true # don't validate password when saving other attributes
  validates :password_digest, presence: true
  validate :password_change, on: :update, if: -> { password_digest_changed? }

  # Associations
  has_one :user, through: :local_authentication_method
  belongs_to :local_authentication_method, foreign_key: :authentication_method_id

  # Scopes
  scope :with_recent_tokens, -> { where('password_reset_sent_at > ?', PASSWORD_RESET_TOKEN_EXPIRATION.ago) }

  def reset_password!
    ActiveRecord::Base.transaction do
      regenerate_password_reset_token
      update!(password_reset_sent_at: DateTime.current)
      send_password_reset_email
    end
  end

  private

  def password_change
    return if can_change_password?
    errors.add(:base, 'User unconfirmed or password reset token invalid')
  end

  def can_change_password?
    confirmed? && secure_compare(provided_token, password_reset_token)
  end

  def send_password_reset_email
    UserMailer.password_reset(user).deliver_later
  end
end
