# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: local_authentication_records
#
#  id                       :integer          not null, primary key
#  password_digest          :string
#  password_reset_token     :string
#  password_reset_sent_at   :datetime
#  authentication_method_id :integer
#  email_confirmation_token :string
#  email_confirmed_at       :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_local_authentication_records_on_authentication_method_id  (authentication_method_id) UNIQUE
#  index_local_authentication_records_on_email_confirmation_token  (email_confirmation_token) UNIQUE
#  index_local_authentication_records_on_email_confirmed_at        (email_confirmed_at)
#  index_local_authentication_records_on_password_reset_sent_at    (password_reset_sent_at)
#  index_local_authentication_records_on_password_reset_token      (password_reset_token) UNIQUE
#
# rubocop:enable Metrics/LineLength

# A wrapper for LocalAuthenticationRecord to make dealing with resetting passwords easier
class PasswordReset < LocalAuthenticationRecord
  include WithPaddedDuration

  class << self
    def padded_find_by!(atts)
      with_padded_duration(2) do
        with_recent_tokens.find_by!(atts)
      end
    end
  end
end
