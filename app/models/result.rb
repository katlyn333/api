# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: results
#
#  id            :integer          not null, primary key
#  status        :integer          default("fail"), not null
#  output        :text
#  title         :string
#  description   :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  nid           :string           not null
#  sir           :integer          default("unevaluated"), not null
#  assessment_id :integer
#  ignored       :boolean          default(FALSE), not null
#  signature     :string           not null
#
# Indexes
#
#  index_results_on_assessment_id  (assessment_id)
#  index_results_on_signature      (signature)
#
# Foreign Keys
#
#  fk_rails_5e3335002f  (assessment_id => assessments.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class Result < ApplicationRecord
  # Attribute Information
  enum status: [:fail, :pass, :warn, :info, :error]
  enum sir: [:unevaluated, :no_impact, :low, :medium, :high, :critical]

  API_ERROR_TITLE = 'API Error'
  API_ERROR_OUTPUT = 'An error occurred in the API while starting the scan. Please retry.'
  ASSESSMENT_TIMEOUT_TITLE = 'This assessment has timed out'
  ASSESSMENT_TIMEOUT_OUTPUT = 'It has been 48 hours since this assessment was scheduled. Please retry.'

  # Associations
  belongs_to :assessment, inverse_of: :results

  # Validations
  validates :status, presence: true
  validates :nid,
            presence: true,
            format: {
              with: /\A[A-Za-z][A-Za-z0-9\._-]*:[A-Za-z0-9\._-]+\z/,
              message: 'can only contain alphanumeric, dashes, and underscores'
            }
  validates :sir,
            inclusion: {
              in: sirs.keys,
              message: 'must be unevaluated, no_impact, low, medium, high, or critical'
            }
  validates :assessment, presence: true
  validates :signature,
            length: { is: 64 },
            format: {
              with: /\A[a-z0-9]+\z/, # case-insensitive pg indexes are cumbersome in rails, so enforce lowercase SHAs
              message: 'can only contain lowercase alphanumeric characters'
            }

  # Scopes
  scope :passing, -> { where(status: statuses['pass']) }
  scope :failing, -> { where(status: statuses['fail']) }
  scope :warning, -> { where(status: statuses['warn']) }
  scope :informing, -> { where(status: statuses['info']) }
  scope :erroring, -> { where(status: statuses['error']) }
  scope :with_signature, ->(signature) { where(signature: signature) }

  # Highly recommended to call this method with a filtered lists of results
  def self.ignore
    update_all(ignored: true)
  end

  # Highly recommended to call this method with a filtered lists of results
  def self.unignore
    update_all(ignored: false)
  end

  # Highly recommended to call this method with a filtered lists of results
  def self.stats
    {
      passing: passing.count,
      warning: warning.count,
      failing: failing.count,
      erroring: erroring.count,
      informing: informing.count
    }
  end
end
