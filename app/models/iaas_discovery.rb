# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: iaas_discoveries
#
#  id                    :integer          not null, primary key
#  iaas_configuration_id :integer          not null
#  state                 :integer          default("pending"), not null
#  error_message         :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_iaas_discoveries_on_ip_state       (iaas_configuration_id,state) UNIQUE
#  index_iaas_discoveries_on_pending_state  (iaas_configuration_id,state) UNIQUE
#
# Foreign Keys
#
#  fk_rails_62a3d3b299  (iaas_configuration_id => iaas_configurations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class IaasDiscovery < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include IaasDiscoveryStateMachine

  # Validations
  validates :iaas_configuration, presence: true
  validates :error_message, absence: true, unless: 'failed?'
  validates :error_message, presence: true, if: 'failed?'

  # Associations
  belongs_to :iaas_configuration

  # Callbacks
  after_commit :schedule_discovery, on: :create

  # Provide a shortcut to the organization
  delegate :organization, to: :iaas_configuration

  private

  def schedule_discovery
    IaasDiscoveryJob.perform_later self, iaas_configuration
  end
end
