# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: requirement_groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# rubocop:enable Metrics/LineLength

class RequirementGroup < ApplicationRecord
  include Authority::Abilities
  resourcify

  # Associations
  has_many :requirements, inverse_of: :requirement_group
  has_many :enforcements, inverse_of: :requirement_group
  has_many :organizations, through: :enforcements
  has_many :security_containers, through: :requirements

  # Validations
  validates :name, presence: true
  validates :description, presence: true
end
