# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_ignore_rules
#
#  id                :integer          not null, primary key
#  ignore_scope_id   :integer          not null
#  ignore_scope_type :string           not null
#  signature         :string           not null
#  comment           :text
#  created_by        :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_result_ignore_rules_on_i_s_type_and_i_s_id                (ignore_scope_type,ignore_scope_id)
#  index_result_ignore_rules_on_i_s_type_and_i_s_id_and_signature  (ignore_scope_type,ignore_scope_id,signature) UNIQUE
#  index_result_ignore_rules_on_signature                          (signature)
#
# rubocop:enable Metrics/LineLength

class ResultIgnoreRule < ApplicationRecord
  # RBAC
  include Authority::Abilities

  belongs_to :ignore_scope, polymorphic: true

  attr_readonly :signature

  validates :signature, presence: true
  validates :signature, format: { with: /\A[a-f0-9]{64}\z/, message: 'must be a SHA256 hash' }
  validates :created_by, presence: true

  after_create :ignore_latest_results
  after_destroy :unignore_latest_results

  def self.for_ignore_scopes(*ignore_scopes)
    where(ignore_scope: ignore_scopes)
  end

  private

  def ignore_latest_results
    IgnoreRuleApplicator.new(signature, ignore_scope).apply
  end

  def unignore_latest_results
    IgnoreRuleApplicator.new(signature, ignore_scope).undo
  end
end
