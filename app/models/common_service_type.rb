# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: common_service_types
#
#  id                 :integer          not null, primary key
#  port               :integer          not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  transport_protocol :integer          default("tcp"), not null
#
# Indexes
#
#  index_common_service_types_on_port  (port)
#
# rubocop:enable Metrics/LineLength

class CommonServiceType < ApplicationRecord
  has_many :services, inverse_of: :common_service_type
  has_many :security_containers, inverse_of: :common_service_type

  enum transport_protocol: { tcp: 0, udp: 1 }
end
