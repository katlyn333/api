# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: services
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  description            :text
#  port                   :integer          not null
#  port_type              :integer          default("tcp"), not null
#  encryption_type        :integer          default("cleartext"), not null
#  machine_id             :integer
#  type                   :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  allow_brute_force      :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  discovered             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_common_service_type_id  (common_service_type_id)
#  index_services_on_machine_id              (machine_id)
#  index_services_on_machine_id_and_port     (machine_id,port) UNIQUE
#  index_services_on_type                    (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (common_service_type_id => common_service_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class GenericService < Service
end
