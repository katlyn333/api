# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class UnreachableMachineError < OrganizationError
  MESSAGE = 'There are machines in your Organization in a private network, '\
    'but no verified Relay is available to scan them.'

  class << self
    def check(org)
      org_has_unreachable_machines?(org) ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end

    private

    def org_has_unreachable_machines?(org)
      org.rfc1918_machines.exists? && !org.primary_relay
    end
  end

  def message
    MESSAGE
  end
end
