# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#
# rubocop:enable Metrics/LineLength

class LocalAuthenticationMethod < AuthenticationMethod
  # Validations
  validates_associated :local_authentication_record

  # Associations
  has_one :local_authentication_record, foreign_key: :authentication_method_id, dependent: :destroy

  # Nested Attributes
  accepts_nested_attributes_for :local_authentication_record

  # Callbacks
  after_commit :send_email_confirmation, on: :create

  def authenticate(params)
    local_authentication_record.authenticate(params[:user][:password])
  end

  def send_email_confirmation
    UserMailer.email_confirmation(user).deliver_later
  end
end
