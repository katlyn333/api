# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: services
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  description            :text
#  port                   :integer          not null
#  port_type              :integer          default("tcp"), not null
#  encryption_type        :integer          default("cleartext"), not null
#  machine_id             :integer
#  type                   :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  allow_brute_force      :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  discovered             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_common_service_type_id  (common_service_type_id)
#  index_services_on_machine_id              (machine_id)
#  index_services_on_machine_id_and_port     (machine_id,port) UNIQUE
#  index_services_on_type                    (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (common_service_type_id => common_service_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class Service < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Attribute Information
  enum port_type: { tcp: 0, udp: 1 }
  enum encryption_type: { cleartext: 0, ssl: 1, ssh: 2 }
  # Note that in the future we may wish to look for a way to generate this list dynamically if the
  # number of subclasses grows large. For now, it's feasible to explicitly list them.
  VALID_TYPES = %w(GenericService SshService WebApplicationService).freeze

  # Associations
  belongs_to :machine, inverse_of: :services
  belongs_to :common_service_type, inverse_of: :services
  has_one :service_identity
  has_many :assessments, inverse_of: :service

  # Validations
  validates :machine, presence: true
  validates :name, presence: true
  validates :port, presence: true
  validates :port, inclusion: { in: 1..65_535, message: 'must be a number between 1 and 65,535' }
  validates :type, inclusion: { in: VALID_TYPES, message: "must be one of [#{VALID_TYPES.join(', ')}]" }

  delegate :organization, to: :machine

  # Callbacks
  before_save :set_common_service_type

  # I'm disabling these cops because I think this is the most readable way to write this
  # functioanlity. I am open to any suggestions to improve it
  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def self.testable_by(container)
    if container.authenticated?
      where(type: 'SshService').of_service_type(container.common_service_type_id)
    elsif container.web_application?
      where(type: 'WebApplicationService').of_service_type(container.common_service_type_id)
    elsif container.brute_force?
      where(allow_brute_force: true).of_service_type(container.common_service_type_id)
    elsif container.ssl_crypto?
      ssl.of_service_type(container.common_service_type_id)
    elsif container.ssh_crypto?
      ssh.of_service_type(container.common_service_type_id)
    else # we should never reach this, but just in case...
      none
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  def self.of_service_type(type_id)
    return all unless type_id
    where('common_service_type_id = ? OR common_service_type_id IS NULL', type_id)
  end

  def service_configuration
    {
      port: port,
      service_username: service_identity&.username,
      service_password: service_identity&.password
    }
  end

  def self.build_or_update_discovered_service(attrs)
    service = where(port: attrs[:port]).first_or_initialize
    return service if service.id && !service.discovered? # Don't modify manually added services
    service.discovered = true
    service.type = infer_type_by_port(attrs[:port])
    service.name = attrs[:name]
    service.port_type = attrs[:port_type]
    service.encryption_type = attrs[:encryption_type]
    service.common_service_type = attrs[:common_service_type]
    service
  end

  def self.infer_type_by_port(port)
    if WebApplicationService::COMMON_WEB_PORTS.include? port
      'WebApplicationService'
    elsif SshService::COMMON_SSH_PORTS.include? port
      'SshService'
    else
      'GenericService'
    end
  end

  private

  # Callback definitions
  def set_common_service_type
    self.common_service_type ||= CommonServiceType.find_by(port: port, transport_protocol: port_type)
  end
end
