# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class RelayOfflineError < RelayError
  MESSAGE = 'Primary Relay is offline!'

  class << self
    def check(org)
      org.primary_relay&.offline? ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end
  end

  def message
    MESSAGE
  end
end
