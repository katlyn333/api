# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  uid        :string           not null
#  firstname  :string
#  lastname   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#  index_users_on_uid    (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

class User < ApplicationRecord
  def to_param
    uid
  end

  attr_readonly :uid, :email

  # RBAC
  include Authority::UserAbilities
  include Authority::Abilities
  rolify

  # Validations
  validates :uid, presence: true
  validates :uid, format: { with: /\A[a-zA-Z]/ }
  validates :firstname, format: { with: /\A[a-zA-Z][a-zA-Z.-]*\z/ }, allow_blank: true
  validates :lastname, format: { with: /\A[a-zA-Z][a-zA-Z.-]*\z/ }, allow_blank: true
  validates :email, presence: true
  validates :email, format: { with: /@/ }
  validates_associated :local_authentication_record

  # Associations
  has_many :memberships
  has_many :organizations, through: :memberships
  has_one :api_token, -> { active }
  has_one :authentication_method, dependent: :destroy
  has_one :local_authentication_method
  has_one :reverse_proxy_authentication_method
  has_one :local_authentication_record, through: :local_authentication_method

  # Callback Declarations
  before_create :build_api_token
  before_validation :initialize_uid_from_email
  after_create :create_default_org

  accepts_nested_attributes_for :local_authentication_method

  # Delegations
  delegate :reset_password!, :confirm!, :confirmed?, to: :local_authentication_record

  # Scopes
  scope :confirmed_users, lambda {
    joins(:local_authentication_record).where('local_authentication_records.email_confirmed_at IS NOT NULL')
  }

  class << self
    def authenticate(params)
      AuthenticationMethod.find_or_create_user_for_authentication(params)
        &.authentication_method
        &.authenticate(params)
    end

    def find_or_create_by_uid(uid)
      where(uid: uid).first_or_create do |new_user|
        new_user.build_reverse_proxy_authentication_method
        new_user.email = "#{uid}@#{AuthenticationMethod.sso_domain}"
      end
    end

    def by_api_token(token_value)
      joins(:api_token).merge(ApiToken.active).find_by(api_tokens: { value: token_value })
    end

    # Maintain backward compatibility with the original definition of ::find. We will cast the input
    # to an integer. Since the uid property must begin with a letter of the alphabet, converting to
    # an integer will result in its value being 0
    def find(input)
      if input.to_i.zero?
        o = find_by uid: input
        raise ActiveRecord::RecordNotFound unless o
        o
      else
        super
      end
    end
  end

  def send_welcome_email
    UserMailer.welcome_notification(self).deliver_later
  end

  private

  def initialize_uid_from_email
    self.uid = email.split('@').first if uid.blank? && email.present?
  end

  def create_default_org
    org = organizations.create!(uid: "#{uid}-org-#{Organization::DEFAULT_SUFFIX}",
                                default_org: true)
    add_role :organization_admin, org
    org
  end
end
