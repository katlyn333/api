# frozen_string_literal: true
module ActsAsAgent
  extend ActiveSupport::Concern

  included do
    # Validations
    validates :public_key, presence: true
    validates :state, presence: true
    validates :verified, presence: true, allow_blank: true
    validates :last_heartbeat, presence: true
    validates :queue_name, presence: true

    # Callback Declarations
    before_validation on: :create do
      self.queue_name = 'placeholder'
      self.last_heartbeat = Time.current
      self.verified = false
      # allow docker relays to be online by default
      self.state = :offline unless is_a? DockerRelay
    end
    before_save :generate_key_signature
    around_create :generate_queue_name
    after_commit :setup_queue, unless: 'Rails.env.test?'

    # Scopes
    scope :verified, -> { where(verified: true) }
  end

  # Instance methods
  def setup_queue
    Publisher.bind_queue associated_organization_token, queue_name
  rescue StandardError => e
    # No matter what happens here, we want to be sure we still return the agent to the client
    logger.error "Problem setting up the queue: => #{e.message}"
  end

  # Decode the public key stored in the database and return an OpenSSL RSA key
  def public_key_to_rsa
    OpenSSL::PKey::RSA.new(Base64.decode64(public_key))
  end

  private

  def associated_organization_token
    # This method should be implented by the class which includes this library
    raise NotImplementedError, 'This method should be implented by the class which includes this library'
  end

  # Callback Definitions
  def generate_queue_name
    self.queue_name = "noradqueue-#{SecureRandom.hex 32}"
    yield
  rescue ActiveRecord::RecordNotUnique => e
    e.message =~ /index_assets_on_queue_name/ ? retry : raise
  end

  def generate_key_signature
    self.key_signature = OpenSSL::Digest::SHA256.hexdigest(public_key_to_rsa.to_der).scan(/../).join ':'
  end
end
