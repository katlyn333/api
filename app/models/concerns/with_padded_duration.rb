# frozen_string_literal: true

# Provides helpers for mitigating timing attacks
module WithPaddedDuration
  extend ActiveSupport::Concern

  included do
    class << self
      def with_padded_duration(duration)
        start = Time.now.utc
        result = yield
        finish = Time.now.utc

        # sleep for 0 if operation took longer than 2 seconds
        sleep [duration.to_f - (finish - start), 0].max

        result
      end
    end
  end
end
