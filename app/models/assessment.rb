# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: assessments
#
#  id                           :integer          not null, primary key
#  identifier                   :string           not null
#  machine_id                   :integer
#  state                        :integer          default("pending_scheduling"), not null
#  title                        :string
#  description                  :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  security_container_id        :integer
#  security_container_secret_id :integer
#  docker_command_id            :integer
#  state_transition_time        :datetime         not null
#  service_id                   :integer
#  type                         :string           not null
#
# Indexes
#
#  index_assessments_on_docker_command_id             (docker_command_id)
#  index_assessments_on_identifier                    (identifier) UNIQUE
#  index_assessments_on_machine_id                    (machine_id)
#  index_assessments_on_security_container_id         (security_container_id)
#  index_assessments_on_security_container_secret_id  (security_container_secret_id)
#  index_assessments_on_service_id                    (service_id)
#
# Foreign Keys
#
#  fk_rails_02ed0901f2  (docker_command_id => docker_commands.id) ON DELETE => cascade
#  fk_rails_1da80f5b8d  (security_container_secret_id => security_container_secrets.id) ON DELETE => cascade
#  fk_rails_6cc66ad8bb  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_94558cc568  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_f27a81522a  (service_id => services.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class Assessment < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include AssessmentStateMachine

  SHA1_HEX_LENGTH = 40
  RANKED_ASSESSMENT_STATUSES = [:erroring, :failing, :warning, :passing, :informing].freeze
  STALE_HOUR = 48

  # Attribute information
  def to_param
    identifier
  end
  attr_readonly :identifier

  # Associations
  has_many :results, inverse_of: :assessment
  belongs_to :docker_command, inverse_of: :assessments
  belongs_to :machine, inverse_of: :assessments
  belongs_to :service, inverse_of: :assessments
  belongs_to :security_container, inverse_of: :assessments
  belongs_to :security_container_secret, inverse_of: :assessments

  # Validations
  validates :identifier, format: { with: /\A[a-f0-9]{40}\z/, message: 'must be a SHA1 hash' }, allow_blank: true
  validates :docker_command, presence: true
  validates :machine, presence: true
  validates :service, presence: :true, unless: proc { |a| a.security_container&.whole_host? }
  validates :security_container, presence: true
  validates :security_container_secret, presence: true

  # Callback Declarations
  # This could result in the state transition
  before_create :initialize_state_transition_timestamp
  around_create :generate_identifier

  class << self
    def find(input)
      return super unless input.to_s.length == SHA1_HEX_LENGTH
      find_by(identifier: input).tap { |assessment| raise ActiveRecord::RecordNotFound unless assessment }
    end

    def latest
      command = DockerCommand.where(id: select(:docker_command_id).distinct).order(:created_at).last
      command ? where(docker_command: command) : none
    end

    def latest_results_stats
      Result.where(assessment: latest).stats
    end

    def stale
      in_progress.where('state_transition_time <= ?', STALE_HOUR.hours.ago)
    end

    def most_recent_by_docker_command_id(limit = 5)
      for_docker_command(
        select(:docker_command_id).order(docker_command_id: :desc).distinct.limit(limit)
      ).order(docker_command_id: :desc)
    end

    def for_docker_command(dcid)
      where(docker_command_id: dcid)
    end

    # Status from a single assessment_stats based on severity
    def status_from_stats(stats)
      RANKED_ASSESSMENT_STATUSES.detect(-> { :pending }) { |status| stats[status]&.positive? }
    end
  end

  # This class size is getting large. The contents of this method are a candidate for being moved to
  # a service object.
  def create_result_set!(results_array)
    transaction do
      results_array.each do |attrs|
        next if attrs['status'] == 'pending'
        results.create! attrs
      end
      complete!
      mark_ignored_results_as_noise
    end
  end

  # This class size is getting large. The contents of this method are a candidate for being moved to
  # a service object.
  def timeout!
    transaction do
      attrs_to_hash = { nid: 'norad:timeout', sir: 'no_impact', status: :error }
      result_attrs = attrs_to_hash.merge(
        title: Result::ASSESSMENT_TIMEOUT_TITLE,
        output: Result::ASSESSMENT_TIMEOUT_OUTPUT,
        signature: OpenSSL::Digest::SHA256.hexdigest(attrs_to_hash.to_json)
      )
      results.create!(result_attrs)
      complete!
    end
  end

  # This class size is getting large. The contents of this method are a candidate for being moved to
  # a query object.
  def associated_requirements
    return [] unless security_container
    security_container
      .requirements
      .joins(requirement_group: :organizations)
      .where(organizations: { id: machine.organization_id })
  end

  def latest_results
    results
  end

  def results_path
    Rails.application.routes.url_helpers.v1_assessment_results_path identifier
  end

  private

  # Callback Definition
  def generate_identifier
    self.identifier = OpenSSL::Digest::SHA1.hexdigest("#{Time.now.to_f}#{SecureRandom.hex}")
    yield
  rescue ActiveRecord::RecordNotUnique
    retry
  end

  def initialize_state_transition_timestamp
    self.state_transition_time = created_at
  end

  def mark_ignored_results_as_noise
    ignore_query = ResultIgnoreRule.select(:signature).for_ignore_scopes(machine, machine.organization)
    IgnoreRuleApplicator.new(ignore_query, self).apply
  end
end
