# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class NoSshKeyPairError < OrganizationError
  MESSAGE = 'You have one or more tests enabled that require authentication, but you have not added an SSH Key Pair.'

  class << self
    def check(org)
      required_ssh_key_pair_missing?(org) ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end

    private

    def required_ssh_key_pair_missing?(org)
      !org_has_ssh_key_pair?(org) && org_has_authenticated_tests_enabled?(org)
    end

    def org_has_authenticated_tests_enabled?(org)
      organization_configs(org)
        .joins(:security_container)
        .or(machine_configs(org).joins(:security_container))
        .for_authenticated_test_type
        .explicitly_enabled # ignore tests enforced by Requirements
        .exists?
    end

    def org_has_ssh_key_pair?(org)
      org.ssh_key_pairs.exists?
    end

    def machine_configs(org)
      SecurityContainerConfig.where(machine_id: org.machines)
    end

    def organization_configs(org)
      SecurityContainerConfig.where(organization_id: org.id)
    end
  end

  def message
    MESSAGE
  end
end
