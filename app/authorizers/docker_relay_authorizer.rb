# frozen_string_literal: true
class DockerRelayAuthorizer < Authority::Authorizer
  def self.readable_by?(user, options)
    return false unless options[:in] && options[:in].is_a?(Organization)
    user.has_role? :organization_admin, options[:in]
  end

  def readable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def deletable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
