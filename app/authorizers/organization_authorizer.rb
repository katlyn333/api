# frozen_string_literal: true
class OrganizationAuthorizer < Authority::Authorizer
  def self.creatable_by?(_user)
    true
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource
  end

  def readable_by?(user)
    user.has_any_role?(*read_write_roles)
  end

  def deletable_by?(user)
    user.has_role? :organization_admin, resource
  end

  private

  def read_write_roles
    [
      { name: :organization_admin, resource: resource },
      { name: :organization_reader, resource: resource }
    ]
  end
end
