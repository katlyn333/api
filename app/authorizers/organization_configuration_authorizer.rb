# frozen_string_literal: true
class OrganizationConfigurationAuthorizer < Authority::Authorizer
  def updatable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def readable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
