# frozen_string_literal: true
class RequirementAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    admin?(user, options)
  end

  def self.readable_by?(_user)
    true
  end

  def updatable_by?(user)
    admin?(user)
  end

  def deletable_by?(user)
    admin?(user)
  end

  def admin?(user)
    user.has_role? :requirement_group_admin, resource.requirement_group
  end

  def self.admin?(user, options)
    user.has_role? :requirement_group_admin, options[:in]
  end
end
