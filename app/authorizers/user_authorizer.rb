# frozen_string_literal: true
class UserAuthorizer < Authority::Authorizer
  def readable_by?(user)
    resource.id == user.id
  end

  def updatable_by?(user)
    resource.id == user.id
  end
end
