# frozen_string_literal: true
class ProvisionAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    requirement_admin? user, options[:in]
  end

  def self.readable_by?(_user, _options)
    true
  end

  def self.updatable_by?(_user, _options)
    false
  end

  def deletable_by?(user)
    self.class.requirement_admin? user, resource.requirement
  end

  # Helper to drill down to :requirement_group_admin
  def self.requirement_admin?(user, requirement)
    user.has_role? :requirement_group_admin, requirement.requirement_group
  end
end
