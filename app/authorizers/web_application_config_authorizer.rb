# frozen_string_literal: true
class WebApplicationConfigAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource.web_application_service.organization
  end

  # There is no index, show, or destroy action
  def self.readable_by?(_user, _options)
    false
  end

  def readable_by?(_user)
    false
  end

  def deletable_by?(_user)
    false
  end
end
