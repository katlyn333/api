# frozen_string_literal: true
class ApiTokenAuthorizer < Authority::Authorizer
  def readable_by?(user)
    resource.user_id == user.id
  end

  def updatable_by?(user)
    resource.user_id == user.id
  end
end
