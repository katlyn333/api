# frozen_string_literal: true
class DockerCommandAuthorizer < BelongsToPolyParentAuthorizer
  private

  def poly_assoc_name
    :commandable
  end
end
