# frozen_string_literal: true
class ResultIgnoreRuleAuthorizer < BelongsToPolyParentAuthorizer
  private

  def poly_assoc_name
    :ignore_scope
  end
end
