# frozen_string_literal: true
class SshKeyPairAuthorizer < Authority::Authorizer
  class << self
    def readable_by?(user, options)
      user.has_role?(:organization_reader, options[:in]) ||
        user.has_role?(:organization_admin, options[:in])
    end

    def creatable_by?(user, options)
      user.has_role? :organization_admin, options[:in]
    end
  end

  def readable_by?(user)
    user.has_role?(:organization_reader, resource.organization) ||
      user.has_role?(:organization_admin, resource.organization)
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def deletable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
