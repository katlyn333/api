# frozen_string_literal: true
class IaasConfigurationAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def deletable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def readable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
