# frozen_string_literal: true
class MachineAuthorizer < Authority::Authorizer
  def self.readable_by?(user, options)
    reader?(user, options) || admin?(user, options)
  end

  def self.creatable_by?(user, options)
    admin?(user, options)
  end

  def readable_by?(user)
    reader?(user) || admin?(user)
  end

  def updatable_by?(user)
    admin?(user)
  end

  def deletable_by?(user)
    admin?(user)
  end

  def reader?(user)
    user.has_role? :organization_reader, resource.organization
  end

  def admin?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def self.reader?(user, options)
    user.has_role? :organization_reader, options[:in]
  end

  def self.admin?(user, options)
    user.has_role? :organization_admin, options[:in]
  end
end
