# frozen_string_literal: true
class IaasDiscoveryAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def self.readable_by?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def readable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
