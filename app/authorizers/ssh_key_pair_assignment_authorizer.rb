# frozen_string_literal: true
class SshKeyPairAssignmentAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def deletable_by?(user)
    user.has_role? :organization_admin, resource.machine.organization
  end
end
