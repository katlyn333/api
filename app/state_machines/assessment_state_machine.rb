# frozen_string_literal: true
module AssessmentStateMachine
  extend ActiveSupport::Concern

  COMPLETE_STATE_NUM = 2

  included do
    include AASM

    enum state: { pending_scheduling: 0, in_progress: 1, complete: COMPLETE_STATE_NUM }

    aasm column: :state, enum: true do
      state :pending_scheduling, initial: true
      state :in_progress
      state :complete
      before_all_events :update_state_transition_timestamp

      event :start do
        after do
          docker_command.increment_assessments_in_flight
        end
        transitions from: :pending_scheduling, to: :in_progress
      end

      event :complete do
        after do
          docker_command.decrement_assessments_in_flight
        end
        transitions from: [:in_progress, :pending_scheduling], to: :complete
      end
    end

    private

    # This will only persist if the state transition is successful
    def update_state_transition_timestamp
      self.state_transition_time = Time.current
    end
  end
end
