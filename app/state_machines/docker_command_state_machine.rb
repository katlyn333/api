# frozen_string_literal: true
module DockerCommandStateMachine
  extend ActiveSupport::Concern

  # rubocop:disable Metrics/BlockLength
  included do
    include AASM
    COMPLETE_STATE_NUM = 2

    enum state: { pending: 0, errored: 1, assessments_created: COMPLETE_STATE_NUM }

    aasm column: :state, enum: true do
      state :pending, initial: true
      state :errored
      state :assessments_created

      event :complete do
        after do
          unless assessments.present?
            # no assessments were actually created
            NotificationService.scan_complete(self)
          end
        end
        transitions from: :pending, to: :assessments_created
      end

      event :cancel do
        after do
          NotificationService.scan_complete(self)
        end
        transitions from: :pending, to: :errored, after: :handle_pending_assessments
      end
    end

    private

    # If we are here, it means things mostly likely went south before any assessments were created.
    # But, just in case, we'll try to handle any assessments that would otherwise be stuck in a
    # pending state.
    def handle_pending_assessments
      assessments.pending_scheduling.each do |assessment|
        assessment.results.create!(
          nid: 'norad:error',
          sir: 'no_impact',
          status: :error,
          title: Result::API_ERROR_TITLE,
          output: Result::API_ERROR_OUTPUT
        )
        assessment.complete!
      end
    end
  end
  # rubocop:enable Metrics/BlockLength
end
