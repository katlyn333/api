# frozen_string_literal: true
module IaasDiscoveryStateMachine
  extend ActiveSupport::Concern

  PENDING_STATE = 0
  IN_PROGRESS_STATE = 1

  included do
    include AASM

    enum state: { pending: PENDING_STATE, in_progress: IN_PROGRESS_STATE, completed: 2, failed: 3 }

    aasm column: :state, enum: true do
      state :pending, initial: true
      state :in_progress, :completed, :failed

      event :start do
        transitions from: :pending, to: :in_progress
      end

      event :complete do
        transitions from: :in_progress, to: :completed
      end

      event :fail do
        transitions from: :in_progress, to: :failed
      end
    end
  end
end
