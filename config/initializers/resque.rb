# frozen_string_literal: true
# Adapted from the Resque GitHub README

unless Rails.env.test?
  rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
  resque_config = YAML.safe_load(ERB.new(File.read(rails_root + '/config/resque.yml')).result)
  Resque.redis = resque_config[Rails.env]
  Resque.logger = Logger.new(Rails.root.join('log', "resque_#{Rails.env}.log"))
  Resque.logger.level = Rails.env.development? ? Logger::DEBUG : Logger::INFO
end
