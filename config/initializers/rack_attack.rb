# frozen_string_literal: true
module Rack
  class Attack
    ### Whitelist Important Clients ###

    # The UI should be whitelisted
    ui_ip_address = ENV.fetch('RACK_ATTACK_UI_IP_WHITELIST')
    safelist('allow from UI') do |req|
      req.ip == ui_ip_address
    end

    ### Throttle Spammy Clients ###

    # If any single client IP is making tons of requests, then they're
    # probably malicious or a poorly-configured scraper. Either way, they
    # don't deserve to hog all of the app server's CPU. Cut them off!
    #
    # Note: If you're serving assets through rack, those requests may be
    # counted by rack-attack and this throttle may be activated too
    # quickly. If so, enable the condition to exclude them from tracking.

    # Throttle all requests by IP (60rpm)
    #
    # Key: "rack::attack:#{Time.now.to_i/:period}:req/ip:#{req.ip}"
    request_limit = ENV.fetch('RACK_ATTACK_REQUESTS').to_i
    throttle('req/ip', limit: request_limit, period: 5.minutes, &:ip)

    login_path_regexp = %r{\A\/v1\/users\/.*\/authenticate\z}

    ### Prevent Brute-Force Login Attacks ###

    # The most common brute-force login attack is a brute-force password
    # attack where an attacker simply tries a large number of usernames and
    # passwords to see if any credentials match.
    #
    # Another common method of attack is to use a swarm of computers with
    # different IPs to try brute-forcing a password for a specific account.
    # We do not currently check passwords, but this will need to be updated
    # once password authentication is introduced. This throttle simply prevents
    # excessive posts to users authenticate route from the same IP address.

    # Throttle POST requests to /v1/users/:username/authenticate by
    # by IP address
    #
    # Key: "rack::attack:#{Time.now.to_i/:period}:logins/ip:#{req.ip}"
    ip_login_limit = ENV.fetch('RACK_ATTACK_IP_LOGINS').to_i
    throttle('logins/ip', limit: ip_login_limit, period: 20.seconds) do |req|
      req.ip if req.post? && req.path =~ login_path_regexp
    end

    # Throttle POST requests to /v1/users/:username/authenticate by
    # username param
    #
    # Key: username = self.username_from_request_path(req.path)
    # Key: "rack::attack:#{Time.now.to_i/:period}:logins/username:#{username}"
    #
    # Note: This creates a problem where a malicious user could intentionally
    # throttle logins for another user and force their login requests to be
    # denied, but that's not very common and shouldn't happen to you. (Knock
    # on wood!)
    email_login_limit = ENV.fetch('RACK_ATTACK_USERNAME_LOGINS').to_i
    throttle('logins/username',
             limit: email_login_limit,
             period: 20.seconds) do |req|
      if req.post? && req.path =~ login_path_regexp
        username_from_request_path(req.path)
      end
    end

    private_class_method def self.username_from_request_path(path)
      path[/#{'/v1/users/'}(.*?)#{'/authenticate'}/m, 1]
    end
  end
end
