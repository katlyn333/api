# frozen_string_literal: true
NoradApi::Application.config.secret_token = ENV['NORAD_API_SECRET_TOKEN']
