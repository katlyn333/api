# frozen_string_literal: true
NOTIFICATIONS = YAML.load_file(Rails.root.join('config/notifications.yml'))
