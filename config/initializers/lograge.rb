# frozen_string_literal: true
# Taken from: https://github.com/Sutto/rocket_pants/issues/111#issuecomment-57982402
if Rails.env.production? && Rails.application.config.lograge.enabled
  ActiveSupport::LogSubscriber.log_subscribers.each do |subscriber|
    case subscriber
    when ActionController::LogSubscriber
      Lograge.unsubscribe(:rocket_pants, subscriber)
    end
  end
  Lograge::RequestLogSubscriber.attach_to :rocket_pants
end
