# frozen_string_literal: true
# Use this file to easily define all of your cron jobs.
# Learn more: http://github.com/javan/whenever

set :output, 'log/cron.log'
job_type :rake, 'cd :path && RAILS_ENV=:environment bundle exec rake :task :output'

every :hour do
  rake :check_for_offline_relays
end

every :day, at: '12:20am' do
  rake :timeout_stale_assessments
end

every :day, at: '1:20am' do
  rake :timeout_stale_service_discoveries
end

every :day, at: '4:00am' do
  rake :sync_scan_schedules
end
