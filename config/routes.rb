# frozen_string_literal: true
# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  if Rails.env.development?
    require 'resque/server'
    mount Resque::Server.new, at: '/resque'
  end
  namespace 'v1' do
    resources :users, only: [:index, :show, :update, :create] do
      resource 'api_token', only: :update
      post 'authenticate', on: :member
      post 'authenticate', on: :collection
    end
    resources :organizations, except: [:new, :edit] do
      resources :result_ignore_rules, only: [:create, :index, :destroy]
      resources :docker_commands, only: [:create, :index]
      resources :machines, only: [:create, :index]
      resources :security_container_configs, only: [:index, :create]
      resources :docker_relays, only: :index
      resource :scan, only: :create
      resources :memberships, only: [:create, :index]
      resources :scan_schedules, only: [:create, :index], controller: :organization_scan_schedules
      resource :organization_token, only: :update
      resources :requirements, only: :index, controller: :organization_requirements
      resources :enforcements, only: [:index, :create]
      resource :iaas_configuration, only: [:create, :show]
      resources :iaas_discoveries, only: [:create, :index]
      resources :ssh_key_pairs, only: [:create, :index]
      resources :notification_channels, only: :index
    end
    resources :machines, only: [:show, :update, :destroy, :create] do
      resources :result_ignore_rules, only: [:create, :index, :destroy]
      resources :docker_commands, only: [:create, :index]
      resources :assessments, only: :index do
        get 'latest', on: :collection
      end
      resources :security_container_configs, only: [:index, :create]
      resources :services, only: [:index, :create]
      resources :service_discoveries, only: [:index, :create]
      resource :scan, only: :create
      resources :scan_schedules, only: [:create, :index], controller: :machine_scan_schedules
      resource :ssh_key_pair_assignment, only: [:create, :destroy]
    end
    resources :result_ignore_rules, only: [:show, :update, :destroy]
    resources :docker_commands, only: [:show, :destroy]
    resources :assessments, only: :show do
      resources :results, only: :create
    end
    resources :docker_relays, only: [:create, :update, :destroy, :show] do
      member do
        post :heartbeat
      end
    end
    resources :security_container_configs, only: [:show, :update, :destroy]
    resources :security_containers, only: [:index, :show]
    resources :services, only: [:show, :update, :destroy] do
      resources :web_application_configs, only: :create
      resources :service_identities, only: :create
    end
    resources :ssh_key_pairs, only: [:show, :update, :destroy]
    resources :notification_channels, only: [:show, :update]
    resources :organization_configurations, only: [:show, :update]
    resources :memberships, only: [:destroy]
    resources :machine_scan_schedules, only: [:update, :show, :destroy]
    resources :organization_scan_schedules, only: [:update, :show, :destroy]
    resources :enforcements, only: :destroy
    resources :requirement_groups, only: [:index, :show, :create, :update] do
      resources :requirements, only: :create
    end
    resources :requirements, only: [:show, :update, :destroy] do
      resources :provisions, only: :create
    end
    resources :provisions, only: :destroy
    resources :iaas_configurations, only: [:show, :update, :destroy]
    resources :iaas_discoveries, only: :show
    resources :service_discoveries, only: [:show, :update]
    resources :service_identities, only: :update
    resources :web_application_configs, only: :update
    resources :password_resets, only: [:create, :update, :show], param: :provided_token
    resources :email_confirmations, only: :create, param: :email_confirmation_token
  end
  mount(ClientTestRouteHelpers::Engine, at: '/client_test_route_helpers') if Rails.env.client_test?
end
# rubocop:enable Metrics/BlockLength
