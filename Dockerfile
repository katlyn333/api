###########################################################
# Dockerfile for Norad API
# Based on Ruby 2.3.3
############################################################

# Set the base image to Ubuntu
FROM ruby:2.3.3

# File Authors / Maintainers
LABEL authors="Blake Hitchcock, Brian Manifold, Roger Seagle"

# Update the system & add essential libs and programs
RUN apt-get update \
&& apt-get install -y --no-install-recommends \
  build-essential \
  software-properties-common \
  libgmp-dev \
  postgresql-client \
  nodejs \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
&& apt-get clean autoclean -y \
&& apt-get autoremove -y

# Create directory where app will live
ENV APP_PATH /norad/api
RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

ENV BUNDLE_PATH /norad/ruby_gems

COPY Gemfile .
COPY Gemfile.lock .

# Install Gems
RUN gem install bundler --no-document \
&& bundle install

# Copy the actual rails app to the image
COPY . .

EXPOSE 3000

# Start the Norad API with development server
CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0"]
