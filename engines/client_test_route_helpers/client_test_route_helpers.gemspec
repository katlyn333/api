# frozen_string_literal: true
$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'client_test_route_helpers/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'client_test_route_helpers'
  s.version = ClientTestRouteHelpers::VERSION
  s.authors = ['Jamil Bou Kheir']
  s.email = ['jamil@elbii.com']
  s.license = 'Apache 2.0'
  s.files = Dir['{config,lib}/**/*', 'Rakefile']
  s.summary = 'Endpoints for Norad UI tests'
  s.add_dependency 'rails', '~> 5.0.1'
end
