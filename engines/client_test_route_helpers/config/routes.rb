# frozen_string_literal: true
ClientTestRouteHelpers::Engine.routes.draw do
  get 'begin_transaction', to: 'helpers#begin_transaction'
  get 'rollback_transaction', to: 'helpers#rollback_transaction'
  get 'mail_deliveries', to: 'helpers#mail_deliveries'
  get 'last_email_confirmation_token', to: 'helpers#last_email_confirmation_token'
  get 'last_password_reset_token', to: 'helpers#last_password_reset_token'
  post 'set_env', to: 'helpers#set_env'
end
