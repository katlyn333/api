# frozen_string_literal: true
module ClientTestRouteHelpers
  class HelpersController < ActionController::API
    def begin_transaction
      # joinable:false ensures nested transactions cannot "join" this one and roll it back
      ActiveRecord::Base.connection.begin_transaction joinable: false
      head :no_content
    end

    def rollback_transaction
      ActiveRecord::Base.connection.rollback_transaction
      head :no_content
    end

    def mail_deliveries
      render json: ActionMailer::Base.deliveries.to_json
    end

    def last_email_confirmation_token
      render json: { token: LocalAuthenticationRecord.last.email_confirmation_token }
    end

    def last_password_reset_token
      render json: { token: LocalAuthenticationRecord.last.password_reset_token }
    end

    def set_env
      params[:env].keys.each { |key| ENV[key.to_s] = params[:env][key] }
      head :no_content
    end
  end
end
