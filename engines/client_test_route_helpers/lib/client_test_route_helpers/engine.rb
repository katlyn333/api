# frozen_string_literal: true
module ClientTestRouteHelpers
  class Engine < ::Rails::Engine
    isolate_namespace ClientTestRouteHelpers
  end
end
